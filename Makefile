include $(DEVROOT)/shared/makefiles/Make.includes

ALL	= $(CMS) eventsolution ewlib gcda hk rtevent rtpage rtseis tnalarm \
    tnchnl tndb tnframework tnpkg tnstd tntcp tntime tnwave tnwaveclient \
    tnwavepool

#uncomment the following line to build the ActiveMQ replacement for CMS
#ALL+=amq

all:
	for dir in $(ALL) ; do \
                echo build for $$dir ... ; \
                (cd $$dir; make depend; make); \
        done

clean:
	for dir in $(ALL) ; do \
                (cd $$dir; make clean); \
        done

veryclean:
	for dir in $(ALL) ; do \
                (cd $$dir; make veryclean); \
        done
