:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:

Libraries used by real time and post processing C++ code. Most of the libraries are written in C++, except where specified. See below for a brief description of each library. Some of the libraries are named with a prefix of rt (real time) and some with a prefix of tn (trinet). These prefixes are obsolete but have been kept for ease of use. 

Some libaries depend on an external library, qlib2. This is a C library developed by Doug Neuhauser at Berkeley for Quanterra data and miniSEED data manipulation. It also contains routines to convert to and from nominal time to true time and vice versa.

## cms

C++ routines for communicating with the CMS server.  

## eventsolution

C++ routines for calculating hypoinverse solutions.  

## ewlib

C++ wrappers for earthworm modules calculating local magnitude, origin time and location.   

## gcda

Encapsulates a generic contiguous data area (gcda). This contiguous data area (cda) is a shared memory ring buffer(or circular queue) and holds evenly spaced samples of channel data.  An empty queue is indicated by an extra sample. 

Each cda has a key or a name associated with it to distinguish it from cdas of other applications. This key is also used to get a list of channels (from the database) to populate the cda with. 

The library includes a classes that 
* maintain channel metadata for each channel in a cda.  
* maintain metadata about each channel's status in the cda.
* implement readers and writers with appropriate locking, shared for readers and exclusive for writers.


## hk 

Fortran routines written by Dr. Hiroo Kanamori. They are based on this paper http://gps-prod-storage.cloud.caltech.edu.s3.amazonaws.com/people_personal_assets/kanamori/HKbssa99b.pdf

## rtevent

Encapsulates event, origin, magnitude, amplitude, arrival, coda and request card. Additionally, the library defines 
* constants for phases 
* constants for event review
* units for amplitudes


## rtpage

Encapsulates a paging system. Library for creating and sending messages to users, groups and devices. Primarily, used by the aqms-alarming module.

## rtseis

Enscapsulates various seismological functions written in fortran. Routines include computing magnitude, nearest geographical location for events, computing travel time. The library also contains C/C++ interfaces to the fortran routines.    

## tnalarm

Encapsulates the state machine of an alarm. The Action class defines the possible states of an alarm as well as how to transistion between states. It forms the base class and/or data structure for AQMS alarming module. 

This library includes classes that define geographical boundaries for an alarm viz. Point, Polygon and Region. A Point is a pair of lat,lon. A Polygon is a list of Points. A Region is derived from Polygon and additionally, has a name. 


## tnchnl

Encapsulates a seismic channel. Includes routines for reading configuration files for channels, reading channels from database and comparing one channel to another.  


## tndb

Encapsulates a database connection. Oracle (via OTL v4) and Postgres databases are supported. It forms a base class for AQMS modules to derive their own implementation of a database class. All AQMS modules making transactions must support Oracle as well as Postgres.
The library includes database constants for the event parametric and waveform schemas.

###### Dependencies : OTL

## tnframework

Encapsulates a framework or template to start, stop and monitor an AQMS module.  

## tnpkg

Routines for packing and unpacking CMS messages.  

## tnstd

C, C++ and fortran routines providing an equivalent of system level functions, including 
* a string class for AQMS,
* a telnet connection class,
* a greate circle distance calculation routine,
* a system() that will timeout if process doesn't finish within specified timeout,
* byte swapping, 
* rounding floating point numbers to integers,
* determing if a 2 digit year is in the 20th or 21st centure,
* use of temporary disk files, 
* use of disk files for status logging,
* reading of files and sub-directories in a UNIX file system directory,
* reading of configuration files including grouped configurations,
* interprocess communication method, pipe,
* use of disk file for locking mechanism and file locking,
* conversion of decimal degress to decimal minutes,
* regular expressions for AQMS,
* logging of status messages for AQMS applications,
* defining exceptions raised by AQMS applications,
* sending and receiving of multicast messages.

###### Dependencies : qlib2

## tntcp

Encapsulates a TCP connection, message, server and client. The classes encapsulate the system-level unix calls needed for running a server that listens on a TCP port and a client that connects to this server. The class encapsulating the messages exchanged between client and server includes support for miniSEED data records. They can be used as base classes for any AQMS modules that employ a TCP/IP client-server architecture.

## tntime

AQMS handles two different time styles; nominal time and true time. A nominal timestamp is unix time timestamp. A true time timestamp is nominal time that is corrected for leap seconds i.e. nominal timestamp +/- any leapseconds till that nominal time stamp. Data from dataloggers is timestamped in either nominal or gps time. All AQMS processing modules use true time. Hence, the need for conversion.  

This library includes classes for timestamp, duration and time window. 

Timestamp class represents a point in time and supports respresentation of time during leap seconds. A timestamp returned my methods of this class is in true time.

Duration class represents a signed length of an arbitrary time interval. 

TimeWindow class represents an arbitrary half-open time interval by its start time and end time. The represented interval starts at the start time and runs up to but not including the end time.

Additionally, there is a YMDHMS class which converts a timestamp to human readable components of YYYY MM DD HH MM SS AM/PM in both UTC and US/Pacific. 

###### Dependencies : qlib2

## tnwave

Encapsulate waveform record. This library support creating a waveform out of miniSEED data segments as well as retrieving them. It also supports basic metadata queries like number of samples and sample rate. Additionally, it encapsulates database (or archive) record of the waveform.

###### Dependencies : qlib2

## tnwaveclient

Library to connect to one or more trinet waveform servers and retrieve miniSEED data. Data can be retrieved for a single channel and time window input or in bulk for a list of channel and time window inputs. The library also includes classes for reading miniSEED data from an input file and for writing miniSEED data to an output file. These reader and writer classes use qlib2. tnwaveclient also includes a class for retrieving metadata regarding a miniSEED data segment viz. word order, compression format (STEIM 1, 2), record type, record size (512, 4096). 

The waveform archiver is one of the main users of this library. 

###### Dependencies : qlib2


## tnwavepool

Library to create, populate and access a shared memory area called the rapid wave pool (rwp). The wave pool holds miniSEED waveform data. A corresponding config file can be used to configure the rapid wave pool in terms of the number of channels and amount of data per channel. The rwp ensures that there is only one writer to it at a time, while allowing many readers. 

Being memory based, the rwp  allows for faster (or rapid) data access, as opposed to the more long term but slower disk wave pool (dwp). It typically is only a few hours deep because of memory constraints. The trinet wave servers access the rwp and dwp to serve out miniSEED data.
