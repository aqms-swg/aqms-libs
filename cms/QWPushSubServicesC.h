/** @file
 * @ingroup group_libs_cms
*/
// -*- C++ -*-
// $Id$

/**
 * Code generated by the The ACE ORB (TAO) IDL Compiler v2.2a
 * TAO and the TAO IDL Compiler have been developed by:
 *       Center for Distributed Object Computing
 *       Washington University
 *       St. Louis, MO
 *       USA
 *       http://www.cs.wustl.edu/~schmidt/doc-center.html
 * and
 *       Distributed Object Computing Laboratory
 *       University of California at Irvine
 *       Irvine, CA
 *       USA
 * and
 *       Institute for Software Integrated Systems
 *       Vanderbilt University
 *       Nashville, TN
 *       USA
 *       http://www.isis.vanderbilt.edu/
 *
 * Information about TAO is available at:
 *     http://www.cs.wustl.edu/~schmidt/TAO.html
 **/

// TAO_IDL - Generated from
// be/be_codegen.cpp:152

#ifndef _TAO_IDL_QWPUSHSUBSERVICESC_FOEQKR_H_
#define _TAO_IDL_QWPUSHSUBSERVICESC_FOEQKR_H_


#include /**/ "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */


#include "tao/AnyTypeCode/AnyTypeCode_methods.h"
#include "tao/AnyTypeCode/Any.h"
#include "tao/ORB.h"
#include "tao/SystemException.h"
#include "tao/Basic_Types.h"
#include "tao/ORB_Constants.h"
#include "tao/Object.h"
#include "tao/Objref_VarOut_T.h"
#include "tao/Arg_Traits_T.h"
#include "tao/Basic_Arguments.h"
#include "tao/Special_Basic_Arguments.h"
#include "tao/Any_Insert_Policy_T.h"
#include "tao/Fixed_Size_Argument_T.h"
#include "tao/Var_Size_Argument_T.h"
#include "tao/Object_Argument_T.h"
#include "tao/Special_Basic_Arguments.h"
#include "tao/UB_String_Arguments.h"
#include /**/ "tao/Version.h"
#include /**/ "tao/Versioned_Namespace.h"

#if TAO_MAJOR_VERSION != 2 || TAO_MINOR_VERSION != 2 || TAO_BETA_VERSION != 0
#error This file should be regenerated with TAO_IDL
#endif

#if defined (TAO_EXPORT_MACRO)
#undef TAO_EXPORT_MACRO
#endif
#define TAO_EXPORT_MACRO 

// TAO_IDL - Generated from
// be/be_visitor_root/root_ch.cpp:160
TAO_BEGIN_VERSIONED_NAMESPACE_DECL



namespace TAO
{
  template<typename T> class Narrow_Utils;
}
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// be/be_visitor_module/module_ch.cpp:38

namespace com
{

  // TAO_IDL - Generated from
  // be/be_visitor_module/module_ch.cpp:38

  namespace isti
  {

    // TAO_IDL - Generated from
    // be/be_visitor_module/module_ch.cpp:38

    namespace quakewatch
    {

      // TAO_IDL - Generated from
      // be/be_visitor_module/module_ch.cpp:38

      namespace corbaclient
      {

        // TAO_IDL - Generated from
        // be/be_visitor_module/module_ch.cpp:38

        namespace qw_client_rec
        {

          // TAO_IDL - Generated from
          // be/be_interface.cpp:750

#if !defined (_COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWRECMSGCALLBACK__VAR_OUT_CH_)
#define _COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWRECMSGCALLBACK__VAR_OUT_CH_

          class QWRecMsgCallBack;
          typedef QWRecMsgCallBack *QWRecMsgCallBack_ptr;

          typedef
            TAO_Objref_Var_T<
                QWRecMsgCallBack
              >
            QWRecMsgCallBack_var;
          
          typedef
            TAO_Objref_Out_T<
                QWRecMsgCallBack
              >
            QWRecMsgCallBack_out;

#endif /* end #if !defined */

          // TAO_IDL - Generated from
          // be/be_visitor_interface/interface_ch.cpp:43

          class  QWRecMsgCallBack
            : public virtual ::CORBA::Object
          {
          public:
            friend class TAO::Narrow_Utils<QWRecMsgCallBack>;

            // TAO_IDL - Generated from
            // be/be_type.cpp:307

            typedef QWRecMsgCallBack_ptr _ptr_type;
            typedef QWRecMsgCallBack_var _var_type;
            typedef QWRecMsgCallBack_out _out_type;

            static void _tao_any_destructor (void *);

            // The static operations.
            static QWRecMsgCallBack_ptr _duplicate (QWRecMsgCallBack_ptr obj);

            static void _tao_release (QWRecMsgCallBack_ptr obj);

            static QWRecMsgCallBack_ptr _narrow (::CORBA::Object_ptr obj);
            static QWRecMsgCallBack_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
            static QWRecMsgCallBack_ptr _nil (void);

            virtual ::CORBA::Boolean receiveMessage (
              const char * xmlMsgStr);

            // TAO_IDL - Generated from
            // be/be_visitor_interface/interface_ch.cpp:140

            virtual ::CORBA::Boolean _is_a (const char *type_id);
            virtual const char* _interface_repository_id (void) const;
            virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
          
          protected:
            // Concrete interface only.
            QWRecMsgCallBack (void);

            // Concrete non-local interface only.
            QWRecMsgCallBack (
                ::IOP::IOR *ior,
                TAO_ORB_Core *orb_core);
            
            // Non-local interface only.
            QWRecMsgCallBack (
                TAO_Stub *objref,
                ::CORBA::Boolean _tao_collocated = false,
                TAO_Abstract_ServantBase *servant = 0,
                TAO_ORB_Core *orb_core = 0);

            virtual ~QWRecMsgCallBack (void);
          
          private:
            // Private and unimplemented for concrete interfaces.
            QWRecMsgCallBack (const QWRecMsgCallBack &);

            void operator= (const QWRecMsgCallBack &);
          };

          // TAO_IDL - Generated from
          // be/be_visitor_typecode/typecode_decl.cpp:37

          extern  ::CORBA::TypeCode_ptr const _tc_QWRecMsgCallBack;

          // TAO_IDL - Generated from
          // be/be_interface.cpp:750

#if !defined (_COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWPUSHSUBSERVICES__VAR_OUT_CH_)
#define _COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWPUSHSUBSERVICES__VAR_OUT_CH_

          class QWPushSubServices;
          typedef QWPushSubServices *QWPushSubServices_ptr;

          typedef
            TAO_Objref_Var_T<
                QWPushSubServices
              >
            QWPushSubServices_var;
          
          typedef
            TAO_Objref_Out_T<
                QWPushSubServices
              >
            QWPushSubServices_out;

#endif /* end #if !defined */

          // TAO_IDL - Generated from
          // be/be_visitor_interface/interface_ch.cpp:43

          class  QWPushSubServices
            : public virtual ::CORBA::Object
          {
          public:
            friend class TAO::Narrow_Utils<QWPushSubServices>;

            // TAO_IDL - Generated from
            // be/be_type.cpp:307

            typedef QWPushSubServices_ptr _ptr_type;
            typedef QWPushSubServices_var _var_type;
            typedef QWPushSubServices_out _out_type;

            static void _tao_any_destructor (void *);

            // The static operations.
            static QWPushSubServices_ptr _duplicate (QWPushSubServices_ptr obj);

            static void _tao_release (QWPushSubServices_ptr obj);

            static QWPushSubServices_ptr _narrow (::CORBA::Object_ptr obj);
            static QWPushSubServices_ptr _unchecked_narrow (::CORBA::Object_ptr obj);
            static QWPushSubServices_ptr _nil (void);

            virtual void checkConnection (
              void);

            virtual void connectRecMsgCallBack (
              ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr recMsgCallBackObj,
              ::CORBA::Boolean initialEnableFlag);

            virtual void setCallBackEnabled (
              ::CORBA::Boolean enabledFlag);

            virtual char * disconnectSubscriber (
              void);

            virtual void subscribe (
              const char * domainStr,
              const char * chanStr);

            virtual void unsubscribe (
              const char * domainStr,
              const char * chanStr);

            // TAO_IDL - Generated from
            // be/be_visitor_interface/interface_ch.cpp:140

            virtual ::CORBA::Boolean _is_a (const char *type_id);
            virtual const char* _interface_repository_id (void) const;
            virtual ::CORBA::Boolean marshal (TAO_OutputCDR &cdr);
          
          protected:
            // Concrete interface only.
            QWPushSubServices (void);

            // Concrete non-local interface only.
            QWPushSubServices (
                ::IOP::IOR *ior,
                TAO_ORB_Core *orb_core);
            
            // Non-local interface only.
            QWPushSubServices (
                TAO_Stub *objref,
                ::CORBA::Boolean _tao_collocated = false,
                TAO_Abstract_ServantBase *servant = 0,
                TAO_ORB_Core *orb_core = 0);

            virtual ~QWPushSubServices (void);
          
          private:
            // Private and unimplemented for concrete interfaces.
            QWPushSubServices (const QWPushSubServices &);

            void operator= (const QWPushSubServices &);
          };

          // TAO_IDL - Generated from
          // be/be_visitor_typecode/typecode_decl.cpp:37

          extern  ::CORBA::TypeCode_ptr const _tc_QWPushSubServices;
        
        // TAO_IDL - Generated from
        // be/be_visitor_module/module_ch.cpp:67
        
        } // module com::isti::quakewatch::corbaclient::qw_client_rec
      
      // TAO_IDL - Generated from
      // be/be_visitor_module/module_ch.cpp:67
      
      } // module com::isti::quakewatch::corbaclient
    
    // TAO_IDL - Generated from
    // be/be_visitor_module/module_ch.cpp:67
    
    } // module com::isti::quakewatch
  
  // TAO_IDL - Generated from
  // be/be_visitor_module/module_ch.cpp:67
  
  } // module com::isti

// TAO_IDL - Generated from
// be/be_visitor_module/module_ch.cpp:67

} // module com

// TAO_IDL - Generated from
// be/be_visitor_arg_traits.cpp:68

TAO_BEGIN_VERSIONED_NAMESPACE_DECL


// Arg traits specializations.
namespace TAO
{

  // TAO_IDL - Generated from
  // be/be_visitor_arg_traits.cpp:147

#if !defined (_COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWRECMSGCALLBACK__ARG_TRAITS_)
#define _COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWRECMSGCALLBACK__ARG_TRAITS_

  template<>
  class  Arg_Traits< ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack>
    : public
        Object_Arg_Traits_T<
            ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr,
            ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_var,
            ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_out,
            TAO::Objref_Traits<com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack>,
            TAO::Any_Insert_Policy_Stream
          >
  {
  };

#endif /* end #if !defined */

  // TAO_IDL - Generated from
  // be/be_visitor_arg_traits.cpp:147

#if !defined (_COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWPUSHSUBSERVICES__ARG_TRAITS_)
#define _COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWPUSHSUBSERVICES__ARG_TRAITS_

  template<>
  class  Arg_Traits< ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices>
    : public
        Object_Arg_Traits_T<
            ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr,
            ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_var,
            ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_out,
            TAO::Objref_Traits<com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices>,
            TAO::Any_Insert_Policy_Stream
          >
  {
  };

#endif /* end #if !defined */
}

TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// be/be_visitor_traits.cpp:62

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Traits specializations.
namespace TAO
{

#if !defined (_COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWRECMSGCALLBACK__TRAITS_)
#define _COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWRECMSGCALLBACK__TRAITS_

  template<>
  struct  Objref_Traits< ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack>
  {
    static ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr duplicate (
        ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr p);
    static void release (
        ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr p);
    static ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr p,
        TAO_OutputCDR & cdr);
  };

#endif /* end #if !defined */

#if !defined (_COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWPUSHSUBSERVICES__TRAITS_)
#define _COM_ISTI_QUAKEWATCH_CORBACLIENT_QW_CLIENT_REC_QWPUSHSUBSERVICES__TRAITS_

  template<>
  struct  Objref_Traits< ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices>
  {
    static ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr duplicate (
        ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr p);
    static void release (
        ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr p);
    static ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr nil (void);
    static ::CORBA::Boolean marshal (
        const ::com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr p,
        TAO_OutputCDR & cdr);
  };

#endif /* end #if !defined */
}
TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// be/be_visitor_interface/any_op_ch.cpp:44



#if defined (ACE_ANY_OPS_USE_NAMESPACE)

namespace com
{
  
  namespace isti
  {
    
    namespace quakewatch
    {
      
      namespace corbaclient
      {
        
        namespace qw_client_rec
        {
           void operator<<= ( ::CORBA::Any &, QWRecMsgCallBack_ptr); // copying
           void operator<<= ( ::CORBA::Any &, QWRecMsgCallBack_ptr *); // non-copying
           ::CORBA::Boolean operator>>= (const ::CORBA::Any &, QWRecMsgCallBack_ptr &);
        }
      }
    }
  }
}

#else


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 void operator<<= (::CORBA::Any &, com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr); // copying
 void operator<<= (::CORBA::Any &, com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr *); // non-copying
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr &);
TAO_END_VERSIONED_NAMESPACE_DECL



#endif

// TAO_IDL - Generated from
// be/be_visitor_interface/any_op_ch.cpp:44



#if defined (ACE_ANY_OPS_USE_NAMESPACE)

namespace com
{
  
  namespace isti
  {
    
    namespace quakewatch
    {
      
      namespace corbaclient
      {
        
        namespace qw_client_rec
        {
           void operator<<= ( ::CORBA::Any &, QWPushSubServices_ptr); // copying
           void operator<<= ( ::CORBA::Any &, QWPushSubServices_ptr *); // non-copying
           ::CORBA::Boolean operator>>= (const ::CORBA::Any &, QWPushSubServices_ptr &);
        }
      }
    }
  }
}

#else


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 void operator<<= (::CORBA::Any &, com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr); // copying
 void operator<<= (::CORBA::Any &, com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr *); // non-copying
 ::CORBA::Boolean operator>>= (const ::CORBA::Any &, com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr &);
TAO_END_VERSIONED_NAMESPACE_DECL



#endif

// TAO_IDL - Generated from
// be/be_visitor_interface/cdr_op_ch.cpp:44

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 ::CORBA::Boolean operator<< (TAO_OutputCDR &, const com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr );
 ::CORBA::Boolean operator>> (TAO_InputCDR &, com::isti::quakewatch::corbaclient::qw_client_rec::QWRecMsgCallBack_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// be/be_visitor_interface/cdr_op_ch.cpp:44

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

 ::CORBA::Boolean operator<< (TAO_OutputCDR &, const com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr );
 ::CORBA::Boolean operator>> (TAO_InputCDR &, com::isti::quakewatch::corbaclient::qw_client_rec::QWPushSubServices_ptr &);

TAO_END_VERSIONED_NAMESPACE_DECL



// TAO_IDL - Generated from
// be/be_codegen.cpp:1703
#if defined (__ACE_INLINE__)
#include "QWPushSubServicesC.inl"
#endif /* defined INLINE */

#endif /* ifndef */

