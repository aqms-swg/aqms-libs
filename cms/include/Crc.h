/** @file
 * @ingroup group_libs_cms
 * @brief Header file for Crc.cc
 */
#ifndef ___crc_h
#define ___crc_h

#include <stdint.h>

//CRC Routines//
uint32_t crc ( unsigned char *buf, int len );
void make_crc_table ( void );
void print_crc_table ( void );
uint32_t update_crc_c ( uint32_t crc, unsigned char c );
uint32_t update_crc_s ( uint32_t crc, unsigned char *buf, int len );
void timestamp ( void );

#endif//___crc_h
