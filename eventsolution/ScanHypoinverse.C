/** @file
 * @ingroup group_libs_eventsolution
 * @brief A fixed format scan routine, which attempts to populate the solution structure
 */
/***********************************************************

File Name :
	ScanHypoinverse.C

Programmer:
	Phil Maechling

Description:
	This is a fixed format scan routine, which attemps
	to populate the solution structure.

Creation Date:
	14 September 1995

Modification History:
	2006/5/17, Pete Lombard: Added several fields that are needed by DB.
	There are still some summary line fields that are not read here,
	but they are either not expected from real-time Earthworm system
	or there is no place to put them in the CISN datbase schema.

	2011/02/17, Pete Lombard: Change to read Preferred Magnitude and 
	type from hypoinverse summary line instead of Duration magnitude.
	This allows you to set rules in hypoinverse (PRE command) to 
	require minimum number of stations for various magnitudes, and
	thus to avoid bogus large Md magnitudes with few noisy stations.
  
         Paul Friberg modification to scan_hinv_message to allow selection of
	the prefmag char instead of hard coding it to D

	2015/02/27, Pete Lombard: Added fields to support geoid depth and
	model depth as well as crust model and type. Requires hypoinverse 1.4
	or newer, and Origin table changes.

Usage Notes:


**********************************************************/
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "RetCodes.h"
#include "seismic.h"
#include "y2k.h"
#include "ScanHypoinverse.h"

using namespace std;

// Scan the hypoinverse summary line; the phase lines are read elsewhere.

int scan_hinv_message(struct hypoinverse_archive_type& sol,char* sumCard, char* pref_mag_chars)
{
    char tempbuf[12];
    memset(&sol, 0, sizeof(struct hypoinverse_archive_type));
    float depth;  // could be model or geoid depth

    /* Date and Time */
 
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[0],(size_t)4); /* yyyy */
    tempbuf[4] = 0;
    sol.year = atoi(tempbuf);

    //
    // Month
    // 
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[4],(size_t)2); /* mm */
    tempbuf[2] = 0;
    sol.month = atoi(tempbuf);

    //
    // Day
    // 

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[6],(size_t)2); /* dd */
    tempbuf[2] = 0;
    sol.day = atoi(tempbuf);

    //
    // Hour
    // 

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[8],(size_t)2); /* hh */
    tempbuf[2] = 0;
    sol.hour = atoi(tempbuf);

    //
    // Minute
    //


    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[10],(size_t)2); /* mm */
    tempbuf[2] = 0;
    sol.minute = atoi(tempbuf);

    //
    // Second
    // 
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[12],(size_t)4); /* ss */
    tempbuf[4] = 0;
    double fsec = atof(tempbuf);
    sol.seconds = (float) (fsec / 100.0);

    //
    // Latitude
    // 

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[16],(size_t)2); /*degrees latitude */
    tempbuf[2] = 0;
    double flt = atof(tempbuf);
    sol.lat_degrees = int (flt);

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[19],(size_t)4); /* minutes latitude */
    tempbuf[4] = 0;
    double ftemp = atof(tempbuf);
    sol.lat_minutes = (float) (ftemp/100.0);
    ftemp = (ftemp/6000.0); /* 100.0 (insert decimal point * 60.0 (degree/ded)  */
    flt = flt + ftemp;
    sol.latitude_dd = (float) flt;
    if (sumCard[18] == 'S') {
	sol.latitude_dd = - sol.latitude_dd;
	sol.lat_degrees = - sol.lat_degrees;
	sol.lat_minutes = - sol.lat_minutes;
    }
 
    //
    // Longitude
    // 

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[23],(size_t)3); /* degrees longitude */
    tempbuf[3] = 0;
    double flong = atof(tempbuf); 
    sol.long_degrees = (int) flong;

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[27],(size_t)4); /* minutes longitude */
    tempbuf[4] = 0;
    ftemp = atof(tempbuf);
    sol.long_minutes = (float) (ftemp/100.0);
    ftemp = ftemp/6000.0; /* adjust decimal point and deg/deg */
    flong = flong + ftemp;
    sol.longitude_dd = (float)flong;
    if (sumCard[26] != 'E') {
	sol.longitude_dd = - sol.longitude_dd;
	sol.long_degrees = - sol.long_degrees;
	sol.long_minutes = - sol.long_minutes;
    }

    //
    // Depths
    //
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[31],(size_t)5); /* depth */
    tempbuf[5] = 0;
    ftemp = atof(tempbuf); 
    depth = (float) (ftemp / 100.0);
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[168],(size_t)1); /* depth type */
    tempbuf[1] = 0;
    if (strcmp(tempbuf, "M") == 0) {
	sol.model_depth = depth;
	sol.have_model_depth = TN_TRUE;
	memset(tempbuf,0,sizeof(tempbuf));
	strncpy(tempbuf,&sumCard[174],(size_t)5); /* geoid depth */
	tempbuf[5] = 0;
	ftemp = atof(tempbuf); 
	sol.geoid_depth = (float) (ftemp / 100.0);
    } else {
	sol.geoid_depth = depth;
	sol.have_model_depth = TN_FALSE;
    }

    // 
    // Number of phases in solution
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[39],(size_t)3); /* Pick count */
    tempbuf[3] = 0;
    sol.number_of_phases_used = atoi(tempbuf);

    //
    // Maxmimum azimuthal gap
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[42],(size_t)3); /* az gap */
    tempbuf[3] = 0;
    sol.max_azimuthal_gap = atoi(tempbuf);


    //
    // Distance to Nearest Station
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[45],(size_t)3); /* dist to nearest station */
    tempbuf[3] = 0;
    ftemp = atof(tempbuf); 
    sol.dist_to_nearest_station = (float) ftemp;

    //
    // Travel Time Residual RMS
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[48],(size_t)4); /* rms */
    tempbuf[4] = 0;
    ftemp = atof(tempbuf); 
    sol.travel_time_residual_rms = (float) (ftemp/100.0);

    //
    // Azimuth of Largest Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[52],(size_t)3);
    tempbuf[3] = 0;
    sol.azimuth_largest_error = (float)atoi(tempbuf);

    //
    // Dip of Largest Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[55],(size_t)2);
    tempbuf[2] = 0;
    sol.dip_largest_error = (float)atoi(tempbuf);

    //
    // Largest Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[57],(size_t)4);
    tempbuf[4] = 0;
    ftemp = atof(tempbuf);
    sol.largest_error = (float)(ftemp / 100.0);

    //
    // Azimuth of Intermediate Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[61],(size_t)3);
    tempbuf[3] = 0;
    sol.azimuth_inter_error = (float)atoi(tempbuf);

    //
    // Dip of Intermediate Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[64],(size_t)2);
    tempbuf[2] = 0;
    sol.dip_inter_error = (float)atoi(tempbuf);

    //
    // Intermediate Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[66],(size_t)4);
    tempbuf[4] = 0;
    ftemp = atof(tempbuf);
    sol.inter_error = (float)(ftemp / 100.0);

    //
    // Duration Magnitude
    //

    // Read the "Preferred magnitude, chosen by the Hypoinverse PRE command."
    // That way, the rules imposed by the PRE command will choose
    // whether the magnitude is good enough.
    // But only use the preferred if it is an Md.
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[147],(size_t)3);
    tempbuf[3] = 0;
    ftemp = atof(tempbuf);
    // pick_ew/eqcoda/hyp2000 can't produce an Md as small as 0
    // yet hyp2000 always puts a number here; don't use it if
    // it isn't believable
    if (strchr(pref_mag_chars, (int)sumCard[146]) != NULL && ftemp > 0.0) {
	sol.have_dur_magnitude = 1;
	sol.dur_magnitude = (float)(ftemp / 100.0);
    } else {
	sol.have_dur_magnitude = 0;
    }

    //
    // Smallest Principal Error
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[76],(size_t)4);
    tempbuf[4] = 0;
    ftemp = atof(tempbuf);
    sol.smallest_error = (float)(ftemp / 100.0);

    //
    // Auxiliary Remark
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[80],(size_t)2);
    tempbuf[2] = 0;
    sol.depth_constrained = (tempbuf[1] == '-') ? 1 : 0;

    // 
    // Number of S Phases In Solution
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[82],(size_t)3); /* Pick count */
    tempbuf[3] = 0;
    sol.number_of_s_phases = atoi(tempbuf);

    //
    // Horizontal error in km
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[85],(size_t)4); /* horizontal error */
    tempbuf[4] = 0;
    ftemp = atof(tempbuf); 
    sol.horizontal_error_km = (float) (ftemp/100.0);

    //
    // Vertical error in km
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[89],(size_t)4); /* vertical error */
    tempbuf[4] = 0;
    ftemp = atof(tempbuf); 
    sol.vertical_error_km = (float) (ftemp/100.0);

    //
    // Quality: NC vs SC difference
    //

    // SC version:
#ifndef NC_NO_EVENT_QUALITY
    memset(tempbuf,0,sizeof(tempbuf));

    if( (sol.horizontal_error_km <= 1.0) &&
	(sol.vertical_error_km <=2.0 ) )
    {
	strncpy(tempbuf,"A",(size_t)1); /* Quality */
    }
    else
	if( (sol.horizontal_error_km <= 2.0) &&
	    (sol.vertical_error_km <=  5.0 ) )
	{
	    strncpy(tempbuf,"B",(size_t)1); /* Quality */

	}
	else
	    if(sol.horizontal_error_km <= 5.0)
	    {
		strncpy(tempbuf,"C",(size_t)1); /* Quality */
	    }
	    else
	    {
		strncpy(tempbuf,"D",(size_t)1); /* Quality */
	    }

    sol.quality = (char) tempbuf[0];
#endif
    // NC version: don't use quality

    // 
    // Number of P First Motions In Solution
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[93],(size_t)3); /* P first motions */
    tempbuf[3] = 0;
    sol.number_of_p_first_motions = atoi(tempbuf);

    // 
    // Number of Weighted Duration Magnitudes In Solution
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[100],(size_t)4); /* weighted dur mags */
    tempbuf[4] = 0;
    sol.number_of_weighted_dur_mags = int(0.1 + atof(tempbuf) / 10.0);

    //
    // Duration Magnitude MAD
    //

    if (sol.have_dur_magnitude) {
	memset(tempbuf,0,sizeof(tempbuf));
	strncpy(tempbuf,&sumCard[107],(size_t)3);
	tempbuf[3] = 0;
	ftemp = atof(tempbuf);
	sol.dur_mag_mad = (float)(ftemp / 100.0);
    }

    //
    // SourceCode char from hyp2000_mgr
    //
    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[114],(size_t)1); /* Source Code char*/
    sol.source_code = (char) tempbuf[0];
    // 
    // Number of input phases
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[118],(size_t)3); /* Pick count */
    tempbuf[3] = 0;
    sol.number_of_phases_input = atoi(tempbuf);

    //
    // Event ID
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[136],(size_t)10); /* event id */
    tempbuf[10] = 0;
    sol.event_id = atoi(tempbuf);

    //
    // Solution Version
    //

    memset(tempbuf,0,sizeof(tempbuf));
    strncpy(tempbuf,&sumCard[162],(size_t)1); /* hypocenter version */
    tempbuf[1] = 0;
    sol.hyp_version = atoi(tempbuf);

    //
    // Domain and Processing Version codes
    //
    strncpy(sol.domain_code, &sumCard[164], (size_t)2);
    sol.domain_code[2] = 0;
    strncpy(sol.processing_version_code, &sumCard[166], (size_t)2);
    sol.processing_version_code[2] = 0;

    //
    // Crust model and model type
    //
    strncpy(sol.crust_model, &sumCard[110], (size_t)3);
    sol.crust_model[3] = 0;    
    strncpy(sol.model_type, &sumCard[169], (size_t)1);
    sol.model_type[1] = 0;


    return(TN_SUCCESS);
}


int insertMagnitude(char hypo_msg[],float magnitude)
{

    char mag_str[10];

    float ten_adjust_mag = magnitude * 100;

    //float hundred_adjust_mag = magnitude * 100;
    //
    // Magnitude
    //

    sprintf(mag_str,"%3.0f",ten_adjust_mag);
    memcpy(&hypo_msg[70],&mag_str[0],3); /* Coda Dur mag */

    return(TN_TRUE);
}

