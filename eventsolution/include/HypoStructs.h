/** @file
 * @ingroup group_libs_eventsolution
 * @brief A structure that defines the contents of a hypoinverse archive message summary line
 */
/***********************************************************

File Name :
	HypoStructs.h

Programmer:
	Phil Maechling

Description:
	This is a structure that defines the contents of a 
	hypoinverse archive message summary line.

Creation Date:
	10 February 1997

Modification History:
	2007/02/03, Pete Lombard: Added several fields that are available
	from the hypoinverse summary line, which we may expect will be filled
        in by the real-time system and used to populate the database.

Usage Notes:


**********************************************************/

#ifndef hypo_structs_H
#define hypo_structs_H

struct hypoinverse_archive_type
{
    int   year;
    int   month;
    int   day;
    int   hour;
    int   minute;
    float seconds;
    int   lat_degrees;
    float lat_minutes;
    float latitude_dd;
    int   long_degrees;
    float long_minutes;
    float longitude_dd;
    float geoid_depth;
    int  have_model_depth;
    float model_depth;
    float azimuth_largest_error;
    float dip_largest_error;
    float largest_error;
    float azimuth_inter_error;
    float dip_inter_error;
    float inter_error;
    float smallest_error;
    float dist_to_nearest_station;
    float travel_time_residual_rms;
    float horizontal_error_km;
    float vertical_error_km;
    float dur_magnitude;
    float dur_mag_mad;
    int   have_dur_magnitude;
    int   number_of_phases_input;
    int   number_of_phases_used;
    int   number_of_s_phases;
    int   number_of_p_first_motions;
    int   number_of_weighted_dur_mags;
    int   max_azimuthal_gap;
    int   quality;
    int   event_id;
    int   hyp_version;
    int   depth_constrained;
    char  domain_code[3];
    char  processing_version_code[3];
    char  model_type[2];
    char  crust_model[4];
    char  source_code;
};

#endif
