/** @file
 * @ingroup group_libs_eventsolution
 * @brief Header file for NetworkTrigger.C
 */
/***********************************************************

File Name :
        NetworkTrigger.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.

	20 Dec 2011 Paul Friberg - added subnetcode option

Usage Notes:


**********************************************************/

#ifndef network_trig_H
#define network_trig_H

// Various include files
#include <map>
#include <stdint.h>
#include "seismic.h"
#include "TimeStamp.h"
#include "TimeWindow.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"
#include "Channel.h"
#include "Trigger.h"


// Definition of an origin list
typedef std::map<Channel, Trigger, std::less<Channel>, std::allocator<std::pair<const Channel, Trigger> > > TriggerList;


class NetworkTrigger
{
 private:

 protected:

 public:
    uint32_t trigid;
    char source[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char locid[DB_MAX_LOCEVID_LEN+1];
    char algorithm[DB_MAX_ALGORITHM_LEN+1];
    char subnet_code[DB_MAX_SUBNET_CODE_LEN+1];
    TimeStamp trigdate;
    int all_chans;
    TimeWindow savewin;
    
    TriggerList trigs;

    NetworkTrigger();
    NetworkTrigger(const NetworkTrigger &n);
    ~NetworkTrigger();

    NetworkTrigger& operator=(const NetworkTrigger &e);
    friend ostream& operator<<(ostream &os, const NetworkTrigger &e);
    friend Logfile& operator<<(Logfile &lf, const NetworkTrigger &e);
    friend StatusManager& operator<<(StatusManager &sm, const NetworkTrigger &e);
};


#endif
