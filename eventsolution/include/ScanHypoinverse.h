/** @file
 * @ingroup group_libs_eventsolution
 * @brief Header file for ScanHypoinverse.C
 */
/***********************************************************

File Name :
	ScanHypoinverse.h

Programmer:
	Phil Maechling

Description:
	This is a fixed format scan routine, which attemps
	to populate the solution structure.

Creation Date:
	14 September 1995

Modification History:


Usage Notes:


**********************************************************/

#ifndef scan_hinv_H
#define scan_hinv_H

#include "HypoStructs.h"

int scan_hinv_message(struct hypoinverse_archive_type& sol,
		      char* input_string, char* pref_mag_chars);

int insertMagnitude(char outmsg[],float magnitude);

#endif
