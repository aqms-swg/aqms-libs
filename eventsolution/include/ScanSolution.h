/** @file
 * @ingroup group_libs_eventsolution
 * @brief Header file for ScanSolution.C
 */
/***********************************************************

File Name :
	ScanSolution.h

Programmer:
	Patrick Small

Description:
	This is a fixed format scan routine, which attemps
	to populate the solution structure.

Creation Date:
	07 Aptril 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef scan_solution_H
#define scan_solution_H

#include "Solution.h"

int scanSolution(struct solution_type &sol, char* input_string);

#endif
