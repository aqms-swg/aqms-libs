/** @file
 * @ingroup group_libs_eventsolution
 * @brief Header file for ScanStation.C
 */
/*	UCB CVS: $Id: ScanStation.h 2148 2007-11-08 08:42:01Z pfriberg $	*/

/***********************************************************

File Name :
	ScanStation.h

Original Author:
	Patrick Small

Description:
	This is a fixed format scan routine, which attemps
	to populate the solution structure.

Creation Date:
	8 September 1998

Modification History:


Usage Notes:


**********************************************************/

#ifndef scan_station_H
#define scan_station_H

#include "StationStructs.h"

int scan_station_message(struct station_archive_type& sol,
			 char* sumCard, char* shadowCard);

#endif
