/** @file
 * @ingroup group_libs_eventsolution
 * @brief A structure used by ISAIAH's hyp_server
 */
/***********************************************************

File Name :
	solutions.h

Programmer:
	Phil Maechling

Description:
	This is a structure used by ISAIAH's hyp_server

Creation Date:
	9 September 1995

Modification History:


Usage Notes:


**********************************************************/

#ifndef solution_H
#define solution_H

struct solution_type
{
  int   event_id;
  int   year;
  int   month;
  int   day;
  int   hour;
  int   minute;
  float seconds;
  float latitude;
  float longitude;
  float depth;
  float solution_rms;
  int   solution_phases;
  int   solution_version;
  int   solution_status;
  float magnitude;
  char  magnitude_type[4];
  char  magnitude_method[5];
  float magnitude_rms;
  int   channels_in_magnitude;
  char  solution_source[4];
  int   solution_count;
};

#endif
