/** @file
 * @ingroup group_libs_eventsolution
 * @brief A structure that defines the contents of a station archive message
 */
/***********************************************************

File Name :
	StationStructs.h

Original Author:
	Patrick Small

Description:
	This is a structure that defines the contents of a 
	station archive message.

Creation Date:
	8 September 1998

Modification History:
	2007/02/03, Pete Lombard: added compile-time-optional fields 
	for coda durations.

Usage Notes:


**********************************************************/

#ifndef station_structs_H
#define station_structs_H

// This station_archive_type structure contains phase arrival information
// read from a hypoinverse archive file.
struct station_archive_type
{
    // Phase section
    int   year;
    int   month;
    int   day;
    int   hour;
    int   minute;
    int   pweight;
    int   sweight;
    float pseconds;
    float presid;
    float pweightactual;
    float pdelay;
    float importparr;
    float sseconds;
    float sresid;
    float sweightactual;
    float sdelay;
    float importsarr;
    float epidist;
    float emergang;
    float azimuth;
    char sta[6];
    char comp[4];
    char net[3];
    char location[3];
    char premark[3];
    char sremark[3];
    char pfirstmo;
    char source;
    
#ifdef CODAS_FROM_PICKEW
    // Coda section
    float afix;           /* Nominal coda amplitude */
    float qfix;           /* Fixed coda decay constant */
    float afree;          /* Free amplitude */
    float qfree;          /* Free decay */
    float coda_rms;       /* RMS for coda fit */
    float dur_mag;        /* Station duration magnitude */
    int	  mag_weight;     /* Magnitude weight */
    int   nsample;        /* Number of code sample windows */
    int   tau;            /* Coda duration */
    int   time1;          /* Time for pair 1 */
    int   amp1;           /* Amplitude for pair 1 */
    int   time2;          /* Time for pair 2 */
    int   amp2;           /* Amplitude for pair 2 */
    int   time3;          /* Time for pair 3 */
    int   amp3;           /* Amplitude for pair 3 */
    int   time4;          /* Time for pair 4 */
    int   amp4;           /* Amplitude for pair 4 */
    int   time5;          /* Time for pair 5 */
    int   amp5;           /* Amplitude for pair 5 */
    int   time6;          /* Time for pair 6 */
    int   amp6;           /* Amplitude for pair 6 */
    char  coda_phase[2];
#endif    

};

#endif
