/** @file
 * @ingroup group_libs_eventsolution
 */
/***********************************************************

File Name :
        Trigger.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef trigger_H
#define trigger_H

// Various include files
#include <stdint.h>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "TimeWindow.h"
#include "Logfile.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"


// Enumeration of the valid types of first motions
typedef enum {TRIG_UNKNOWN, TRIG_TRIG, TRIG_MEMBER} trigType;


// Definition of the maximum amp type enumeration
const int TRIG_MAX_TRIG = 3;


// String descriptions of each amplitude type
const char trigTypeStrings[TRIG_MAX_TRIG][16] = {"Unknown", "Triggered",
						 "Member"};

// Database string descriptions of each amplitude type
const char trigTypeDBStrings[TRIG_MAX_TRIG][DB_MAX_TRIGFLAG_LEN+1] = {"u", 
								      "t", 
								      "m"};



class Trigger
{
 private:

 protected:

 public:
    uint32_t trigid;
    trigType trigflag;
    TimeStamp trigtime;
    TimeWindow savewin;

    Trigger();
    Trigger(const Trigger &t);
    ~Trigger();

    Trigger& operator=(const Trigger &t);
    friend int operator<(const Trigger &t1, const Trigger &t2);
    friend int operator>(const Trigger &t1, const Trigger &t2);

    friend ostream& operator<<(ostream &os, const Trigger &t);
    friend Logfile& operator<<(Logfile &lf, const Trigger &t);
    friend StatusManager& operator<<(StatusManager &sm, const Trigger &t);
};


#endif
