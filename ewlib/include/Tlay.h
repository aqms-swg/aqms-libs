/** @file
 * @ingroup group_libs_ewlib
 * @brief Header file to Tlay.C
 */
/***********************************************************

File Name :
        Tlay.h

Original Author:
        Paul Friberg

Description:


Creation Date:
        2 May 2003

Modification History:


Usage Notes:


**********************************************************/

#ifndef Tlay_H
#define Tlay_H

// include some earthworm includes here
extern "C" {    
#include "tlay.h"
}


class Tlay {
 private:
    int is_initialized;
    char *model_filename;
 public:

    Tlay();
    ~Tlay();
    int ParseVelocityModel(char *file);
	// the following return the earliest arrivals
    double GetPWaveTravelTime(double dist, double depth);
    double GetSWaveTravelTime(double dist, double depth);
	// the following return just the P and S direct phase
    double GetDirectPWaveTravelTime(double dist, double depth);
    double GetDirectSWaveTravelTime(double dist, double depth);
};

#endif
