/** @file
 * @ingroup group_libs_ewlib
 * @brief Header file for ewLogA0.C
 */
#ifndef _ewloga0
#define _ewloga0 1

/* this is a C++ class to mimic the logA0 function that the
	earthworm (ew) module localmag from Peter Lombard.

	This class borrows heavily from Pete's localmag code.

*/


#define NO_MAG -9.0

typedef struct _LOGA0
{
  int dist;
  double val;
} LOGA0;


class ewLogA0 {
public:
	ewLogA0( char * filename);
	~ewLogA0();
	float getEWla100();
	float getEWlogA0(float );
        double GetMagFromMag100(double, double );
private:
	LOGA0 *gLa0tab;		/* the distance logA0 values */
        int nLtab;		/* number of entries in LogA0 table */
	float la100;		/* cached value of logA0 at 100km epicentral dist and 8km depth for rad */
};
#endif
