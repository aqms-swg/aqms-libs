/** @file
 * @ingroup group_libs_gcda
 * @brief Accepts a configuration file name and a station and channel name, and returns a populated channel def structure if it can be found
 */
/***********************************************************

File Name : findchancfg.C


Programmer:
	Phil Maechling

Description:
	Find Channel config routine. This accepts a configuration
	file name and a station and channel name, and returns
	a populated channel def structure if it can be found.

Creation Date:
	17 September 1996


Usage Notes:



Modification History:
	14 September 1998
		Added network and telemetry type.

**********************************************************/
#include <iostream>
#include <cstdio>
#include <cstring>
#include "RetCodes.h"
#include "findchancfg.h"
#include "chan.h"

using namespace std;

int find_channel_config(char* cfg_file,
                       char* in_network,
                       char* in_station,
		       char* in_channel,
		       char* in_location,
                       struct channel_definition_type& channel_def,
		       int debug_flag)
{
  FILE *cfgfp;
  cfgfp = fopen(cfg_file,"r");
  if (cfgfp == NULL)
  {

    std::cout << "Error opening the configuration file" << std::endl;
    std::cout << "Failed opening " << cfg_file << std::endl;
    return(TN_FAILURE);
  }

  char inputline[MAX_CHARS_IN_FILE_LINE];

  // Define scanned values

  int res;
  int res1;
  int res2;
  int res3;
  char* cres;
 
  int station_channel_not_found = TN_TRUE;

  while (( !feof(cfgfp) ) && (station_channel_not_found)) 
  {

    cres = fgets(inputline,sizeof(inputline),cfgfp);

    if (feof(cfgfp))
    {
      std::cout << "Found end of channel configuration file" << std::endl;
      fclose(cfgfp);
      return(TN_FAILURE);
    }    

    if (cres == NULL)
    {
      std::cout << "Error scanning channel configuration file" << std::endl;
      fclose(cfgfp);
      return(TN_FAILURE);
    }

    // Check for leading # 
    
    res = strncmp("#",inputline,1);
    if(res == 0)
    {
      // Found comment line
      continue;
    } 
  
    char  network[MAX_CHARS_IN_NETWORK_STRING];
    char  station[MAX_CHARS_IN_STATION_STRING];
    char  channel[MAX_CHARS_IN_CHANNEL_STRING];
    char  location[MAX_CHARS_IN_LOCATION_STRING];
    char  telemetry_type[CHARS_IN_CHANNEL_HEADER_STRING];
    float latitude,longitude,elevation;
    float sample_period;
    int   calibrated;
    float gain_factor,mlcor,mecor;

    res = sscanf(inputline,"%s %s %s %s %s %f %f %f %f %f %f %f %d",
	         network,station,channel,location,telemetry_type,
		 &latitude,&longitude,&elevation,
                 &sample_period,
                 &gain_factor,
                 &mlcor,
		 &mecor,
		 &calibrated);

    
    //    cout << "  latitude,longitude,elevation, sample_period,  gain_factor, mlcor, mecor " << endl;
    //    cout << " " << latitude << " " << longitude << " " << elevation << " " <<  sample_period << " " <<   gain_factor << " " <<  mlcor << " " <<  mecor  << endl;		

    if (res != 13)
    {
      std::cout << "Error scanning channel configuration file" << std::endl;
      return(TN_FAILURE);
    }

    // convert "-" to "" for location
    mapLC(network,location, MEMORY);

    // std::cout << "DEBUG: searching for "<<in_network<<"."<<in_station<<"."<<in_channel<<".'"<<in_location<<"'"<<std::endl;
    // std::cout << "DEBUG: comparing against "<<network<<"."<<station<<"."<<channel<<".'"<<location<<"'"<<std::endl;

    res = strcmp(station,in_station);
    res1 = strcmp(channel,in_channel);
    res2 = strcmp(network, in_network);
    res3 = strcmp(location, in_location);

    if ((res == 0) && (res1 == 0) && (res2 == 0) && (res3 == 0))
    {
      station_channel_not_found = TN_FALSE;
      fclose(cfgfp);

      // Move to calling structure

      strcpy(channel_def.network,network);
      strcpy(channel_def.station,station);
      strcpy(channel_def.seed_channel,channel);
      strcpy(channel_def.location,location);
      strcpy(channel_def.telemetry_type,telemetry_type);

      channel_def.latitude          = latitude;
      channel_def.longitude         = longitude;
      channel_def.elevation         = elevation;
      channel_def.sample_period     = sample_period;
      channel_def.valid_corrections = calibrated;
      channel_def.gain_factor       = gain_factor;
      channel_def.ml_correction     = mlcor;
      channel_def.me_correction     = mecor;
      channel_def.clip_level_counts = 0;	// this cannot be guessed from the tridefs.cfg yet...and probably won't be

      //
      // Interpret the Channel name and assign a instrument type
      //
      int instrument_type;
      int hres = strncmp(&channel[1],"H",1);
      int sres = strncmp(&channel[1],"S",1);
      int fres = strncmp(&channel[1],"F",1);
      if ( (hres == 0) ||
	   (sres == 0) ||
           (fres == 0) )
      {
        instrument_type = BROADBAND;
      }
      else
      { 
	  int lres = strncmp(&channel[1],"L",1);
	  int nres = strncmp(&channel[1],"N",1);
	  if ( (lres == 0) ||
	       (nres == 0) )
        {
          instrument_type = STRONG_MOTION;
        }
        else
        {
          std::cout << "Unable to classify instrument type : " << channel <<std::endl;
          return(TN_FAILURE);
        }
      }

      channel_def.instrument_type = instrument_type;

      // Dump contents for logging purposes

      if(debug_flag)
      {
        std::cout << "Network        : " 
         << channel_def.network << std::endl;
        std::cout << "Station        : " 
         << channel_def.station << std::endl;
        std::cout << "Channel        : " 
         << channel_def.seed_channel << std::endl;
        std::cout << "Location        : " 
         << mapLC(channel_def.network,channel_def.location,ASCII) << std::endl;
        mapLC(channel_def.network,channel_def.location, MEMORY);	 // map it back if necessary
	std::cout << "Telemetry_Type : "
	  << channel_def.telemetry_type << std::endl;
        std::cout << "Latitude       : " << 
          channel_def.latitude << std::endl;
        std::cout << "Longitude      : " 
         << channel_def.longitude << std::endl;
        std::cout << "Elevation      : " 
         << channel_def.elevation << std::endl;
        std::cout << "Instrument Type : "
         << channel_def.instrument_type
	 << std::endl;
        std::cout << "Sample Period   : " 
         << channel_def.sample_period 
	 << std::endl;
        std::cout << "Valid Corrections : " <<
          channel_def.valid_corrections << std::endl;
        std::cout << "Gain Factor      : " 
         << channel_def.gain_factor  << std::endl;
        std::cout << "Ml Correction    : " 
         << channel_def.ml_correction 
	 << std::endl;
        std::cout << "Me Correction : " << channel_def.me_correction 
	<< std::endl;
        std::cout << "" << std::endl;
      }
    }
  } // end reading configuration file

  return(TN_SUCCESS);

}
