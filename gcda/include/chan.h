/** @file
 * @ingroup group_libs_gcda
 * @brief Channel definition. Collects all the information needed to define a channel into a single structure
 */
/***********************************************************

File Name :
	chan.h

Programmer:
	Phil Maechling

Description:
	Channel Definition Type.
	This collects all the information needed to define a channel
	into a single structure. Presumably, the configuration file
	will contain this information for each channel in the network.


Creation Date:
	7 August 1996

Modification History:


Usage Notes:


**********************************************************/

#ifndef chan_H
#define chan_H

#include "seismic.h"
#include "nscl.h"



struct channel_definition_type
{
  char  network[MAX_CHARS_IN_NETWORK_STRING];
  char  station[MAX_CHARS_IN_STATION_STRING];
  char  seed_channel[MAX_CHARS_IN_CHANNEL_STRING];
  char  location[MAX_CHARS_IN_LOCATION_STRING];
  char  gain_units[MAX_CHARS_IN_GAIN_UNITS_STRING]; 	/* new - for storing units related to instrument_type determination */

  char  telemetry_type[MAX_CHARS_IN_STATION_STRING]; /* Types are A or D */
  float latitude;
  float longitude;
  float elevation;
  float sample_period;
  int   instrument_type;   // 0 = Broad band 1 = Strong Motion
  int   valid_corrections; // 0 = False 1 = TRUE;
  float gain_factor;
  float ml_correction;
  float me_correction;
  int  clip_level_counts; // the counts at which this instrument could clip (used for BB now)
};

#endif
