/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for chancfg.C
 */
/***********************************************************

File Name :
	chancfg.h

Programmer:
	Phil Maechling

Description:
	Scan channel config file routine. 
	This accepts a configuration file name and a an
	array of channel structures and loads the stream
	structures, and passed the array back to the calling program.

Creation Date:
	8 August 1996

Usage Notes:


Modification History:


**********************************************************/
#ifndef chancfg_H
#define chancfg_H

#include "seismic.h"
#include "nscl.h"

struct channel_identifier_type
{
  char network[MAX_CHARS_IN_NETWORK_STRING];
  char station[MAX_CHARS_IN_STATION_STRING];
  char channel[MAX_CHARS_IN_CHANNEL_STRING];
  char location[MAX_CHARS_IN_LOCATION_STRING];
};


int scan_channel_config_file(char* cfg_file,
			     struct channel_identifier_type chans[],
			     int& number_of_channels_found,
			     int debug_flag);

#endif
