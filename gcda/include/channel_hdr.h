/** @file
 * @ingroup group_libs_gcda
 * @brief Shared channel description between the writer and the reader processes
 */
/***********************************************************

File Name :
	channel_hdr.h

Programmer:
	Phil Maechling

Description:
	This is the shared channel description between the writer
	and the reader processes.

Limitations or Warnings:


Creation Date:
	22 March 1995

Modification History:
	14 Octo 1996
	
**********************************************************/
#ifndef channel_header_h
#define channel_header_h

#include <iostream>
#include "seismic.h"
#include "nscl.h"

// leaptimeval looks like a Unix timeval structure, but it is not the same.
// The leaptimeval.tv_sec member INCLUDES leapseconds, where as the
// Unix timeval.tv_sec member does not include leapseconds.
typedef struct
{
    int32_t    tv_sec;    /* seconds since Jan. 1, 1970 INCLUDING LEAPSECONDS */
    int32_t    tv_usec;   /* and microseconds */
} leaptimeval;

    

struct Channel_Header
{
    char          network[MAX_CHARS_IN_NETWORK_STRING];
    char          station[MAX_CHARS_IN_STATION_STRING];
    char          channel[MAX_CHARS_IN_CHANNEL_STRING]; 
    char          location[MAX_CHARS_IN_LOCATION_STRING];
    float         samples_per_second;
    unsigned int  seconds_in_memory;
    unsigned int  channel_samples_in_memory;

    unsigned int  offset_to_first_sample;
    unsigned int  offset_to_last_sample;

    unsigned int  position_of_newest_sample;
    unsigned int  position_of_oldest_sample;

    leaptimeval   time_of_newest_sample;
    leaptimeval   time_of_oldest_sample;

    unsigned int  offset_to_first_validity_flag;


};

#endif
