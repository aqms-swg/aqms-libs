/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for dchan.C
 */
/***********************************************************

File Name :
	dchan.h

Programmer:
	Phil Maechling

Description:
	This is a base class representing a data channel.

	Ideally the constructor would be called with memmap, 
	station, and channel info. However, the constructor can't
	return a result, so the initialize routine does the work.
	The result of the initialize routine is to populate the
	private data variables with information on how to access
	the data channel mentioned. Derived classes are writers and
	readers which have their own functions to access the data.

        The Data Channel Writer, and Data Channel Reader were
	declared friends for the following reasons.
        We want Writer and readers to have access to the member
        functions in the base class. They are TRUSTED to lock and
        unlock when they are accessing the functions.
        We don't want others calling base class functions, because
        they may not lock and unlock properly.


Limitations or Warnings:


Creation Date:
	11 July 1996

Modification History:

**********************************************************/
#ifndef dchan_H
#define dchan_H

#include <string>
#include <sys/shm.h>
#include <sys/types.h>
#include "TimeStamp.h"
#include "channel_hdr.h"
#include "nsem.h"
#include "seismic.h"

template <class T> class Data_Channel_Writer;
template <class T> class Data_Channel_Reader;
template <class T> class Data_Channel_Monitor;
template <class T> class Defined_Data_Channel_Reader;

template <class T> class Data_Channel
{

    friend class Data_Channel_Writer<T>;
    friend class Data_Channel_Reader<T>;
    friend class Data_Channel_Monitor<T>;
    friend class Defined_Data_Channel_Reader<T>;

 public:
    
    Data_Channel(key_t key);

    virtual ~Data_Channel();

    int Initialize_Channel(char* memmap,char * network, char* station,char* channel, char * location);

    int Set_Debug_On(int debug_on);

    TimeStamp      time_of_newest_sample_in_cda();
    TimeStamp      time_of_oldest_sample_in_cda();

   //
   // A read routine used for checking config file rate against
   // data rate given in data packets.
   //

   float Rate_in_Samples_per_Second();

 private:

    unsigned int offset_to_first_validity_flag_in_cda();
    unsigned int offset_to_first_sample_in_cda();

    // 
    // Reporter functions giving station and channel
    //

    int name_of_network(char* net);
    int name_of_station(char* sta);
    int name_of_channel(char* chan);
    int name_of_location(char* location);

    // newest sample pointer's
 	
    void         set_time_of_newest_sample_in_cda(TimeStamp newtime);

    unsigned int position_of_newest_sample_in_cda();
    void         set_position_of_newest_sample_in_cda(unsigned int position);

    unsigned int position_of_next_new_sample_in_cda();


    // oldest sample pointer's 

    void         set_time_of_oldest_sample_in_cda(TimeStamp time_to_set);

    unsigned int position_of_oldest_sample_in_cda();
    void         set_position_of_oldest_sample_in_cda(unsigned int position);


    // This is float to allow sample rates less than a sample per second

    float        Samples_per_Second();


    // This returns a double to allow large periods of time
    // to be represented.

    double samples_in_time_period(double usecs_in_time_diff);

    // 
    // These are unsigned because the are conceptually always positive
    //
    unsigned int channel_samples_in_memory();
    unsigned int Samples_in_Memory();
    unsigned int Seconds_in_Memory();
    unsigned int usecs_per_sample();

    // 
    // The following routines do nearly identical things. However,
    // the first returns an inclusive time, the second returns an
    // exclusive time.
    //

    TimeStamp time_of_last_sample_in_window(const TimeStamp first_sample_time,
				          const int number_of_samples);

    TimeStamp time_after_samples_end(const TimeStamp first_sample_time,
			           const int number_of_samples);

    TimeStamp time_of_last_sample_in_full_queue(const TimeStamp 
					      time_of_newest_samples);

    //
    // We need for gcda return the actual time of the sample.
    //
    unsigned int cda_position_for_time(TimeStamp this_time);
    unsigned int cda_position_for_time(TimeStamp this_time, TimeStamp & actual_time);

    char*   Start_of_CDA();

    // The semaphore is left visible so that a client can
    // get a lock, then do a sequence of operations, then release
    // the lock

    Semaphores channel_sem;

    key_t mem_key;
    char* start_of_memmap;
    Channel_Header* this_channel_header;

 public:
    int p_debug;
};

#endif
