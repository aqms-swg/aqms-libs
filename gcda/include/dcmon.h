/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for dcmon.C
 */
/***********************************************************

File Name :
	dcmon.h

Programmer:
	Phil Maechling

Description:
	dcmon : Data Channel Monitor
	This is a derived class, derived from a data channel class.
	This class will return text strings about the specified channels's
	status.

Limitations or Warnings:


Creation Date:
	29 July 1996

Modification History:

**********************************************************/
#ifndef dcmon_H
#define dcmon_H

#include "TimeStamp.h"
#include "dchan.h"

template <class T> class Data_Channel_Monitor : public Data_Channel<T>
{
  public:
    
    Data_Channel_Monitor(key_t key);
    ~Data_Channel_Monitor();

    char* network_name(char* );
    char* station_name(char* );
    char* location_name(char* );
    char* channel_name(char* );

    char* samples_per_second(char* );
    float samples_per_second();

    char* seconds_per_sample(char* );
    float seconds_per_sample();

    char* seconds_in_memory(char* );
    unsigned int seconds_in_memory();

    char* samples_in_memory(char* );
    unsigned int samples_in_memory();

    char*  time_of_oldest_data_in_cda(char* );
    TimeStamp time_of_oldest_data_in_cda();

    char*  time_of_newest_data_in_cda(char* );
    TimeStamp time_of_newest_data_in_cda();

    char* position_of_oldest_data_in_cda(char* );
    unsigned int position_of_oldest_data_in_cda();

    char* position_of_newest_data_in_cda(char* );
    unsigned int position_of_newest_data_in_cda();

    char* channel_is_empty(char* );
    int channel_is_empty();

    char* number_of_invalid_samples_in_cda(char* );
    unsigned int number_of_invalid_samples_in_cda();

  private:

};

#endif
