/** 
 * @file
 * @ingroup group_libs_gcda
 * @brief Header file for ddcr.C
 */
/***********************************************************

File Name :
	ddcr.h

Programmer:
	Phil Maechling

Description:
	ddcr - Define Data Channel Reader
	This is a derived class, derived from a data channel reader class.
	This class will be able to read data from the a data channel
	in a cda. In addition, it contains information about the
	data channel which is used in parametric calculations.

Limitations or Warnings:


Creation Date:
	17 September 1996

Modification History:
	24 March 2004
	Added an overloaded Initialize_Defined_Channel method that uses
	a database connection to obtain channel_definition_type information.
	Pete Lombard, UCB

	12 May 2008 Paul Friberg added gain_units tracking and get_gain_units() func

**********************************************************/
#include <sys/shm.h>
#include "channel_hdr.h"
#include "dchan.h"
#include "dreader.h"
#include "chan.h"
#include "ChannelReaderDB.h"

#ifndef ddcr_H
#define ddcr_H


template <class T> class Defined_Data_Channel_Reader: 
	public Data_Channel_Reader<T>
{
  public:
    
  Defined_Data_Channel_Reader(key_t key);
  virtual ~Defined_Data_Channel_Reader();

  int Initialize_Defined_Channel(char* memmap,char * network, char *station, 
				 char* channel, char * location,
				 char* channel_config);
  
  int Initialize_Defined_Channel(char* memmap,char * network, char *station, 
				 char* channel, char * location,
				 ChannelReaderDB &db);
  
  //
  // This class requires a gcda_file, and a channel configuration file
  // or database connection
  // to be created. The implication is that if an object of this
  // type exists, the data_channel_structure has been filled in,
  // and the get routines have something to get. There are no set
  // routines because all population is done on construction.
  //

  int   get_network_id(char* network_id);
  char* get_network_id();

  int   get_station_id(char* station_id);
  char* get_station_id();

  int   get_seed_channel(char* seed_channel);
  char* get_seed_channel();

  int   get_location_id(char* location_id);
  char* get_location_id();

  int   get_gain_units(char* gain_units);
  char* get_gain_units();

  float get_latitude();
  float get_longitude();
  float get_elevation();
  float get_sample_period();
  int   get_instrument_type();
  int   valid_corrections();
  float get_gain_factor();
  float get_ml_correction();
  float get_me_correction();
  int get_clip_counts();
  TimeStamp get_time_of_newest_sample();

  private:

  //
  // The items in this structure are read out of a configuration file
  //
  struct channel_definition_type p_channel_def;

  //
  // This is establised at runtime by a call to data channel class
  //
  unsigned int p_usec_per_sample;

};

#endif
