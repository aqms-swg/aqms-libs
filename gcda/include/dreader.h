/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for dreader.C
 */
/***********************************************************

File Name :
	dreader.h

Programmer:
	Phil Maechling

Description:
	dreader : Data Channel Reader
	This is a derived class, derived from a data Channel class.
	This class will be able to read data from the a data channel
	in a cda.

Limitations or Warnings:


Creation Date:
	30 July 1996

Modification History:

**********************************************************/
#include <sys/shm.h>
#include "TimeStamp.h"
#include "channel_hdr.h"
#include "dchan.h"

#ifndef dreader_H
#define dreader_H


const int MAXIMUM_SAMPLES_RETURNED = 360000;

template <class T> class Data_Channel_Reader: public Data_Channel<T>
{
  public:
    
    Data_Channel_Reader(key_t key);
    virtual ~Data_Channel_Reader();

//
// These routines return TN_SUCCESS or TN_FAILURE
// TN_SUCCESS means that valid data was retrieved, and at least one
// valid sample was requested.
//  

    int Get_Sample(const TimeStamp requested_time_of_sample, 
                   TimeStamp& actual_time_of_sample,
		   T& sample);

    int Get_Contiguous_Samples(const TimeStamp request_start_time,
	                       const int number_of_requested_samples,
			       TimeStamp& time_of_first_sample,
			       int& number_of_returned_samples,	
	                       T samples[]);

    int Get_Contiguous_Samples(const TimeStamp request_start_time,
	                       const int duration_seconds,
			       const int duration_usecs,
			       TimeStamp& time_of_first_sample,
			       int& number_of_returned_samples,	
	                       T samples[]);


    int Get_All_Available_Samples(const TimeStamp request_start_time,
	                          const int number_of_requested_samples,
			          int& number_of_returned_samples,
				  TimeStamp sampletimes[],
	                          T samples[]);

    int Get_All_Available_Samples(const TimeStamp request_start_time,
	                          const int duration_seconds,
			          const int duration_usecs,
			          int& number_of_returned_samples,
				  TimeStamp sampletimes[],
	                          T samples[]);


    //
    // This routine returns TN_SUCCESS or TN_FAILURE.
    // Success means it has a valid sample.
    // Failure means it has moved all the way up to the
    // most recent data in the cda, and it can't find any
    // new data. Invalid data is skipped over, and the gaps flag
    // is set TN_TRUE
    //
    // After a failed call, it is the caller's responsiblity to wait
    // a resonable length of time (2 * sample_periods maybe) before calling
    // this routine again. Otherwise the application will be in quite
    // a busy wait loop.
    //

    int Get_Next_Sample(TimeStamp& time_of_sample,
			T& next_sample,
			int& time_gap_since_last_sample);


    //
    // This version skips the old data, and starts with the newest
    // sample. This avoids printing out all the old data when you
    // start up a client.
    //

    int Get_Next_New_Sample(TimeStamp& time_of_sample,
			    T& next_sample,
			    int& time_gap_since_last_sample);


    //
    // This routine will retrieve the newest unread sample in the gcda.
    // This ignores any samples other than newest sample.
    //

    int Get_Newest_Unread_Sample(TimeStamp& time_of_sample,
			         T& next_sample);



    //
    // This allows you to set the time of the
    // last sample read. Now when you call next sample, it will
    // take up at this point.
    //

    int Set_Time_of_Last_Sample_Read(TimeStamp& time_of_sample);


    // This flushes the Newest read so the next loop gets the "newest" 
    // sample and not just the NEXT sample...if this is not called, then
    // Get_Next_New_Sample() will operate as normal.
    int Reset_Next_New();


  private:

    //
    // This routine offers contigous_data_mode or
    //  or  all_available_data_mode
    //

    int Common_Get_Samples(const TimeStamp request_start_time,
	                   const int  number_of_requested_samples,
			   const int  mode,
		           TimeStamp  time_array[],
			   int& number_of_returned_samples,	
	                   T samples[]);

    //
    // This mode options allows either get next (mode 0) or
    // get next new (mode 1).
    // 

    int Common_Get_Next_Sample(TimeStamp& time_of_sample,
			       T& next_sample,
			       int& time_gap_since_last_sample,
			       const int mode);


    int     p_first_time_through_get_next_sample;
    TimeStamp p_time_of_last_processed_sample;
    TimeStamp p_time_of_last_new_sample_read;
    unsigned int p_position_of_last_sample_for_channel();
};

#endif
