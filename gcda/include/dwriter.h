/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for dwriter.C
 */
/***********************************************************

File Name :
	dwriter.h

Programmer:
	Phil Maechling

Description:
	dwriter : Data Writer
	This is a derived class, derived from a data channel class.
	This class will be able to write data into the data array.

Limitations or Warnings:


Creation Date:
	11 July 1996

Modification History:

**********************************************************/
#include <sys/shm.h>
#include "TimeStamp.h"
#include "channel_hdr.h"
#include "dchan.h"

#ifndef dwriter_H
#define dwriter_H

template <class T> class Data_Channel_Writer: public Data_Channel<T>
{

  public:
    
    Data_Channel_Writer(key_t key);
    ~Data_Channel_Writer();

    // Put a single sample into the cda.

    int Put_Sample(const TimeStamp time_of_sample, const T& sample);

   // Put a series of contiguous samples in the cda
   // The pointer to the samples is to allow a variable
   // length array.

    int Put_Samples(const TimeStamp time_of_first_sample, 
	        const int number_of_samples,
	        const T samples[]);

  private:

   void write_sample(unsigned int, const T&);
   void write_samples(unsigned int position, const T samples[], 
		     unsigned int number_of_samples);
   void write_sample_as_invalid(unsigned int);
   void adjust_oldest_sample_info(unsigned int start_position,
				  unsigned int number_of_samples,
				  unsigned int n_samples_in_mem);

};

#endif
