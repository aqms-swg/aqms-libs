/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for findchancfg.C
 */
/***********************************************************

File Name :
	findchancfg.h

Programmer:
	Phil Maechling

Description:
	scan channel config file routine. 
	This accepts a network, station, channel, location name and a 
	channel_definition_type structure. 
	It looks through the file and tries to find the	matching entry. 
	When it does, it populates the structure and returns TN_SUCCESS;
	otherwise it returns TN_FAILURE.

Creation Date:
	17 September 1996

Usage Notes:


Modification History:


**********************************************************/
#ifndef scanchancfg_H
#define scanchancfg_H

#include "chan.h"

int find_channel_config(char* cfg_file,
		       char * network,
		       char* station,
		       char* channel,
		       char* location,
		       struct channel_definition_type& channel_def,
		       int debug_flag);

#endif
