/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for gcda.C
 */
/***********************************************************

File Name :
	gcda.h

Programmer:
	Phil Maechling

Description:

	gcda - generic contiguous data area.

	This defines the gcda map class, an updated and improved
	version of the original memmap.

	This routine reads the memory map configuration file (wda.cgf) 
	and then creates a shared memory area based on configuration
	file or database. The constructor creates the shared memory area,
	and the destructor removes it.

	CDA stands for Contiguous data area. This implies that
	the ring buffer created by this routines, contains evenly
	spaced samples of data. Values are put into the area. The data
	in the buffer can be consider "contiguous". Missing data is
	flagged as invalid.


Limitations or Warnings:


Creation Date:
	9 July 1996

Modification History:

**********************************************************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string>
#include <vector>

#include "channel_hdr.h"
#include "seismic.h"
#include "nsem.h"
#include "Database.h"
#include "nscl.h"
#include "RetCodes.h"

#ifndef gcda_H
#define gcda_H

#define DEF_GCDA_KEY_FILE "gcda.cfg"
#define GCDA_FILE_ENV_VAR "GCDA_FILE"
#define GCDA_KEY_NAME_LEN 32

// channel_gcda_type holds data needed to configure a GCDA
struct channel_gcda_type
{
    char  network[MAX_CHARS_IN_NETWORK_STRING];
    char  station[MAX_CHARS_IN_STATION_STRING];
    char  seedchan[MAX_CHARS_IN_CHANNEL_STRING];
    char  location[MAX_CHARS_IN_LOCATION_STRING];
    double         samples_per_second;
    unsigned int   seconds_in_memory;
    unsigned int   channel_samples_in_memory;
};
typedef std::vector<struct channel_gcda_type, 
		    std::allocator<struct channel_gcda_type> >ChannelArray;


template<class T> class Generic_CDA
{
 public:

    // These two constructors would have the same signature
    // if we had key_name as a char*. Since we need to support
    // the old, config_file base constructor, we make Key_name
    // a string reference. Kludge!
    Generic_CDA(const std::string &key_name, int debug_flag = TN_FALSE);
    //    Generic_CDA(char *config_file, int debug_flag = TN_FALSE);
    Generic_CDA(const std::string &key_name, char* config_name, Database &db, 
		int debug_flag, int seconds_in_memory, 
		double override_sample_rate = -1.0);
    ~Generic_CDA();
    int Delete_Memory_Area(); 
    char* Start_of_Memory_Area();
    int Set_Debug_On(int debug_flag);
    key_t Get_Memory_Key();
    int Scan_Channel_Headers(ChannelArray &channels);
    
 private:
    
    int Lookup_Memory_Key(const char *key_name);
    key_t Get_Memory_Key(char *config_file);
    int Get_Channel_List(char *config_name, Database &db, 
			 ChannelArray &channels, double override_samp_rate);
    int Determine_Size_of_Memory_Area(ChannelArray& channels, 
				      int seconds_in_memory);
    int Populate_Header(ChannelArray &channels);
    unsigned int Samples_in_Memory(int seconds_in_memory,
				   double samples_per_second);

    
    char* start_of_memmap;
    key_t cda_key;
    int shmid;  
    Semaphores first_sem;
    int p_debug;
};

// convenient time converters
TimeStamp ltv_to_ts(leaptimeval &ltv);
leaptimeval ts_to_ltv(TimeStamp &ts);
  

#endif
