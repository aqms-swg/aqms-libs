/** @file
 * @ingroup group_libs_gcda
 * @brief Header file for nsem.C
 */
/***********************************************************

File Name :
	nsem.h

Programmer:
	Phil Maechling

Description:
	This is the class definition for the semaphore class. 
	This implementation of semaphores provides both reader (shared)
	and writer (exclusive) locks.

Limitations or Warnings:


Creation Date:
	27 March 1995

Modification History:

**********************************************************/
#ifndef nsem_H
#define nsem_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

class Semaphores
{
  public:

    Semaphores();
    ~Semaphores();
 
 
    int Initialize_Semaphore(key_t key);
    void Acquire_Shared_Lock();
    void Release_Shared_Lock();
  
 
    void Acquire_Exclusive_Lock();
    void Release_Exclusive_Lock();

    void Delete_Semaphore();

  private:
    int sem_id;         
};

#endif
