C> @file
C> @ingroup group_libs_hk

C This log A0 function, written by Hiroo Kanamori probably before 2000,
c has been superceded by the CISN log A0 function rtseis/CISN_MlAo.f
c Pete Lombard, 2007/04/30
c
       function aloga0(dist)
       parameter (c=0.49710, an=1.2178, ak=0.00530, dref=8.)
       r=sqrt(dist**2+dref**2)
       aloga0=c*r**(-an)*exp(-ak*r)
       aloga0=-alog10(aloga0)
       return
       end
       function  qr(r)
       parameter (c=0.49710, an=1.2178, ak=0.00530)
       qr=c*r**(-an)*exp(-ak*r)
       return
       end
