C> @file
C> @ingroup group_libs_hk

       SUBROUTINE  DELAZ5( THEI, ALEI, THSI, ALSI, DELT, DELTDG,
     2DELTKM, AZES, AZESDG, AZSE, AZSEDG, I )
       DOUBLE  PRECISION C, AK, D, E, CP, AKP, DP, EP,
     2A, B, G, H, AP, BP, GP, HP
      IF(I) 50, 50, 51
C     IF  COORDINATES ARE GEOGRAPH DEG I=0
C     IF COORDINATES ARE GEOCENT RADIAN  I=1
   50 THE=1.745329252E-2*THEI
      ALE=1.745329252E-2*ALEI
      THS=1.745329252E-2*THSI
      ALS=1.745329252E-2*ALSI
      AAA=0.9931177*TAN(THE)
      THE=ATAN(AAA)
      AAA=0.9931177*TAN(THS)
      THS=ATAN(AAA)
      GO TO 32
   51 THE=THEI
      ALE=ALEI
      THS=THSI
      ALS = ALSI
   32 CONTINUE
      C= SIN(THE)
      AK=-COS(THE)
      D=SIN(ALE)
      E= -COS(ALE)
      A= AK*E
      B= -AK*D
      G=-C*E
      H=C*D
      CP=SIN(THS)
      AKP=-COS(THS)
      DP=SIN(ALS)
      EP = -COS(ALS)
      AP = AKP*EP
      BP=-AKP*DP
      GP=-CP*EP
      HP=CP*DP
      C1=A*AP+B*BP+C*CP
      IF( C1-0.94 )  30, 31, 31
   30 IF(C1+0.94) 28, 28, 29
   29 DELT=ACOS(C1)
   33 DELTKM=6371.0*DELT
      C3 = (AP-D)**2+(BP-E)**2+CP**2-2.0
      C4 = (AP-G)**2+(BP-H)**2+(CP-AK)**2-2.0
      C5 = (A-DP)**2+ (B-EP)**2+C**2-2.0
      C6 = (A-GP)**2+(B-HP)**2+(C-AKP)**2-2.0
      DELTDG = 57.29577951*DELT
      AZES = ATAN2(C3, C4 )
      IF ( AZES ) 80, 81, 81
   80 AZES = 6.283185308+ AZES
   81 AZSE = ATAN2( C5, C6 )
      IF ( AZSE ) 70, 71 , 71
   70 AZSE=6.283185308+AZSE
   71 AZESDG=57.29577951*AZES
      AZSEDG=57.29577951*AZSE
      RETURN
   31 C1=(A-AP)**2+(B-BP)**2+(C-CP)**2
      C1= SQRT(C1)
      C1=C1/2.0
      DELT = ASIN(C1)
      DELT= 2.0*DELT
      GO TO 33
   28 C1=(A+AP)**2+(B+BP)**2+(C+CP)**2
      C1 = SQRT(C1 )
      C1= C1/2.0
      DELT = ACOS(C1)
      DELT = 2.0*DELT
      GO TO 33
      END
