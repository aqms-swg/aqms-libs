C> @file
C> @ingroup group_libs_hk

      SUBROUTINE MAXMIN2(X,N,AMAX,AMIN,kmax,kmin)
      DIMENSION X(*)
      AMAX=X(1)
      AMIN=AMAX
      KMAX=1
      KMIN=1
      DO 1100 K=1,N
      AK=X(K)
      IF(AK-AMAX) 120,120,115
115   AMAX=AK
      KMAX=K
      GO TO 1100
120   IF(AK-AMIN) 125, 1100,1100
125      AMIN=AK
      KMIN=K
1100  CONTINUE
      RETURN
      END
