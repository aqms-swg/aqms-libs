C> @file
C> @ingroup group_libs_hk

      SUBROUTINE  ORDRJX ( X, J, N )
      DIMENSION X(*),J(*)
      J(1) = 1
      IF( N .EQ. 1 )  GO TO 320
      I = 2
  310 IF( I .GT. N )  GO  TO 320
      XTST = X(I)
      IM1 = I-1
      DO  300 JJ = 1, IM1
      K = I - JJ
      TST= X(K) - XTST
      IF( TST .GT. 0.0 )  GO TO  300
      KST = K + 1
      GO TO 330
  300 CONTINUE
      KST = K
  330 C=XTST
      IC = I
      DO 340 L=KST, I
      B = X(L)
      X(L) = C
      C = B
      IB = J(L)
      J(L) = IC
      IC = IB
  340 CONTINUE
      I = I + 1
      GO TO 310
  320 RETURN
      END
