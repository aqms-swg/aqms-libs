C> @file
C> @ingroup group_libs_hk

      subroutine realsp(x,n,xmax,kmax,av,smr)
c     subroutine for real1.f
c     looks for the absolute maximum, and computes
c     sigma/average of abs(x) for diagnostics of glitches
c     if smr>>1 (e.g. smr>3), the signal is likely to be a glitch
c     6/28/1995  h. kanamori
      dimension x(*)
      call maxmin2(x,n,xmax,xmin,kmax,kmin)
      if (abs(xmax).ge.abs(xmin)) then
      xmax=abs(xmax)
      kmax=kmax
      else
      xmax=abs(xmin)
      kmax=kmin
      end if
      s1=0.
      s2=0.
      do i=1, n
      s1=s1+abs(x(i))
      s2=s2+abs(x(i)**2)
      end do
      av=s1/float(n)
      sigma=sqrt(s2/float(n)-av**2)
      smr=sigma/av
      return
      end


