/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for Amplitude.C
 */ 
/***********************************************************

File Name :
        Amplitude.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef single_amp_H
#define single_amp_H

// Various include files
#include <iostream>
#include <list>
#include <map>
#include <stdint.h>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Logfile.h"
#include "Units.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"
#include "Phase.h"
#include "Channel.h"

using std::ostream;
using std::istream;

class Amplitude;

// Definition of an Amplitude list
typedef std::list<Amplitude> AmpList;

// Definition of a Channel-Amplitude mapping
typedef std::map<Channel, AmpList, std::less<Channel> > ChanAmpList;


// Enumeration of the valid types of amplitudes
typedef enum {AMP_NONE, AMP_C, AMP_WA, AMP_WAS, AMP_PGA, AMP_PGV, AMP_PGD,
	      AMP_WAC, AMP_WAU, AMP_IV2, AMP_SP03, AMP_SP10,
	      AMP_SP30, AMP_ML100, AMP_ME100, AMP_EGY, AMP_M0} ampType;

// Definition of the maximum amp type enumeration
const int AMP_MAX_TYPE = 17;

// String descriptions of each amplitude type
const char ampTypeStrings[AMP_MAX_TYPE][48] = {"None",
					       "Continuous",
					       "WA Photo", 
					       "WA Synth", 
					       "Peak Ground Accel", 
					       "Peak Ground Vel", 
					       "Peak Ground Disp", 
					       "WA Corrected", 
					       "WA Uncorrected", 
					       "Integral of Vel Squared",
					       "Sp. Peak 0.3", 
					       "Sp. Peak 1.0", 
					       "Sp. Peak 3.0", 
					       "ML100", "ME100", "Energy", "Moment"};

// Database string descriptions of each amplitude type
const char ampTypeDBStrings[AMP_MAX_TYPE][DB_MAX_AMPTYPE_LEN+1] = {"UN",
								   "C",
								   "WA", 
								   "WAS", 
								   "PGA", 
								   "PGV", 
								   "PGD",
								   "WAC", 
								   "WAU", 
								   "IV2", 
								   "SP.3", 
								   "SP1.0", 
								   "SP3.0", 
								   "ML100", 
								   "ME100", 
								   "EGY",
								   "M0"};


// Enumeration of the valid scales
typedef enum {AMP_SCALE_BELOW, AMP_SCALE_ON, AMP_SCALE_ABOVE} ampScale;

// Definition of the maximum amp scale enumeration
const int AMP_MAX_SCALE = 3;

// String descriptions of each amplitude scale
const char ampScaleStrings[AMP_MAX_SCALE][16] = {"Below Scale",
						 "On Scale",
						 "Above Scale"};

// Database string descriptions of each amplitude scale
const char ampScaleDBStrings[AMP_MAX_SCALE][DB_MAX_ONSCALE_LEN+1] = {"bn",
								     "os",
								     "cl"};


class Amplitude
{
 private:

 protected:

 public:
    uint32_t ampid;
    TimeStamp amptime;
    double value;
    unitType units;
    ampType amptype;
    float quality;
    ampScale scale;
    TimeStamp winstart;
    Duration duration;
    double period;
    arrivalType phase;
    double snr;
    int fullwin;
    float amp_corr;	/* recently added 2008-01-17 Paul Friberg */

    // For amplitude-magnitude associations
    double mag;
    
    Amplitude();
    Amplitude(const Amplitude &a);
    ~Amplitude();

    Amplitude& operator=(const Amplitude &a);
    friend int operator<(const Amplitude &a1, const Amplitude &a2);
    friend int operator>(const Amplitude &a1, const Amplitude &a2);

    friend ostream& operator<<(ostream &os, const Amplitude &a);
    friend Logfile& operator<<(Logfile &lf, const Amplitude &a);
    friend StatusManager& operator<<(StatusManager &sm, const Amplitude &a);
    //    friend RTStatusManager& operator<<(RTStatusManager &sm, 
    //				       const Amplitude &a);
};


#endif
