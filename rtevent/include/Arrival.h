/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for Arrival.C
 */
/***********************************************************

File Name :
        Arrival.h

Original Author:
        Patrick Small

Description:


Creation Date:
        11 June 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef arrival_H
#define arrival_H

// Various include files
#include <iostream>
#include <list>
#include <map>
#include <stdint.h>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Logfile.h"
#include "Units.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"
#include "Phase.h"
#include "Channel.h"

using std::ostream;
using std::istream;

class Arrival;

// Definition of an arrival list
typedef std::list<Arrival> ArrivalList;

// Definition of a channel-arrival mapping
typedef std::map<Channel, ArrivalList, std::less<Channel> > ChanArrivalList;


// Enumeration of the valid types of first motions
typedef enum {ARRIVAL_MOTION_UNKNOWN, ARRIVAL_MOTION_SPCOMP,
	      ARRIVAL_MOTION_LPCOMP, ARRIVAL_MOTION_SPDIL,
	      ARRIVAL_MOTION_LPDIL, ARRIVAL_MOTION_SPCOMP_LPCOMP,
	      ARRIVAL_MOTION_SPCOMP_LPDIL, ARRIVAL_MOTION_SPDIL_LPCOMP,
	      ARRIVAL_MOTION_SPDIL_LPDIL} motionType;


// Definition of the maximum amp type enumeration
const int ARRIVAL_MAX_MOTION = 9;

// String descriptions of each amplitude type
const char motionTypeStrings[ARRIVAL_MAX_MOTION][16] = {"Unknown",
							 "SP Comp",
							 "LP Comp",
							 "SP Dil",
							 "LP Dil",
							 "Comp-Comp", 
							 "Comp-Dil", 
							 "Dil-Comp",
							 "Dil-Dil"};

// Database string descriptions of each amplitude type
const char motionTypeDBStrings[ARRIVAL_MAX_MOTION][DB_MAX_FIRSTMO_LEN+1] = 
{"..", "c.", ".u", "d.", ".r", "cu", "cr", "du", "dr"};

// Enumeration of the valid types of onset quality
typedef enum {ARRIVAL_APP_UNKNOWN, ARRIVAL_APP_IMPULSIVE, 
	      ARRIVAL_APP_EMERGENT, ARRIVAL_APP_WEAK} onsetQualityType;

// Definition of the maximum onset type enumeration
const int ARRIVAL_MAX_ONSET = 4;

// String descriptions of each amplitude type
const char onsetQualityTypeStrings[ARRIVAL_MAX_ONSET][16] = {"Unknown",
							     "Impulsive",
							     "Emergent",
							     "Weak"};

// Database string descriptions of each amplitude type
const char onsetQualityTypeDBStrings[ARRIVAL_MAX_ONSET][DB_MAX_FIRSTMO_LEN+1] = {"", "i", "e", "w"};




class Arrival
{
 private:

 protected:

 public:
    uint32_t arid;
    TimeStamp arrtime;
    arrivalType arrtype;
    float quality;
    float delay;
    float importance;
    float distance;
    float emergenceAngle;
    float esAzimuth;
    motionType firstmo;
    onsetQualityType onset;
    float residual;
    float inweight;
    float outweight;
    char  source[DB_MAX_SUBSOURCE_LEN+1];

    Arrival();
    Arrival(const Arrival &a);
    ~Arrival();

    Arrival& operator=(const Arrival &a);
    friend int operator<(const Arrival &a1, const Arrival &a2);
    friend int operator>(const Arrival &a1, const Arrival &a2);

    friend ostream& operator<<(ostream &os, const Arrival &a);
    friend Logfile& operator<<(Logfile &lf, const Arrival &a);
    friend StatusManager& operator<<(StatusManager &sm, const Arrival &a);
};


#endif
