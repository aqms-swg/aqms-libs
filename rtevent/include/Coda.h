/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for Coda.C
 */
/***********************************************************

File Name :
        Coda.h

Original Author:
        Patrick Small

Description:


Creation Date:
        20 January 2000

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef coda_H
#define coda_H

// Various include files
#include <iostream>
#include <list>
#include <map>
#include <stdint.h>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Logfile.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"
#include "Channel.h"
#include "Units.h"

using std::ostream;
using std::istream;

class Coda;

// Definition of a coda list
typedef std::list<Coda> CodaList;

// Definition of a Channel-Coda mapping
typedef std::map<Channel, CodaList, std::less<Channel> > ChanCodaList;

// Number of time/amplitude pairs
const int MaxCodaAmps = 6;

class Coda
{
 private:

 protected:

 public:
    uint32_t coid;
    TimeStamp starttime;  // start of the coda, typically P arrival time
    double afix;
    double afree;
    double qfix;
    double qfree;
    unitType units;
    double duration;
    int numsamples;
    double rms;
    double quality;
    double magnitude;
    double magWeight;
    int time[MaxCodaAmps];
    int amp[MaxCodaAmps];
    char phase[2];
    
    Coda();
    Coda(const Coda &c);
    ~Coda();

    Coda& operator=(const Coda &c);
    friend int operator<(const Coda &c1, const Coda &c2);

    friend ostream& operator<<(ostream &os, const Coda &c);
    friend Logfile& operator<<(Logfile &lf, const Coda &c);
    friend StatusManager& operator<<(StatusManager &sm, const Coda &c);
};

#endif
