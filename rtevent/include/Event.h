/** @file
 * @ingroup group_libs_rtevent
 * @brief Header filr for Event.C
 */
/***********************************************************

File Name :
        Event.h

Original Author:
        Patrick Small

Description:


Creation Date:
        07 June 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef event_H
#define event_H

// Various include files
#include <list>
#include <stdint.h>
#include "seismic.h"
#include "Origin.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"


// Definition of an origin list
typedef std::list<Origin> OriginList;

// Enumeration of the valid types of events
typedef enum {EVENT_UNKNOWN, EVENT_QUAKE,
	      EVENT_QUARRY, EVENT_NUCLEAR, EVENT_SONIC, 
	      EVENT_SUBTRIG, EVENT_SLOW, EVENT_LP, EVENT_TORNILLO,
	      EVENT_NVTREMOR, EVENT_VOLTREMOR, 
	      EVENT_CALIBRATION, EVENT_EXPLOSION, EVENT_SHOT, 
	      EVENT_THUNDER, EVENT_ERUPTION, EVENT_MINE, EVENT_DEBRIS,
	      EVENT_AVALANCHE, EVENT_LANDSLIDE, EVENT_ROCKBURST,
	      EVENT_ROCKSLIDE, EVENT_BUILDING, EVENT_PLANE, EVENT_METEOR,
	      EVENT_OTHER, EVENT_SURFACE, EVENT_LOWFREQ, EVENT_PROBBLAST} eventType;

// Definition of the maximum event type enumeration
const int EVENT_MAX_TYPE = 29;

// String descriptions of each event type
const char eventTypeStrings[EVENT_MAX_TYPE][16] = {"Unknown", "Quake", 
						   "Quarry", "Nuclear", 
						   "Sonic", "SubTrig", "Slow",
						   "LongPeriod", "Tornillo", "NVtremor",
						   "VolcanicTremor",
						"Calibration", "Explosion", "SurveyShot", 
						"Thunder", "VolcanicErupt", "MineCollapse",		
						"DebrisFlow", "Avalanche", "Landslide", "RockBurst",
						"RockSlide", "Building", "PlaneCrash",
						"MeteorImpact", "Other", "SurfaceEvent", 
						"LowFrequency", "ProbableBlast"};

// Database string descriptions of each event type
const char eventTypeDBStrings[EVENT_MAX_TYPE][DB_MAX_EVENTTYPE_LEN+1] = {"uk", 
									 "eq", 
									 "qb", 
									 "nt", 
									 "sn",
									 "st",
									 "se",
									 "lp",
									 "to",
									 "tr",
									 "vt",
									 "ce",
									 "ex",
									 "sh",
									 "th",
									 "ve",
									 "co",
									 "df",
									 "av",
									 "ls",
									 "rb",
									 "rs",
									 "bc",
									 "pc",
									 "mi",
									 "ot",
									 "su",
									 "lf",
									 "px" };


class Event
{
 private:

 protected:

 public:
    uint32_t evid;
    eventType etype;
    char comment[DB_MAX_COMMENT_LEN+1];
    char source[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    OriginList origins;
    int select;
    int version;
    
    Event();
    Event(const Event &e);
    ~Event();

    Event& operator=(const Event &e);
    friend ostream& operator<<(ostream &os, const Event &e);
    friend Logfile& operator<<(Logfile &lf, const Event &e);
    friend StatusManager& operator<<(StatusManager &sm, const Event &e);
};


#endif
