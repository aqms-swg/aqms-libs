/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for EventArchiver.C
 */
/***********************************************************

File Name :
        EventArchiver.h

Original Author:
        Paul Friberg

Description:
	Persistently store an eventid (evid) as a file in an
	archive directory. It names the files using the evid
	and puts a counter inside the file indicating the
	number of times accessed. When the counter reaches 0
	the file is automatically unlinked.
	
	Note that some of the methods below only work on the
	archive files and others only work on an image of
	what is in the archive directory..


Creation Date:
        13 Nov 2003


Modification History:


Usage Notes:


**********************************************************/

#ifndef event_archiver_H
#define event_archiver_H

// Various include files
#include <map>


class EventArchiver;

// first element is event id and second is retry count of that eventid
typedef std::multimap<int, int, std::less<int>,
        std::allocator<std::pair<const int, int> > > EventIDList;


class EventArchiver
{
 private:
    char *archive_directory;
    int retry_init;
    EventIDList evids;
    EventIDList::iterator evidp;
    
    int init;
    int _buildList();
    int _clearList();
    char * _buildFilename(int);

 public:
    EventArchiver();
    EventArchiver(char * dir, int retry);
    ~EventArchiver();

    int setRetryInit(int);
    int setArchiveDirectory(char *);	// acts as an initializer if dir is not used in constructor

	// all of the following take an evid as an argument and operate on files!
    int archiveEvent(int);
    int removeEvent(int);
    int decrementCounter(int);
    int getRetryCount(int);

	// funcs to loop over evids in list 
	// (note list is only init'ed once at start of EA class or after setArchiveDirectory() call)
    int startEventLoop();
    int getNextEvent();
};

#endif
