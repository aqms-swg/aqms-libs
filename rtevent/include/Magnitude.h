/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for Magnitude.C
 */
/***********************************************************

File Name :
        Magnitude.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef magnitude_H
#define magnitude_H

// Various include files
#include <iostream>
#include <list>
#include <stdint.h>
#include "DatabaseLimits.h"
#include "seismic.h"
#include "Logfile.h"
#include "Amplitude.h"
#include "Coda.h"
#include "StatusManager.h"

using std::ostream;
using std::istream;

class Magnitude;

// Definition of a magnitude list
typedef std::list<Magnitude> MagList;


// Enumeration of the valid types of magnitudes
typedef enum {
              MAG_UNKNOWN, MAG_NONE, MAG_PRIM_AMP, MAG_BODYWAVE, MAG_ENERGY,
              MAG_LOCAL, MAG_LOCAL_REDUCED, MAG_TRAD_UCB_LOCAL, MAG_NET_UCB_LOCAL, MAG_LG,
              MAG_PRIM_CODA, MAG_SURFWAVE, MAG_MOMENT, MAG_LOW_GAIN, MAG_BENIOFF,
              MAG_DUR, MAG_HELIO
             } magType;

// Definition of the maximum mag type enumeration (number of array elements)
const int MAG_MAX_TYPE = 17;

// String descriptions of each event type
const char magTypeStrings[MAG_MAX_TYPE][16] = {
                           "NA", "None", "Ma", "Mb", "Me",
                           "Ml", "Mlr", "Ml1", "Ml2", "Mlg",
                           "Mc", "Ms", "Mw", "Mz", "MB",
                           "Md", "Mh"
};

// Database string descriptions of each event type
const char magTypeDBStrings[MAG_MAX_TYPE][DB_MAX_MAGTYPE_LEN+1] = {
                                 "un", "n", "a", "b", "e",
                                 "l", "lr", "l1", "l2", "lg",
                                 "c", "s", "w", "z", "B",
                                 "d", "h"};

class Magnitude
{
 private:

 protected:

 public:
    uint32_t magid;
    double mag;
    magType magtype;
    char algorithm[DB_MAX_ALGORITHM_LEN+1];
    char source[DB_MAX_AUTH_LEN+1]; 	// this is the netmag.auth, but calling it source to be consistent with Origin
    int numchan;
    int numsta;
    double rms;
    double mindist;
    double maxgap;
    double quality;
    double uncertainty;
    
    ChanAmpList amps;
    ChanCodaList codas;
 
    Magnitude();
    Magnitude(const Magnitude &m);
    ~Magnitude();

    Magnitude& operator=(const Magnitude &m);
    friend ostream& operator<<(ostream &os, const Magnitude &m);
    friend Logfile& operator<<(Logfile &lf, const Magnitude &m);
    friend StatusManager& operator<<(StatusManager &sm, const Magnitude &m);
    //    friend RTStatusManager& operator<<(RTStatusManager &sm, 
    //				       const Magnitude &m);
};


#endif
