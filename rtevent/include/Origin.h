/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for Origin.C 
 */
/***********************************************************

File Name :
        Origin.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef origin_H
#define origin_H

// Various include files
#include <iostream>
#include <stdint.h>
#include "DatabaseLimits.h"
#include "TimeStamp.h"
#include "Logfile.h"
#include "Magnitude.h"
#include "Arrival.h"
#include "Amplitude.h"
#include "Review.h"
#include "Coda.h"
#include "StatusManager.h"

using std::ostream;
using std::istream;

// Enumeration of the valid types of events
typedef enum {GTYPE_UNKNOWN, GTYPE_LOCAL,
	      GTYPE_REGIONAL, GTYPE_TELESEISM} geogType;

// Definition of the maximum geographic type enumeration
const int GEOG_MAX_TYPE = 4;

// String descriptions of each event type
const char geogTypeStrings[GEOG_MAX_TYPE][16] = {"Unknown", "Local", 
						 "Regional", "Teleseism"};

// Database string descriptions of each event type
const char geogTypeDBStrings[GEOG_MAX_TYPE][DB_MAX_GEOGTYPE_LEN+1] = {"", 
								      "l", 
								      "r", 
								      "t"};

class Origin
{
 private:

 protected:

 public:
    uint32_t orid;
    geogType gtype;
    char locevid[DB_MAX_LOCEVID_LEN+1];
    char algorithm[DB_MAX_ALGORITHM_LEN+1];
    char source[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char domain_code[3];
    char processing_version_code[3];
    char model_type[2];
    char crust_model[4];
    TimeStamp origdate;
    double latitude;
    double longitude;
    double depth;
    double model_depth;
    double maxgap;
    double nearsta;
    double rms;
    double horerr;
    double verterr;
    double laterr;
    double lonerr;
    double quality;
    double azimuthLargestError;
    double dipLargestError;
    double largestError;
    double azimuthInterError;
    double dipInterError;
    double interError;
    double smallestError;
    reviewType rflag;
    int fixeddepth;
    int bogus;
    int totalarr;
    int totalamp;
    int numberInputPhases;
    int numberPhasesUsed;
    int numberSPhases;
    int numberPFirstMotions;
    int numberWeightedMags;
    int hyp_version;
    int have_model_depth;
    
    MagList mags;
    ChanArrivalList arrivals;
    ChanAmpList amps;
    ChanCodaList codas;
    
    Origin();
    Origin(const Origin &o);
    ~Origin();

    int GetNumArrivals() const;
    int GetNumAmplitudes() const;
    int GetNumCodas() const;

    Origin& operator=(const Origin &o);
    friend ostream& operator<<(ostream &os, const Origin &o);
    friend Logfile& operator<<(Logfile &lf, const Origin &o);
    friend StatusManager& operator<<(StatusManager &sm, const Origin &o);
};


#endif
