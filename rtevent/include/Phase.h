/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for phase or arrival constants
 */
/***********************************************************

File Name :
        Phase.h

Original Author:
        Patrick Small

Description:


Creation Date:
        20 January 2000

Modification History:


Usage Notes:


**********************************************************/

#ifndef phase_H
#define phase_H

// Various include files
#include "DatabaseLimits.h"


// Enumeration of the valid types of phases
typedef enum {ARRIVAL_UNKNOWN, ARRIVAL_P, ARRIVAL_S} arrivalType;

// Definition of the maximum amp type enumeration
const int ARRIVAL_MAX_TYPE = 3;

// String descriptions of each amplitude type
const char arrivalTypeStrings[ARRIVAL_MAX_TYPE][16] = {"Unknown", "P Phase", 
						      "S Phase"};

// Database string descriptions of each amplitude type
const char arrivalTypeDBStrings[ARRIVAL_MAX_TYPE][DB_MAX_ARRTYPE_LEN+1] = {"?",
									   "P",
									   "S"};


#endif
