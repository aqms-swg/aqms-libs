/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for RequestCard.C
 */
/***********************************************************

File Name :
        RequestCard.h

Original Author:
        Patrick Small

Description:


Creation Date:
        11 June 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef request_card_H
#define request_card_H

// Various include files
#include <stdint.h>
#include "Channel.h"
#include "Compat.h"
#include "TimeStamp.h"
#include "TimeWindow.h"
#include "Logfile.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"


// Enumeration of the valid types of waveform archives
typedef enum {REQUEST_UNKNOWN, REQUEST_TRIG, REQUEST_CONT} requestType;

// Definition of the maximum request type enumeration
const int REQUEST_MAX_TYPE = 3;

// String descriptions of each request type
const char requestTypeStrings[REQUEST_MAX_TYPE][16] = {"Un", "Trig", "Cont"};

// Database string descriptions of each request type
const char requestTypeDBStrings[REQUEST_MAX_TYPE][DB_MAX_REQTYPE_LEN+1] = {"U",
									   "T",
									   "C"};

// Enumeration of the valid request priorities
typedef enum {REQUEST_PRIO_UNKNOWN, REQUEST_PRIO_LOW, REQUEST_PRIO_MED,
	      REQUEST_PRIO_HIGH} requestPrioType;


class RequestCard
{
 private:

 protected:

 public:
    uint32_t rcid;

    uint32_t wfid;
    int pcstored;
    int pcretrieved;

    Channel chan;
    TimeWindow win;
    int retry;
    TimeStamp lastretry;
    requestType rtype;
    requestPrioType priority;
    

    RequestCard();
    RequestCard(const RequestCard &r);
    ~RequestCard();

    RequestCard& operator=(const RequestCard &r);
    friend int operator<(const RequestCard &r1, const RequestCard &r2);
    friend ostream& operator<<(ostream &os, const RequestCard &r);
    friend Logfile& operator<<(Logfile &lf, const RequestCard &r);
    friend StatusManager& operator<<(StatusManager &sm, const RequestCard &r);
};


#endif
