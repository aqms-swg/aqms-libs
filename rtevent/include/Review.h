/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for actions related to reviewing an event
 */
/***********************************************************

File Name :
        Review.h

Original Author:
        Patrick Small

Description:


Creation Date:
        15 January 2000

Modification History:
	7 August 2009 Pete Lombard
    Added Intermediate value to review flag.

	14 December 2011 Pete Lombard
    Added Cancelled value to review flag.

Usage Notes:


**********************************************************/

#ifndef review_H
#define review_H

// Various include files
#include "DatabaseLimits.h"


#define REVIEW_MASK_AUTOMATIC     0x001
#define REVIEW_MASK_HUMAN         0x002
#define REVIEW_MASK_FINAL         0x004
#define REVIEW_MASK_INTERMEDIATE  0x008
#define REVIEW_MASK_CANCELLED     0x010

// Enumeration of the valid types of units
typedef enum {REVIEW_AUTOMATIC, REVIEW_HUMAN, REVIEW_FINAL, 
	      REVIEW_INTERMEDIATE, REVIEW_CANCELLED} reviewType;

// Definition of the maximum amp type enumeration
const int REVIEW_MAX_TYPE = 5;

// String descriptions of each amplitude type
const char reviewTypeStrings[REVIEW_MAX_TYPE][16] = {"Automatic", "Human", 
						     "Final", "Intermediate",
						     "Cancelled"};

// Database string descriptions of each amplitude type
const char reviewTypeDBStrings[REVIEW_MAX_TYPE][DB_MAX_RFLAG_LEN+1] = {"A", 
								       "H", 
								       "F",
								       "I",
								       "C"};

#endif
