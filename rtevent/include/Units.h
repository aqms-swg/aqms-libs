/** @file
 * @ingroup group_libs_rtevent
 * @brief Header file for various units used in AQMS
 */
/***********************************************************

File Name :
        Units.h

Original Author:
        Patrick Small

Description:


Creation Date:
        08 June 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef units_H
#define units_H

// Various include files
#include "DatabaseLimits.h"


// Enumeration of the valid types of units
typedef enum {UNITS_NONE, UNITS_COUNTS, UNITS_SECONDS, UNITS_MM, UNITS_CM, 
	      UNITS_M, UNITS_MS, UNITS_MSS, UNITS_CMS, UNITS_CMSS, UNITS_MMS,
	      UNITS_MMSS, UNITS_MC, UNITS_NM, UNITS_ERGS, UNITS_IOVS, 
	      UNITS_SPA, UNITS_DYNECM} unitType;

// Definition of the maximum amp type enumeration
const int UNIT_MAX_TYPE = 18;

// String descriptions of each amplitude type
const char unitTypeStrings[UNIT_MAX_TYPE][16] = {"none", "counts", "sec", 
						 "mm", "cm", "m", "m/s", 
						 "m/sec2", "cm/sec", "cm/sec2",
						 "mm/sec", "mm/sec2", "mc", 
						 "nm", "ergs", "v2", "sp", "dyne-cm"};

// Database string descriptions of each amplitude type
const char unitTypeDBStrings[UNIT_MAX_TYPE][DB_MAX_UNITTYPE_LEN+1] = {"none", 
								      "c", 
								      "s", 
								      "mm",
								      "cm", 
								      "m",
								      "ms", 
								      "mss", 
								      "cms", 
								      "cmss", 
								      "mms", 
								      "mmss", 
								      "mc", 
								      "nm", 
								      "e", 
								      "iovs", 
								      "spa", 
								      "dycm"};


#endif
