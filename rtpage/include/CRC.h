/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for CRC.C
 */
/***********************************************************

File Name :
        CRC.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 February 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef CRC_H
#define CRC_H

// Various include files
#include <iostream>


// Kermit CRC length
const int CRC_KERMIT_LENGTH = 4;


class CRC
{
 private:
    static unsigned short _CrcCcitt(unsigned char* pch);

 public:

    CRC();
    ~CRC();

    static int Kermit(const char *str, char *crcstr);
};


#endif

