/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for CheckSum.C
 */
/***********************************************************

File Name :
        CheckSum.h

Original Author:
        Patrick Small

Description:


Creation Date:
        14 January 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef checksum_H
#define checksum_H

// Various include files
#include <iostream>


class CheckSum
{
 private:

 public:

    CheckSum();
    ~CheckSum();

    static unsigned int QPager(char* s);
    static unsigned int TAP(char* s);
};

#endif

