/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for Interface.C
 */
/***********************************************************

File Name :
        Interface.h

Original Author:
        Patrick Small

Description:


Creation Date:
        18 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef interface_H
#define interface_H

// Various include files
#include "PagingLimits.h"
#include "Duration.h"


class Interface
{
 private:

 protected:
    int valid;
    char pin[PAGING_MAX_PIN_LEN];
    int latency;

    int _SafePrint(const char *buf);

 public:
    Interface();
    Interface(const Interface &i);
    ~Interface();

    // Some interfaces may require knowledge of the user's PIN to
    // be able to send data to the right destination
    int SetPIN(char *p);

    // Get the inherent transmission latency with this type of interface
    int GetLatency(int &l);

    virtual int Send(const char *buf, int len);
    virtual int Receive(char *buf, int &len, Duration dur);

    friend int operator!(const Interface &i);
    Interface& operator=(const Interface &i);
};

#endif
