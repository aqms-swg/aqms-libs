/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for Page.C
 */
/***********************************************************

File Name :
        Page.h

Original Author:
        Patrick Small

Description:


Creation Date:
        18 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef page_H
#define page_H

// Various include files
#include <map>
#include <string>
#include "PageInfo.h"


// Maximum size of a page in wire format (in bytes)
//
const int PAGE_MAX_WIRE = 1024;


class Page;

// Definition of a page list
typedef std::map<Page, PageInfo, std::less<Page>, std::allocator<std::pair<const Page, PageInfo> > > PageList;



// Definition of the maximum page message length
const int PAGE_MAX_MESSAGE_LEN = 1024;


class Page
{
 private:

 public:
    std::string msg;

    Page();
    Page(const Page &p);
    Page(const char *buf);
    ~Page();

    int Append(char *buf);
    int Serialize(char *buf, int &buflen) const;
    
    Page& operator=(const Page &p);
    friend int operator<(const Page &p1, const Page &p2);
    friend int operator==(const Page &p1, const Page &p2);
};

#endif

