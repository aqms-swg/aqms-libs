/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for PageClient.C
 */
/***********************************************************

File Name :
        PageClient.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 March 2000

Modification History:


Usage Notes:


**********************************************************/

#ifndef pageclient_H
#define pageclient_H

// Various include files
#include <string>
#include "Client.h"
#include "TCPClient.h"
#include "SNPPClient.h"


class PageClient
{

 private:
    int valid;
    TCPClient conn;
    SNPPClient snpp;

 public:
    PageClient();
    PageClient(const Client &h);
    ~PageClient();

    static int StripInvalidChars(Page &p);
    static int HasInvalidChars(const Page &p);
    int SendPages(PageList &pl);

    friend int operator!(const PageClient &pc);
};

#endif
