/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for PageInfo.C
 */
/***********************************************************

File Name :
        PageInfo.h

Original Author:
        Patrick Small

Description:


Creation Date:
        18 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef page_info_H
#define page_info_H

// Various include files
#include <list>
#include "GenLimits.h"
#include "PageStatus.h"


// Definition of a page list
typedef std::list<PageStatus, std::allocator<PageStatus> > PageStatusList;


// Maximum length of the queue filename
const int PAGEINFO_MAX_FILE_LEN = 128;


class PageInfo
{
 private:

 public:
    //    char queuefile[PAGEINFO_MAX_FILE_LEN];
    PageStatusList sl;

    PageInfo();
    PageInfo(const PageInfo &p);
    ~PageInfo();

    PageInfo& operator=(const PageInfo &p);
};

#endif
