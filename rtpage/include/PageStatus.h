/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for PageStatus.C
 */
/***********************************************************

File Name :
        PageStatus.h

Original Author:
        Patrick Small

Description:


Creation Date:
        18 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef page_status_H
#define page_status_H

// Various include files
#include <list>
#include "GenLimits.h"
#include "User.h"


// Maximum size of a user in wire format (in bytes)
//
const int PAGESTATUS_MAX_WIRE = 264;


// Definition of the possible page status conditions
typedef enum {PAGESTATUS_NONE, PAGESTATUS_PENDING, PAGESTATUS_RECEIVED, 
	      PAGESTATUS_SENT, PAGESTATUS_FAIL} pageStatus;


class PageStatus
{
 private:

 public:
    User user;
    int retry;
    pageStatus status;

    PageStatus();
    PageStatus(const PageStatus &p);
    PageStatus(const char *buf);
    ~PageStatus();

    int Serialize(char *buf, int &buflen) const;

    PageStatus& operator=(const PageStatus &p);
    friend int operator<(const PageStatus &p1, const PageStatus &p2);
    friend int operator==(const PageStatus &p1, const PageStatus &p2);
};

#endif
