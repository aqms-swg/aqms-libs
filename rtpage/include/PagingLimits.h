/** @file
 * @ingroup group_libs_rtpage
 * @brief Definitions of paging limits
 */
/***********************************************************

File Name :
        PagingLimits.h

Original Author:
        Patrick Small

Description:


Creation Date:
        20 March 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef paging_limits_H
#define paging_limits_H


// Maximum length of a page. The practical limits are
// the Page.msg field length and the amount of data
// that can be packed into a TAP block...
const int PAGING_MAX_MSG_LEN = 247;


// Maximum length of a PIN identifier
const int PAGING_MAX_PIN_LEN = 32;


// Maximum length of a mail program path
const int PAGING_MAX_PROGRAM_LEN = 32;


// Maximum number of digits in a telephone number
const int PAGING_MAX_TEL_LEN = 16;


// Maximum number of digits in a domain name
const int PAGING_MAX_DOMAIN_LEN = 64;


// Maximum length of a modem init string
const int PAGING_MAX_MODEM_INIT_LEN = 80;


// Maximum length of a modem init string
const int PAGING_MAX_RADIO_CONNECT_LEN = 80;


// Definition of the maximum service label length
const int PAGING_MAX_SERVICE_LABEL_LEN = 32;


// Maximum length of a device label
const int PAGING_MAX_DEVICE_LABEL_LEN = 32;


// Maximum length of a device path
const int PAGING_MAX_DEVICE_PATH_LEN = 256;


// Definition of the maximum user id length
const int PAGING_MAX_USERID_LEN = 32;


// Definition of the maximum user id length
const int PAGING_MAX_PASS_LEN = 32;


// Definition of the maximum user id length
const int PAGING_MAX_GROUP_LEN = 32;

#endif

