/** @file
 * @ingroup group_libs_rtpage
 */
/***********************************************************

File Name :
        Protocol.h

Original Author:
        Patrick Small

Description:


Creation Date:
        22 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef protocol_H
#define protocol_H

// Various include files
#include <list>
#include <string>
#include "Duration.h"
#include "Interface.h"


// Definition of the possible Protocol conformance levels
typedef enum {PROTO_STRICT, PROTO_TRINET} protoConform;


// Definition of the maximum packet terminator length
const int PROTOCOL_MAX_TERM_LEN = 32;


// Protocol reference structure
//
// This makes multiple Protocol objects possible, while using
// the same underlying Interface object.
//
struct protocolcb {
    int refcount;
    Interface *iface;
};


// Definition of a response terminator list
typedef std::list<std::string, std::allocator<std::string> > ProtocolResponseList;


class Protocol
{
 private:

 protected:
    int valid;
    struct protocolcb *myproto;
    protoConform conform;
    ProtocolResponseList responses;

    int _Close();
    int _SafePrint(const char *buf);
    int _IsResponse(const char *resp);
    int _AddResponse(const char *resp);
    int _GetPacket(char *buf, int &len, Duration dur);
    Duration _GetWait(Duration dur);

 public:
    Protocol();
    Protocol(const Protocol &p);
    Protocol(Interface *i);
    ~Protocol();

    int SetConformance(protoConform c);

    friend int operator!(const Protocol &p);

    Protocol& operator=(const Protocol &p);
};

#endif
