/** @file
 * @ingroup group_libs_rtpage
 */
/***********************************************************

File Name :
        ProtocolClient.h

Original Author:
        Patrick Small

Description:


Creation Date:
        22 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef protocol_client_H
#define protocol_client_H

// Various include files
#include "Page.h"
#include "Protocol.h"


class ProtocolClient : public Protocol
{
 private:
    userPINSelection pinsel;

 protected:
    int _GetPIN(const User &u, Telephone &t);

 public:
    ProtocolClient();
    ProtocolClient(const ProtocolClient &p);
    ProtocolClient(Interface *i);
    ~ProtocolClient();

    int SetPINSelection(userPINSelection p);
    int SendPages(PageList &pl);

    friend int operator!(const ProtocolClient &p);

    ProtocolClient& operator=(const ProtocolClient &p);
};

#endif
