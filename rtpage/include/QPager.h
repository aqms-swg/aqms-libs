/** @file
 * @ingroup group_libs_rtpage
 * @brief Header for QPager.C
 */
/***********************************************************

File Name :
	QPager.h

Programmer:
	Phil Maechling

Description:
	These define the routines that create and disassmble the
	qpager paging messages.

Limitations or Warnings:


Creation Date:
	26 June 1995

Modification History:
	2012/01/13 PNL: QEventBeltPage and QInit are apparently no longer
	used. Since the wheresub fortran code diesn't compile with gfortran,
	I have commented out these two functions.

**********************************************************/
#ifndef qpager_H
#define qpager_H

#define LENGTH_OF_EVENT_PAGE 250
#define LENGTH_OF_AMP_PAGE 68
#define SIZE_OF_QFIELD 12


// Structure filled in by the application containing
// the event information to report.
struct qpager_data_type
{
    char   message_type[SIZE_OF_QFIELD];    /* A2 "E " */
    int    event_id;                        /* Event id number */ 
    char   data_source[SIZE_OF_QFIELD];     /* A2 "PA" is pasadena */
    int    event_version;
    int    year;                            /* gmt */
    int    month;
    int    day;
    int    hour;
    int    minute;
    float  second;
    float  latitude;
    float  longitude;
    float  depth;
    double  magnitude;
    int    stations_in_location;
    int    phases_in_location;
    float  closest_station;
    float  time_rms;
    float  horizontal_rms;
    float  vertical_rms;
    float  az_gap;
    char   mag_type[SIZE_OF_QFIELD];
    int    stations_in_mag;
    float  magnitude_rms;
    char   quality;
    char   method_of_location[SIZE_OF_QFIELD];
    int    reviewed;
    char   checksum[SIZE_OF_QFIELD];
};



// Strcuture filled in by the application containing
// the event information to report.
struct qpager_amp_data_type
{
    char   message_type[SIZE_OF_QFIELD];    /* A2 "E " */
    int    data_id;
    char   data_source[SIZE_OF_QFIELD];     /* A2 "PA" is pasadena */
    char   motion_type;
    int    year;                            /* gmt */
    int    month;
    int    day;
    int    hour;
    int    minute;
    float  second;
    float  latitude;
    float  longitude;
    char   network[SIZE_OF_QFIELD];
    char   station[SIZE_OF_QFIELD];
    char   channel[SIZE_OF_QFIELD];
    char   location[SIZE_OF_QFIELD];
    int    motion_az;
    int    motion_zenith;
    char   clip;
    float  value;
    float  dom_freq;
    char   crc[SIZE_OF_QFIELD];
};


#ifdef __cplusplus
extern "C"
{
    //int QInit(char *tfile, char *ffile);
int QEventCompPage(struct qpager_data_type *qevent, char *comppage);
int QDeleteEventCompPage(struct qpager_data_type *qevent, char *comppage);
int QAmpCompPage(struct qpager_amp_data_type *qamp, char *amppage);
int QDeleteAmpCompPage(struct qpager_amp_data_type *qamp, char *amppage);
//int QEventBeltPage(struct qpager_data_type *qevent, char *beltpage,
//		   char *source);
int QTestBeltPage(char *beltpage);
int QTestCompPage(char *comppage);
}
#else
//int QInit(char *tfile, char *ffile);
int QEventCompPage(struct qpager_data_type *qevent, char *comppage);
int QDeleteEventCompPage(struct qpager_data_type *qevent, char *comppage);
int QAmpCompPage(struct qpager_amp_data_type *qamp, char *amppage);
int QDeleteAmpCompPage(struct qpager_amp_data_type *qamp, char *amppage);
//int QEventBeltPage(struct qpager_data_type *qevent, char *beltpage,
//		   char *source);
int QTestBeltPage(char *beltpage);
int QTestCompPage(char *comppage);
#endif
#endif
