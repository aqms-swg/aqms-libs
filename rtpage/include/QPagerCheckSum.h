/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for QPagerCheckSum.C
 */
/***********************************************************

File Name :
	QPagerCheckSum.h

Programmer:
	Phil Maechling

Description:
	This defines a checksum routine used by the qpager software.

Creation Date:
	9 Septermber 1995

Modification History:


Usage Notes:


**********************************************************/

#ifndef qpager_checksum_H
#define qpager_checksum_H

#ifdef __cplusplus

extern "C"
{
    unsigned int QPagerCheckSum(char* s);
}

#else

unsigned int QPagerCheckSum(char* s);

#endif
#endif
