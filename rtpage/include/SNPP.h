/** @file
 * @ingroup group_libs_rtpage
 * @brief Definitions for SNPP
 */
/***********************************************************

File Name :
        SNPP.h

Original Author:
        Patrick Small

Description:


Creation Date:
        18 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef snpp_H
#define snpp_H

// Definition of the maximum command length
const int SNPP_MAX_COMMAND_LEN = 256;


// Definition of the maximum response length
const int SNPP_MAX_RESPONSE_LEN = 80;


// Definition of the maximum data length
const int SNPP_MAX_DATA_LEN = 2048;


// Definition of the maximum wait time before the server hangs up
const double SNPP_MAX_WAIT_TIME = 30.0;


// Definition of the standard SNPP port number
const int SNPP_SVC_PORT = 444;


// Maximum number of chars in a response code
const int SNPP_MAX_CODE_LEN = 3;


// Definition of the standard response strings
const int SNPP_RESP_214 = 214;
const int SNPP_RESP_218 = 218;
const int SNPP_RESP_220 = 220;
const int SNPP_RESP_221 = 221;
const int SNPP_RESP_250 = 250;
const int SNPP_RESP_354 = 354;
const int SNPP_RESP_421 = 421;
const int SNPP_RESP_500 = 500;
const int SNPP_RESP_503 = 503;
const int SNPP_RESP_550 = 550;
const int SNPP_RESP_552 = 552;
const int SNPP_RESP_554 = 554;


// Level 1 commands
const int SNPP_CMDERROR = 0;	// bad command
const int SNPP_CMDPAGE = 1;	// specify a pagerid
const int SNPP_CMDMESS = 2;	// specify the message text
const int SNPP_CMDRESE = 3;	// reset all settings
const int SNPP_CMDSEND = 4;	// send the page
const int SNPP_CMDQUIT = 5;	// disconnect
const int SNPP_CMDHELP = 6;	// send help information


// Level 2 commands
const int SNPP_CMDDATA = 7;	// prompt for message text
const int SNPP_CMDLOGI = 8;	// authenticate remote user
const int SNPP_CMDLEVE = 9;	// specify service level
const int SNPP_CMDALER = 10;	// alert (not implemented)
const int SNPP_CMDCOVE = 11;	// specify alternate coverage
const int SNPP_CMDHOLD = 12;	// hold page until some future time
const int SNPP_CMDCALL = 13;	// specify callerid
const int SNPP_CMDSUBJ = 14;	// subject (not implemented)


// Level 3 commands (not implemented)
const int SNPP_CMD2WAY = 15;	// begin 2-way paging
const int SNPP_CMDPING = 16;	// ping a pager
const int SNPP_CMDEXPT = 17;	// change expiration time
const int SNPP_CMDNOQU = 18;	// don't queue message
const int SNPP_CMDACKR = 19;	// read acknowledgment
const int SNPP_CMDRTYP = 20;	// reply type
const int SNPP_CMDMCRE = 21;	// multiple choice responce codes
const int SNPP_CMDMSTA = 22;	// message status
const int SNPP_CMDKTAG = 23;	// kill message

// Maximum number of commands;
const int SNPP_CMDMAX = 24;



// Structure storing the string representation and command code for
// an SNPP command.
struct snppCommand {
	char *cmdname;
	int cmdcode;
};

static struct snppCommand snppCmdTab[] = { {NULL, SNPP_CMDERROR},
					   {(char*)"PAGEr", SNPP_CMDPAGE},
					   {(char*)"MESSage", SNPP_CMDMESS},
					   {(char*)"RESEt", SNPP_CMDRESE},
					   {(char*)"SEND", SNPP_CMDSEND},
					   {(char*)"QUIT", SNPP_CMDQUIT},
					   {(char*)"HELP", SNPP_CMDHELP},
					   {(char*)"DATA", SNPP_CMDDATA},
					   {(char*)"LOGIn", SNPP_CMDLOGI},
					   {(char*)"LEVEl", SNPP_CMDLEVE},
					   {(char*)"ALERt", SNPP_CMDALER},
					   {(char*)"COVErage", SNPP_CMDCOVE},
					   {(char*)"HOLDuntil", SNPP_CMDHOLD},
					   {(char*)"CALLerid", SNPP_CMDCALL},
					   {(char*)"SUBJect", SNPP_CMDSUBJ},
					   {(char*)"2WAY", SNPP_CMD2WAY},
					   {(char*)"PING", SNPP_CMDPING},
					   {(char*)"EXPTag", SNPP_CMDEXPT},
					   {(char*)"NOQUEUEing", SNPP_CMDNOQU},
					   {(char*)"ACKRead", SNPP_CMDACKR},
					   {(char*)"RTYPe", SNPP_CMDRTYP},
					   {(char*)"MCREsponse", SNPP_CMDMCRE},
					   {(char*)"MSTAtus", SNPP_CMDMSTA},
					   {(char*)"KTAG", SNPP_CMDKTAG} };



#endif
