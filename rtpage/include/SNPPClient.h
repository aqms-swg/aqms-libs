/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for SNPPClient.C
 */
/***********************************************************

File Name :
        SNPPClient.h

Original Author:
        Patrick Small

Description:


Creation Date:
        18 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef snpp_client_H
#define snpp_client_H

// Various include files
#include "Page.h"
#include "SNPP.h"
#include "ProtocolClient.h"



class SNPPClient : public ProtocolClient
{
 private:
    int _MakeCommand(int code, char *data, char *msg, int &len);
    int _GetCode(char *resp, int &code);
    int _GetResponse(int &code);
    int _SendRequest(int code, char *data, int &respcode);
    int _MarkPages(pageStatus p, PageStatusList::iterator slpstart, 
		   PageStatusList::iterator slp);
    int _Login();
    int _Logout();
    int _SendData(PageList::iterator plp,
		  PageStatusList::iterator slpstart, 
		  PageStatusList::iterator slp);
    
 protected:

 public:
    SNPPClient();
    SNPPClient(const SNPPClient &s);
    SNPPClient(Interface *i);
    ~SNPPClient();

    int SendPages(PageList &pl);

    SNPPClient& operator=(const SNPPClient &s);
    friend int operator!(const SNPPClient &s);
};

#endif
