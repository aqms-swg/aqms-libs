/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for TCP.C
 */
/***********************************************************

File Name :
        TCP.h

Original Author:
        Patrick Small

Description:


Creation Date:
        17 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_H
#define tcp_H

// Various include files
#include "Duration.h"
#include "Client.h"
#include "Interface.h"


// Connection reference structure
//
// This makes multiple connection objects possible, while using
// only a single TCP socket connection.
//
struct conncb {
    int refcount;
    int sd;
    Client destaddr;
};


class TCP : public Interface
{
 private:

 protected:
    struct conncb *mysession;

    int _Open(struct sockaddr *addr);
    int _Close();
    int _ConstructIP(unsigned short port, struct sockaddr *ipaddr);
    int _ConstructIP(const char *host, unsigned short port,
 		     struct sockaddr *ipaddr);

 public:
    TCP();
    TCP(const TCP &c);
    TCP(int sd, Client dest);
    ~TCP();
    int Send(const char *buf, int len);
    int Receive(char *buf, int &len, Duration dur);

    int GetDestAddress(Client &dest);
    int GetDescriptor(int &sd);
    friend int operator!(const TCP &c);
    TCP& operator=(const TCP &c);
};

#endif
