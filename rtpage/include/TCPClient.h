/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file to TCPClient.h
 */
/***********************************************************

File Name :
        TCPClient.h

Original Author:
        Patrick Small

Description:


Creation Date:
        17 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_client_H
#define tcp_client_H

// Various include files
#include "TCP.h"
#include "Client.h"


class TCPClient : public TCP
{
 private:

    int _Establish(const Client &addr);

 public:
    TCPClient();
    TCPClient(const char *svhost, unsigned int svport);
    TCPClient(const Client &addr);
    ~TCPClient();
};

#endif

