/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for Telephone.C
 */
/***********************************************************

File Name :
        Telephone.h

Original Author:
        Patrick Small

Description:


Creation Date:
        17 March 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef telephone_H
#define telephone_H

// Various include files


// Maximum length of a Telephone object in wire format
const int TEL_MAX_WIRE = 32;


// Maximum length of an area code
const int TEL_MAX_AREACODE_LEN = 3;


// Maximum length of a local telephone number
const int TEL_MAX_LOCAL_LEN = 7;


// Definition of the valid contruction types
typedef enum{TEL_CONSTRUCT_TEL, TEL_CONSTRUCT_SERIALOBJ} telConType;

class Telephone
{
 private:
    char areacode[TEL_MAX_AREACODE_LEN + 1];
    char local[TEL_MAX_LOCAL_LEN + 1];

 public:
    Telephone();
    Telephone(const char *a, const char *l);
    Telephone(const char *buf, telConType contype = TEL_CONSTRUCT_TEL);
    Telephone(const Telephone &t);
    ~Telephone();

    int Serialize(char *buf, int &buflen) const;

    int GetLocal(char *tel) const;
    int GetFull(char *tel) const;
    int GetDial(char *tel) const;

    Telephone& operator=(const Telephone &t);
    friend int operator==(const Telephone &t1, const Telephone &t2);
};

#endif

