/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for User.C
 */
/***********************************************************

File Name :
        User.h

Original Author:
        Patrick Small

Description:


Creation Date:
        24 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef user_H
#define user_H

// Various include files
#include <set>
#include "TimeWindow.h"
#include "Telephone.h"
#include "PagingLimits.h"


// Maximum size of a user in wire format (in bytes)
//
const int USER_MAX_WIRE = 256;


// Definition of the valid contruction types
typedef enum{USER_CONSTRUCT_USERID, USER_CONSTRUCT_SERIALOBJ} constructType;


// Definition of the pins that are available for use
typedef enum{USER_PIN_PRIMARY, USER_PIN_SECONDARY} userPINSelection;


class User;

// Definition of a user list
typedef std::set<User, std::less<User>, std::allocator<User> > UserList;


class User
{
 private:

 public:
    Telephone pin1;
    Telephone pin2;
    char userid[PAGING_MAX_USERID_LEN];
    char password[PAGING_MAX_PASS_LEN];
    TimeWindow active;
    char service[PAGING_MAX_SERVICE_LABEL_LEN];

    User();
    User(const char *buf, constructType contype = USER_CONSTRUCT_USERID);
    User(const User &u);
    ~User();

    int Serialize(char *buf, int &buflen) const;

    User& operator=(const User &u);
    friend int operator==(const User &u1, const User &u2);
    friend int operator<(const User &u1, const User &u2);
};

#endif

