/** @file
 * @ingroup group_libs_rtpage
 * @brief Header file for page.c 
 */
/***********************************************************

File Name :
	page.h

Programmer:
	Phil Maechling

Description:
	This has the defines for the paging routines

Limitations or Warnings:


Creation Date:
	6 July 1995

Modification History:


**********************************************************/
#ifndef PAGE_H
#define PAGE_H

#define GROUP_DIR "groups"
#define TPAGE_BIN "/home/rtem/paging/tnpage"

#ifdef __cplusplus
extern "C"
{
 int send_page(char *who,char *msg_file);
 int send_pages(char *group_file,char *msg_file);
}
#else
 int send_page(char *who,char *msg_file);
 int send_pages(char *group_file,char *msg_file);

#endif
#endif


