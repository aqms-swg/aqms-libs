/** @file
 * @ingroup group_libs_rtpage
 * @brief Routine accepts group name and a message file and sends the page in the msg file to the group specified 
 */
/***********************************************************

File Name :
	page.c	

Programmer:
	Phil Maechling

Description:
	This accepts group name and a message file and sends the
	page in the msg file to the group specified.

Limitations or Warnings:


Creation Date:
	6 July 1995

Modification History:

**********************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "page.h"

/*
  This first version sends to a specific person or name
  The destination is the character string passed in.
*/

int send_page(char *who,char *msg_file)
{
  char sysline[250];
  int status; 

  sprintf(sysline, "%s %s - < %s",TPAGE_BIN, who, msg_file);

  status = system(sysline);

  if(status)
  {
    return(-1);
  }
  else
  {
    return(0);
  }
}


int send_pages(char *group_file,char *msg_file)
{
  char sysline[250];
  char dest_line[150];
  char group_line[250];
  int status; 
  FILE *fp;

  sprintf(group_line,"%s/%s",GROUP_DIR,group_file);

  fp = fopen(group_line,"r");
  if (fp == NULL)
  {
    printf("Error opening groupd file : %s \n",group_file);
    return(-1);
  }

  status = fscanf(fp,"%s",dest_line);
 
  fclose(fp);

  if(status != 1)
  {
    return(-1); 
  }

  sprintf(sysline, "%s %s - < %s",TPAGE_BIN, dest_line, msg_file);

  status = system(sysline);

  if(status)
  {
    return(-1);
  }
  else
  {
    return(0);
  }
}
