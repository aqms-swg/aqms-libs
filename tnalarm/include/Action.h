/** @file
 * @ingroup group_libs_tnalarm
 * @brief Header file for Action.C. Includes definition of action types 
 */
/***********************************************************

File Name :
        Action.h

Original Author:
        Patrick Small

Description:


Creation Date:
        24 September 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef action_H
#define action_H

// Various include files
#include "Logfile.h"
#include "StatusManager.h"
#include "DatabaseLimits.h"


// Enumeration of the valid types of amplitudes
typedef enum {ACTION_STATE_UNKNOWN, ACTION_STATE_PENDING,
	      ACTION_STATE_PROCESSING, ACTION_STATE_COMPLETED,
	      ACTION_STATE_CANCEL_PROCESSING, ACTION_STATE_CANCELLED,
	      ACTION_STATE_OVERRULED, ACTION_STATE_ERROR} actionState;


// Definition of the maximum action type enumeration
const int ACTION_MAX_TYPE = 8;

// String descriptions of each action type
const char actionTypeStrings[ACTION_MAX_TYPE][32] = {"Unknown", "Pending", 
						     "Processing", 
						     "Completed", 
						     "Cancel Processing", 
						     "Cancelled",
						     "Overruled", "Error"};


// String descriptions of each action type
const char actionTypeDBStrings[ACTION_MAX_TYPE][16] = {"UNKNOWN", "PENDING", 
						       "PROCESSING", 
						       "COMPLETED", 
						       "CANCEL_PROC", 
						       "CANCELLED",
						       "OVERRULED", "ERROR"};


class Action
{
 private:

 protected:

 public:
    char name[DB_MAX_ALARM_ACTION_LEN];
    int modcount;
    actionState state;
    
    Action();
    Action(const char *nm);
    Action(const Action &a);
    ~Action();

    Action& operator=(const Action &a);
    friend ostream& operator<<(ostream &os, const Action &a);
    friend Logfile& operator<<(Logfile &lf, const Action &a);
    friend StatusManager& operator<<(StatusManager &sm, const Action &a);

};

#endif
