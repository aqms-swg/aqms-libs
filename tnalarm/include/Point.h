/** @file
 * @ingroup group_libs_tnalarm
 * @brief Header file for Point.C
 */
/***********************************************************

File Name :
        Point.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 September 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef point_H
#define point_H


class Point
{
 private:

 protected:

 public:
    double x;
    double y;

    Point();
    Point(double nx, double ny);
    Point(const Point &p);
    ~Point();

    friend int operator==(const Point &p1, const Point &p2);
    friend int operator!=(const Point &p1, const Point &p2);
};


#endif
