/** @file
 * @ingroup group_libs_tnalarm
 * @brief Header file for Polygon.C
 */
/***********************************************************

File Name :
        Polygon.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 September 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef polygon_H
#define polygon_H

// Various include files
#include <list>
#include "Point.h"


// Definition of a point list
typedef std::list<Point, std::allocator<Point> > PointList;


class Polygon
{
 private:

 protected:
    PointList verts;
    int valid;

    int _IsClosed();
    int _IsCrossed();

 public:
    Polygon();
    Polygon(const PointList &v);
    Polygon(const Polygon &p);
    ~Polygon();

    int IsInside(Point p);

    int operator!();
};

#endif
