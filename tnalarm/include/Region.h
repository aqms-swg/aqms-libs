/** @file
 * @ingroup group_libs_tnalarm
 * @brief Header file for Region.C
 */
/***********************************************************

File Name :
        Region.h

Original Author:
        Patrick Small

Description:


Creation Date:
        21 September 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef region_H
#define region_H

// Various include files
#include "GenLimits.h"
#include "Polygon.h"


class Region : public Polygon
{
 private:
    char name[MAXSTR];

 protected:

 public:
    Region();
    Region(const char *nm, const PointList &v);
    Region(const Region &r);
    ~Region();

    int GetName(char *nm);

    Region& operator=(const Region &r);
};

#endif
