/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        DBSumScript.C

Original Author:
        Patrick Small

Description:


Creation Date:
        17 January 2000


Modification History:


Usage Notes:


**********************************************************/

// Various include files
#include <cstring>
#include "RetCodes.h"
#include "DBSumScript.h"



DBSumScript::DBSumScript()
{
}


DBSumScript::~DBSumScript()
{
}


int DBSumScript::SetEvents(EventList &elist)
{
  el = elist;
  return(TN_SUCCESS);
}


int DBSumScript::SetWherePaths(char *tfile, char *ffile)
{
  strcpy(townfile, tfile);
  strcpy(faultfile, ffile);
  return(TN_SUCCESS);
}


int DBSumScript::ParseConfiguration(const char *tag, const char *value)
{
  std::cout << "Received configuration line " << tag << " " << value << std::endl;
  return(TN_SUCCESS);
}


int DBSumScript::Run()
{
  return(TN_SUCCESS);
}

