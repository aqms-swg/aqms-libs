/** @file
 * @ingroup group_libs_tnframework
 * @brief Abstracts the framework for an realtime (RT) application 
 */
/***********************************************************

File Name :
        Application.C

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:


Usage Notes:


**********************************************************/

// Various include files
#include "RetCodes.h"
#include "RTApplication.h"


RTApplication::RTApplication() : Program()
{
}



RTApplication::~RTApplication()
{
}



int RTApplication::GetConnection(Connection &c)
{
  c = conn;
  return(TN_SUCCESS);
}


int RTApplication::GetStatusManager(RTStatusManager &statman)
{
  statman = sm;
  return(TN_SUCCESS);
}


int RTApplication::SetConnection(Connection &c)
{
  conn = c;
  return(TN_SUCCESS);
}


int RTApplication::SetStatusManager(RTStatusManager &statman)
{
  sm = statman;
  return(TN_SUCCESS);
}

