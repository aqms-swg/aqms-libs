/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for Application.C
 */
/***********************************************************

File Name :
        Application.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef application_H
#define application_H

// Various include files
#include "Program.h"
#include "StatusManager.h"

class Application : public Program
{

 private:
    
 protected:
    StatusManager sm;

 public:
    Application();
    ~Application();

    int GetStatusManager(StatusManager &statman);
    int SetStatusManager(StatusManager &statman);
};

#endif
