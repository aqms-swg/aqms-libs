/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for BasicApplication.C
 */
/***********************************************************

File Name :
        BasicApplication.h

Original Author:
        Patrick Small

Description:


Creation Date:
        25 April 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef basic_app_H
#define basic_app_H

// Various include files
#include "Program.h"


class BasicApplication : public Program
{

 private:
    
 protected:

 public:
    BasicApplication();
    ~BasicApplication();
};

#endif
