/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for BasicRuntime.C
 */
/***********************************************************

File Name :
        BasicRuntime.h

Original Author:
        Patrick Small

Description:


Creation Date:
        25 April 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef basic_runtime_H
#define basic_runtime_H


// Various include files
#include "GenLimits.h"
#include "BasicApplication.h"


class BasicRuntime
{
 private:
    int valid;
    char progname[MAXSTR];
    char configfile[MAXSTR];

    BasicApplication *app;

    int _Usage(char *appname);
    int _ParseArgs(int argc, char **argv, char *configfile, ArgList &al);
    int _LoadConfig();
    int _DoStartup();
    int _DoShutdown();

 protected:

 public:
    BasicRuntime(BasicApplication *a, const char *cfile);
    BasicRuntime(BasicApplication *a, int argc, char **argv);
    ~BasicRuntime();

    int Run();
    int GetExitCode(int &code);

    int operator!();
};

#endif



