/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        DBScript.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 January 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef db_script_H
#define db_script_H

// Various include files
#include "RetCodes.h"
#include "GenLimits.h"
#include "Event.h"
#include "ChannelReaderDB.h"


class DBScript
{

 private:

 protected:
    char scriptname[MAXSTR];
    Event event;
    ChannelConfigList chans;
    int nmod;
    char townfile[MAXSTR];
    char faultfile[MAXSTR];

 public:
    DBScript();
    ~DBScript();

    int SetScriptName(const char *name);
    int SetEvent(const Event &ev, int nm);
    int SetChannels(const ChannelConfigList &cl);
    int SetWherePaths(const char *tfile, const char *ffile);

    virtual int ParseConfiguration(const char *tag, const char *value);
    virtual int Run();
    virtual int RunCancel();
};

#endif
