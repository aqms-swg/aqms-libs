/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        DBSumScript.h

Original Author:
        Patrick Small

Description:


Creation Date:
        17 January 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef db_sum_script_H
#define db_sum_script_H

// Various include files
#include "RetCodes.h"
#include "GenLimits.h"
#include "Event.h"


// Definition of an event list
typedef std::list<Event, std::allocator<Event> > EventList;


class DBSumScript
{

 private:
    
 protected:
    EventList el;
    char townfile[MAXSTR];
    char faultfile[MAXSTR];

 public:
    DBSumScript();
    ~DBSumScript();

    int SetEvents(EventList &elist);
    int SetWherePaths(char *tfile, char *ffile);

    virtual int ParseConfiguration(const char *tag, const char *value);
    virtual int Run();
};

#endif
