/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for DMRuntime.C
 */
/***********************************************************

File Name :
        DMRuntime.h

Original Author:
        Patrick Small

Description:


Creation Date:
        15 February 2000


Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef dm_runtime_H
#define dm_runtime_H

// Various include files
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Logfile.h"
#include "Daemon.h"
#include "DMStatusManager.h"


class DMRuntime
{
 private:
    int valid;
    char progname[MAXSTR];
    char configfile[MAXSTR];
    char logfile[MAXSTR];
    Logfile lf;
    int replevel;
    int logsem;
    int logsemkey;
    int parentpid;
    DMStatusManager sm;

    Daemon *app;

    int _Daemonize();
    int _Usage(char *appname);
    int _ParseArgs(int argc, char **argv, char *configfile, ArgList &al);
    int _GetSemaphore(int key, int &sem);
    int _RemoveSemaphore(int sem);
    int _Listen(Duration dur, DescriptorList &dl);
    int _Sleep(Duration dur);
    int _AppListen(Duration dur, DescriptorList &dl, TimeStamp &nextwake);
    int _LoadConfig();
    int _DoStartup();
    int _DoShutdown();

 protected:

 public:
    DMRuntime(Daemon *d, int argc, char **argv);
    ~DMRuntime();

    int Run();

    int ModifySM();

    int operator!();
};

#endif



