/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        DMStatusManager.h

Original Author:
        Patrick Small

Description:


Creation Date:
        15 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef dm_status_manager_H
#define dm_status_manager_H

// Various include files
#include "GenLimits.h"
#include "StatusManager.h"


class DMStatusManager : public StatusManager
{

 private:
    int logsem;
    int pid;

    int _GetSemaphore();
    int _ReleaseSemaphore();

 public:
    DMStatusManager();
    DMStatusManager(Logfile &l, int replev, int lsem, int procid);
    ~DMStatusManager();

    int DebugMessage(char *msg);
    int InfoMessage(char *msg);
    int ErrorMessage(char *msg);

    DMStatusManager& operator=(const DMStatusManager &sm);
};



#endif



