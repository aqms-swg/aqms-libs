/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        Daemon.h

Original Author:
        Patrick Small

Description:


Creation Date:
        15 February 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef daemon_H
#define daemon_H

// Various include files
#include "Program.h"
#include "DMStatusManager.h"


class Daemon : public Program
{

 private:
    
 protected:
    DMStatusManager sm;
    DescriptorList desc;

 public:
    Daemon();
    ~Daemon();

    int GetStatusManager(DMStatusManager &statman);
    int SetStatusManager(DMStatusManager &statman);

    int GetDescriptors(DescriptorList &dl);

    virtual int Ready(DescriptorList &dl);
};

#endif
