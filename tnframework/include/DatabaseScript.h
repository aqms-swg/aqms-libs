/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        DatabaseScript.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 January 2000

Modification History:


Usage Notes:


**********************************************************/

#ifndef database_script_H
#define database_script_H

// Various include files
#include <list>
#include <stdint.h>
#include "Event.h"
#include "Database.h"


// Definition of an event list
typedef std::list< Event, std::allocator<Event> > EventList;


class DatabaseScript : public Database
{
 private:
    int _GetOriginAmplitudes(uint32_t orid, ChanAmpList &al);
    int _GetMagAmplitudes(uint32_t magid, ChanAmpList &al);
    int _GetArrivals(uint32_t orid, ChanArrivalList &al);
    int _GetMagnitude(Magnitude &mag);
    int _GetMagnitudes(uint32_t orid, uint32_t magid, MagList &ml);
    int _GetOrigin(uint32_t orid, Origin &org);

 public:
    DatabaseScript();
    DatabaseScript(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseScript();

    int GetEvent(uint32_t evid, Event &e);
    int GetEventLocation(uint32_t evid, Event &e);
    int GetEvents(EventList &el, double minmag, double maxrms, int minphase,
		  int maxdays);
};

#endif
