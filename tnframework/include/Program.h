/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        Program.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef program_H
#define program_H

// Various include files
#include <vector>
#include <string>
#include <values.h>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "System.h"


// Definition of the default application wake time (ie: Application never
// wakes by default)
const TimeStamp PROGRAM_DEFAULT_WAKE = TimeStamp(3000,0,0,0,0,0,0);


// Definition of an argument list
typedef std::vector<std::string> ArgList;


class Program
{

 private:
    
 protected:
    System sys;
    ArgList args;
    char progname[MAXSTR];
    char progver[MAXSTR];
    TimeStamp nextwaketime;
    int exitcode;

 public:
    Program();
    ~Program();

    int GetSystem(System &s);
    int GetNextWakeup(TimeStamp &waketime);
    int GetProgramName(char *pname);
    int GetProgramVersion(char *pver);
    int GetExitCode(int &code);

    int SetSystem(System &s);
    int SetArgs(ArgList &al);
    int SetProgramName(const char *pname);
    int SetProgramVersion(const char *pver);
    int SetNextWakeup(TimeStamp &waketime);

    virtual int ParseConfiguration(const char *tag, const char *value);
    virtual int Startup();
    virtual int Run();
    virtual int Shutdown();
};


#endif
