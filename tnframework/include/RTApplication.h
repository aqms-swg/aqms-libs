/** @file
 * @ingroup group_libs_tnframework
 */
/***********************************************************

File Name :
        RTApplication.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef rt_application_H
#define rt_application_H

// Various include files
#include "Connection.h"
#include "RTStatusManager.h"
#include "Program.h"


class RTApplication : public Program
{

 private:
    
 protected:
    Connection conn;
    RTStatusManager sm;

 public:
    RTApplication();
    ~RTApplication();

    int GetConnection(Connection &c);
    int GetStatusManager(RTStatusManager &statman);

    int SetConnection(Connection &c);
    int SetStatusManager(RTStatusManager &statman);
};


#endif
