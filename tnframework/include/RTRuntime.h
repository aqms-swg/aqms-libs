/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for RTRuntime.C
 */
/***********************************************************

File Name :
        RTRuntime.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/


#ifndef rt_runtime_H
#define rt_runtime_H

// Various include files
#include "GenLimits.h"
#include "RTApplication.h"
#include "TimeStamp.h"
#include "Duration.h"


// Definitions of the valid events
const int RTRUNTIME_SHUTDOWN = 0;
const int RTRUNTIME_RESTART = 1;


class RTRuntime
{
 private:
    int valid;
    int pid;
    char progname[MAXSTR];
    char configfile[MAXSTR];
    char logfile[MAXSTR];
    char tssfile[MAXSTR];
    Duration hsinterval;
    TimeStamp nexths;
    Connection conn;
    Logfile lf;
    char systemsubject[MAXSTR];
    char statussubject[MAXSTR];
    char debugsubject[MAXSTR];
    char infosubject[MAXSTR];
    char errorsubject[MAXSTR];
    RTStatusManager sm;
    int replevel;
    int conflag;
    int shutdownflag;
    int restartflag;

    RTApplication *app;

    int _LoadConfig();
    int _Listen(Duration dur);
    int _ListenNextMessage(Duration dur);
    int _DoStartup();
    int _DoShutdown();
    int _DoRestart();
    int _SendHSMessage();

    
 protected:

 public:
    RTRuntime(RTApplication *a, const char *cfile);
    ~RTRuntime();

    int Run();
    int HandleEvent(int event);

    int operator!();
};


#endif



