/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for RTStatusManager.C 
 */
/***********************************************************

File Name :
        RTStatusManager.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef rt_status_manager_H
#define rt_status_manager_H

// Various include files
#include "GenLimits.h"
#include "Connection.h"
#include "StatusManager.h"


class RTStatusManager : public StatusManager
{

 private:
    Connection conn;
    char debugsubject[MAXSTR];
    char infosubject[MAXSTR];
    char errorsubject[MAXSTR];

 public:
    RTStatusManager();
    RTStatusManager(Connection &c, Logfile &l, int replev);
    ~RTStatusManager();

    int SetDebugSubject(const char *dsubject);
    int SetInfoSubject(const char *isubject);
    int SetErrorSubject(const char *esubject);

    int DebugMessage(const char *msg);
    int InfoMessage(const char *msg);
    int ErrorMessage(const char *msg);

    RTStatusManager& operator=(const RTStatusManager &sm);
};



#endif



