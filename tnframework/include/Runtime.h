/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for Runtime.C
 */
/***********************************************************

File Name :
        Runtime.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef runtime_H
#define runtime_H

// Various include files
#include "GenLimits.h"
#include "Logfile.h"
#include "Application.h"
#include "StatusManager.h"


class Runtime
{
 private:
    int valid;
    char progname[MAXSTR];
    char configfile[MAXSTR];
    char logfile[MAXSTR];
    Logfile lf;
    int replevel;
    int conflag;
    StatusManager sm;

    Application *app;

    int _Usage(char *appname);
    int _ParseArgs(int argc, char **argv, char *configfile, ArgList &al);
    int _LoadConfig();
    int _DoStartup();
    int _DoShutdown();

 protected:

 public:
    Runtime(Application *a, const char *cfile);
    Runtime(Application *a, int argc, char **argv);
    ~Runtime();

    int Run();
    int GetExitCode(int &code);

    int operator!();
};

#endif



