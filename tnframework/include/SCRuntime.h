/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for SCRuntime.C
 */
/***********************************************************

File Name :
        SCRuntime.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 January 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef sc_runtime_H
#define sc_runtime_H

// Various include files
#include "GenLimits.h"
#include "DBScript.h"
#include "DatabaseScript.h"
#include "Event.h"
#include "ChannelReaderDB.h"


// The script modes determine what amount of event information is
// extracted from the database.
typedef enum{SCRIPT_EVENT_FULL, SCRIPT_EVENT_LOCATION} infoLevel;


// Definition of the possible modes of operation
typedef enum {SCRIPT_ACTION, SCRIPT_CANCEL} modeType;


class SCRuntime
{
 private:
    int valid;
    modeType mode;
    DBScript *script;

    char scriptname[MAXSTR];
    char townfile[MAXSTR];
    char faultfile[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;

    int _ParseArgs(int argc, char **argv, char *configfile, 
		   uint32_t &ev, int &nm);
    int _GetEvent(Event &e, infoLevel lev);
    int _GetChannels(ChannelConfigList &cl);
    int _LoadConfig(char *configfile);

 protected:

 public:
    SCRuntime(DBScript *scr, int argc, char **argv, 
	      infoLevel lev = SCRIPT_EVENT_FULL);
    ~SCRuntime();

    int Run();

    int operator!();
};

#endif



