/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for SCSumRuntime.C
 */
/***********************************************************

File Name :
        SCSumRuntime.h

Original Author:
        Patrick Small

Description:


Creation Date:
        17 January 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef sc_sum_runtime_H
#define sc_sum_runtime_H

// Various include files
#include "GenLimits.h"
#include "DBSumScript.h"
#include "DatabaseScript.h"
#include "Event.h"


class SCSumRuntime
{
 private:
    int valid;
    DBSumScript *script;

    char townfile[MAXSTR];
    char faultfile[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    
    int _ParseArgs(int argc, char **argv, char *configfile, double &minmag, 
		   double &maxrms, int &minphase, int &maxdays);
    int _LoadConfig(char *configfile);

 protected:

 public:
    SCSumRuntime(DBSumScript *scr, int argc, char **argv);
    ~SCSumRuntime();

    int Run();

    int operator!();
};

#endif



