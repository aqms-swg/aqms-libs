/** @file
 * @ingroup group_libs_tnframework
 * @brief Header file for System.C
 */
/***********************************************************

File Name :
        System.h

Original Author:
        Patrick Small

Description:


Creation Date:
        06 May 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef system_H
#define system_H

// Various include files
#include <list>
#include <map>


// Definitions of system events
const int SYSTEM_BEFORE_FORK = 0;
const int SYSTEM_AFTER_FORK = 1;

// Define the less-than comparitor function for the callback map keys
typedef std::less<int> eventLess;


// Definition of the selection criteria for a file descriptor
struct descriptorEntry {
    int fd;
    int rflag;
    int wflag;
    int eflag;
};

typedef struct descriptorEntry descriptorEntry;


// Definition of a file descriptor list
typedef std::list<descriptorEntry> DescriptorList;



// Class that stores information needed to execute an event callback
class eventCB
{
 public:
    
    void (*evfunc)(int, void *);
    void *arg;

    // Default constructor
    eventCB();

    // Copy constructor
    eventCB(const eventCB &);

    //Destructor
    ~eventCB();
};


// Definition of the event map
typedef std::map<int, eventCB, eventLess> EventMap;


class System
{
 private:
    EventMap events;
    
 protected:

 public:
    System();
    ~System();
    int RegisterEventHandler(int event, void (*evfunc)(int, void*), void *arg);
    int RegisterSignalHandler(int sig, void (*sigfunc)(int));
    int Fork();
    int SendSignal(int pid, int sig);
    int Wait(int pid);

    System& operator=(const System &s);
};


#endif



