/** @file
 * @ingroup group_libs_tnframework
 * @brief Definitions used by the System class
 */
#ifndef system_control_H
#define system_control_H


const int SYSTEM_CONTROL_SHUTDOWN = 1;
const int SYSTEM_CONTROL_RESTART = 2;

#endif
