/** @file
 * @ingroup group_libs_tnpkg
 * @brief Routines for TN_TMT_SUBNET_TRIG messages. Two functions are provided to pack and unpack a Subnet Trigger message
 */
/***********************************************************

File Name :
        PackageSubnetTrigger.C

Original Author:
        Patrick Small

Description:


Creation Date:
        02 December 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.

 	20 Dec 2011 Paul Friberg - added in nettrig.subnet_code value storage

Usage Notes:


**********************************************************/

// Various include files
#include <cstring>
#include <iostream>
#include "RetCodes.h"
#include "PackageSubnetTrigger.h"


// Static data structures for storing the string pointers used
// in packed messages.
//
static char *networks[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
static char *stations[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
static char *channels[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
static char *locations[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];

static char *trigflags[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
static double trigtimes[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
static double starttimes[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
static double durations[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];


int packSubnetTrigger(Message &msg, struct subnetTrigger &st)
{
  if (msg.append((int32_t)st.locid) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append event identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.source) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append source identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.subsource) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append subsource identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.program) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append program identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.origtime.ts_as_double(TRUE_EPOCH_TIME)) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append origin time" << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.global.all_channels) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append all channels flag" 
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.global.starttime.ts_as_double(TRUE_EPOCH_TIME)) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append global start flag" 
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.global.duration) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append global duration" 
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.subnet_code) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append subnet_code" << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append number of triggered channels" << std::endl;
    return(TN_FAILURE);
  }

  // Fill string pointers into static data structures
  for (int i = 0; i < st.num_triggers; i++) {
    networks[i] = st.trigs[i].network;
    stations[i] = st.trigs[i].station;
    channels[i] = st.trigs[i].channel;
    locations[i] = st.trigs[i].location;
    trigflags[i] = st.trigs[i].trigflag;
    trigtimes[i] = st.trigs[i].trigtime.ts_as_double(TRUE_EPOCH_TIME);
    starttimes[i] = st.trigs[i].starttime.ts_as_double(TRUE_EPOCH_TIME);
    durations[i] = st.trigs[i].duration;
  }

  if (msg.append(networks, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append network array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(stations, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append station array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(channels, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error packSubnetTrigger): Unable to append channel array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(locations, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error packSubnetTrigger): Unable to append location array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(trigflags, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append trigger flag array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(trigtimes, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append trigger time array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(starttimes, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append start time array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.append(durations, st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (packSubnetTrigger): Unable to append duration array"
	 << std::endl;
    return(TN_FAILURE);
  }

  return(TN_SUCCESS);
}



int unpackSubnetTrigger(struct subnetTrigger &st, Message &msg)
{
  int size;
  char *src;
  char *subsrc;
  char *prog;
  char **nets;
  char **stas;
  char **chans;
  char **locations;
  char **trigflags;
  double time;
  double *trigtimes;
  double *starttimes;
  double *durations;

  if (msg.next(st.locid) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve event identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.next(&src) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve source identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (strlen(src) > DB_MAX_AUTH_LEN) {
    std::cout << "Error (unpackSubnetTrigger): Source identifier is too long" 
	 << std::endl;
    strncpy(st.source, src, DB_MAX_AUTH_LEN);
    st.source[DB_MAX_AUTH_LEN] = 0;
  } else {
    strcpy(st.source, src);
  }
  if (msg.next(&subsrc) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve subsource identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (strlen(subsrc) > DB_MAX_SUBSOURCE_LEN) {
    std::cout << "Error (unpackSubnetTrigger): Subsource identifier is too long" 
	 << std::endl;
    strncpy(st.subsource, subsrc, DB_MAX_SUBSOURCE_LEN);
    st.subsource[DB_MAX_SUBSOURCE_LEN] = 0;
  } else {
    strcpy(st.subsource, subsrc);
  }
  if (msg.next(&prog) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve program identifier"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (strlen(prog) > DB_MAX_ALGORITHM_LEN) {
    std::cout << "Error (unpackSubnetTrigger): Program identifier is too long" 
	 << std::endl;
    strncpy(st.program, prog, DB_MAX_ALGORITHM_LEN);
    st.program[DB_MAX_ALGORITHM_LEN] = 0;
  } else {
    strcpy(st.program, prog);
  }

  if (msg.next(time) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve origin time"
	 << std::endl;
    return(TN_FAILURE);
  }
  st.origtime = TimeStamp(TRUE_EPOCH_TIME, time);

  if (msg.next(st.global.all_channels) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve all channels flag" 
	 << std::endl;
    return(TN_FAILURE);
  }
  if (msg.next(time) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve global start time" 
	 << std::endl;
    return(TN_FAILURE);
  }
  st.global.starttime = TimeStamp(TRUE_EPOCH_TIME, time);
  
  if (msg.next(st.global.duration) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve global duration" 
	 << std::endl;
    return(TN_FAILURE);
  }

// new field to nettrig table subnet_code (may not be in every AQMS db just yet)
  if (msg.next(&subsrc) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve subnet_code value" << std::endl;
    return(TN_FAILURE);
  }
  if (strlen(subsrc) > DB_MAX_SUBNET_CODE_LEN) {
    std::cout << "Error (unpackSubnetTrigger): SubnetCode  identifier is too long" 
	 << std::endl;
    strncpy(st.subnet_code, subsrc, DB_MAX_SUBNET_CODE_LEN);
    st.subnet_code[DB_MAX_SUBNET_CODE_LEN] = 0;
  } else {
    strcpy(st.subnet_code, subsrc);
  }

  if (msg.next(st.num_triggers) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve number of confirming triggers" << std::endl;
    return(TN_FAILURE);
  }
  if (st.num_triggers > MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS) {
    std::cout << "Error (unpackSubnetTrigger): Truncating size to fit structure"
	 << std::endl;
    st.num_triggers = MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS;
  }

  if (msg.next(&nets, size) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve network array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of network array does not match number of triggers" << std::endl;
    return(TN_FAILURE);
  }
  if (msg.next(&stas, size) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve station array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of station array does not match number of triggers" << std::endl;
    return(TN_FAILURE);
  }


  if (msg.next(&chans, size) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve channel array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of channel array does not match number of triggers" << std::endl;
    return(TN_FAILURE);
  }

  if (msg.next(&locations, size) != TN_SUCCESS) {
    std::cout << "Error (unpackSubnetTrigger): Unable to retrieve location array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of location array does not match number of triggers" << std::endl;
    return(TN_FAILURE);
  }


  if (msg.next(&trigflags, size) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve trigger flag array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of trigger flag array does not match number of triggers" << std::endl;
    return(TN_FAILURE);
  }
  if (msg.next(&trigtimes, size) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve trigger time array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of trigger time array does not match number of triggers" << std::endl;
    return(TN_FAILURE);
  }

  if (msg.next(&starttimes, size) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve start time array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of start time array does not match number of picks" << std::endl;
    return(TN_FAILURE);
  }

  if (msg.next(&durations, size) != TN_SUCCESS) {
    std::cout << 
      "Error (unpackSubnetTrigger): Unable to retrieve duration array"
	 << std::endl;
    return(TN_FAILURE);
  }
  if (size != st.num_triggers) {
    std::cout << "Error (unpackSubnetTrigger): Size of duration array does not match number of picks" << std::endl;
    return(TN_FAILURE);
  }

  for (int i = 0; i < st.num_triggers; i++) {
    if (strlen(nets[i]) > MAX_CHARS_IN_NETWORK_STRING) {
      std::cout << "Error (unpackSubnetTrigger): Truncating network identifier" 
	   << std::endl;
      strncpy(st.trigs[i].network, nets[i], MAX_CHARS_IN_NETWORK_STRING - 1);
      st.trigs[i].network[MAX_CHARS_IN_NETWORK_STRING - 1] = 0;
    } else {
      strcpy(st.trigs[i].network, nets[i]);
    }
    if (strlen(stas[i]) > MAX_CHARS_IN_STATION_STRING) {
      std::cout << "Error (unpackSubnetTrigger): Truncating station identifier" 
	   << std::endl;
      strncpy(st.trigs[i].station, stas[i], MAX_CHARS_IN_STATION_STRING - 1);
      st.trigs[i].station[MAX_CHARS_IN_STATION_STRING - 1] = 0;
    } else {
      strcpy(st.trigs[i].station, stas[i]);
    }
    if (strlen(chans[i]) > MAX_CHARS_IN_CHANNEL_STRING) {
      std::cout << "Error (unpackSubnetTrigger): Truncating channel identifier" 
	   << std::endl;
      strncpy(st.trigs[i].channel, chans[i], MAX_CHARS_IN_CHANNEL_STRING - 1);
      st.trigs[i].channel[MAX_CHARS_IN_CHANNEL_STRING - 1] = 0;
    } else {
      strcpy(st.trigs[i].channel, chans[i]);
    }

    mapLC(nets[i],locations[i],MEMORY);
    if (strlen(locations[i]) > MAX_CHARS_IN_LOCATION_STRING) {
      std::cout << "Error (unpackSubnetTrigger): Truncating location identifier" 
	   << std::endl;
      strncpy(st.trigs[i].location, locations[i], MAX_CHARS_IN_LOCATION_STRING - 1);
      st.trigs[i].location[MAX_CHARS_IN_LOCATION_STRING - 1] = 0;
    } else {
      strcpy(st.trigs[i].location, locations[i]);
    }

    if (strlen(trigflags[i]) > DB_MAX_TRIGFLAG_LEN) {
      std::cout << "Error (unpackSubnetTrigger): Truncating trig flag" 
	   << std::endl;
      strncpy(st.trigs[i].trigflag, trigflags[i], DB_MAX_TRIGFLAG_LEN);
      st.trigs[i].trigflag[DB_MAX_TRIGFLAG_LEN] = 0;
    } else {
      strcpy(st.trigs[i].trigflag, trigflags[i]);
    }
    st.trigs[i].trigtime = TimeStamp(TRUE_EPOCH_TIME, trigtimes[i]);
    st.trigs[i].starttime = TimeStamp(TRUE_EPOCH_TIME, starttimes[i]);
    st.trigs[i].duration = durations[i];
  }

  return(TN_SUCCESS);
}

