/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_ALARM messages
 */ 
/***********************************************************

File Name :
        PackageAlarm.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_ALARM messages. Two functions are provided to pack and
unpack an Alarm message.


Creation Date:
        18 September 1998

Modification History:


Usage Notes:

Warnings:

        There is one caveat to be aware of when using this
module:

1) The alarm data structure supplied to the packAlarm() function
   MUST remain valid for the lifetime of the message. If the alarm
   data structure is freed before the contructed message is sent, its
   fields become invalid.

       This restriction is necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_alarm_H
#define package_alarm_H


// Various include files
#include "seismic.h"
#include "Connection.h"


// Structure containing the information for an Associator location
// message
//
struct alarm {
    int32_t evid;
    char alarm_type[MTYPE_MAX_ALARM_TYPE_NAME_LEN];
    int level;
    float magnitude;
};


int packAlarm(Message &msg, struct alarm &a);
int unpackAlarm(struct alarm &a, Message &msg);


#endif

