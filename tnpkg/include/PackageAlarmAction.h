/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_ACTION_SIG messages.
 */
/***********************************************************

File Name :
        PackageAlarmAction.h

Original Author:
        Pete Lombard

Description:

        This header file defines the interface to the package routines
for TN_TMT_ACTION_SIG messages. Two functions are provided to pack and
unpack an AlarmAction message.


Creation Date:
        31 May 2007

Modification History:


Usage Notes:

Warnings:

        There is one caveat to be aware of when using this
module:

1) The AlarmAction data structure supplied to the packAlarmAction() function
   MUST remain valid for the lifetime of the message. If the AlarmAction
   data structure is freed before the contructed message is sent, its
   fields become invalid.

       This restriction is necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_alarmaction_H
#define package_alarmaction_H


// Various include files
#include "Action.h"
#include "Connection.h"

// Structure containing the information for an Alarm Action
// message
//
struct alarmaction {
    int32_t evid;
    char name[DB_MAX_ALARM_ACTION_LEN];
    int modcount;
};


int packAlarmAction(Message &msg, struct alarmaction &a);
int unpackAlarmAction(struct alarmaction &a, Message &msg);


#endif

