/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_ALARM_CANCEL messages.
 */
/***********************************************************

File Name :
        PackageAlarmCancel.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_ALARM_CANCEL messages. Two functions are provided to pack and
unpack an Alarm message.


Creation Date:
        08 October 1999

Modification History:


Usage Notes:

Warnings:

        There is one caveat to be aware of when using this
module:

1) The alarm_cancel structure supplied to the packAlarmCancel() 
   function MUST remain valid for the lifetime of the message. If the 
   alarm_cancel data structure is freed before the contructed 
   message is sent, its fields become invalid.

       This restriction is necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.

**********************************************************/

#ifndef package_alarm_cancel_H
#define package_alarm_cancel_H


// Various include files
#include <stdint.h>
#include "seismic.h"
#include "DatabaseLimits.h"
#include "Connection.h"


// Structure containing the information for an Alarm Cancellation
// message.
//
struct alarm_cancel {
    uint32_t evid;
    char name[DB_MAX_ALARM_ACTION_LEN];
};

int packAlarmCancel(Message &msg, struct alarm_cancel &a);
int unpackAlarmCancel(struct alarm_cancel &a, Message &msg);

#endif

