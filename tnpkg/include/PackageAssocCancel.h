/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_ASSOC_CANCEL messages
 */
/***********************************************************

File Name :
        PackageAssocCancel.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_ASSOC_CANCEL messages. Two functions are provided to pack and
unpack an Association Cancellation message.


Creation Date:
        03 December 1999

Modification History:


Usage Notes:

**********************************************************/

#ifndef package_assoc_cancel_H
#define package_assoc_cancel_H


// Various include files
#include "seismic.h"
#include "Connection.h"
#include "DatabaseLimits.h"


// Structure containing the information for an Association Cancellation
// message
//
struct assocCancel {
    int32_t locid;
    char source[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char loc_algo[DB_MAX_ALGORITHM_LEN+1];
};


int packAssocCancel(Message &msg, struct assocCancel &ac);
int unpackAssocCancel(struct assocCancel &ac, Message &msg);


#endif

