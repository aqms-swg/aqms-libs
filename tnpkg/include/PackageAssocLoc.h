/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_ASSOC_LOC messages.
 */
/***********************************************************

File Name :
        PackageAssocLoc.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_ASSOC_LOC messages. Two functions are provided to pack and
unpack an Associator Location message.


Creation Date:
        18 September 1998

Modification History:
	2007/02/03, Pete Lombard: added fields and class to support many
	more fields that need to be read from hypoinverse archive message.

Usage Notes:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.

Warnings:

        There are two caveats to be aware of when using this
module:

1) Any messages that are packed using this interface must be
   immediately transmitted. Calling the packAsslocLoc() function
   for a new message before sending the old message will result in 
   the fields of the old message being invalidated.


2) The assocLoc data structure supplied to the packAssocLoc() function
   MUST remain valid for the lifetime of the message. If the assocLoc
   data structure is freed before the contructed message is sent, its
   fields become invalid.

       These restrictions are necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_assocloc_H
#define package_assocloc_H


// Various include files
#include "TimeStamp.h"
#include "seismic.h"
#include "Connection.h"
#include "DatabaseLimits.h"
#include "nscl.h"


class assocPickChannel : public nscl{
 public:
    TimeStamp arrtime;
    float actual_weight;
    float delay;
    float importance;
    float distance;
    float emergang;
    float azimuth;
    float tt_resid;
    int assigned_weight;
    char pick_type[MAX_CHARS_IN_PICKTYPE_STRING];
    char phase_remark[MAX_CHARS_IN_REMARK_STRING];
    char first_motion[MAX_CHARS_IN_FIRSTMO_STRING];
    char source[DB_MAX_SUBSOURCE_LEN];
};

#ifdef CODAS_FROM_PICKEW
class assocCodaChannel : public nscl{
 public:
    TimeStamp starttime;  /* start of coda, typically P arrival time */
    float afix;           /* Nominal coda amplitude */
    float qfix;           /* Fixed coda decay constant */
    float afree;          /* Free amplitude */
    float qfree;          /* Free decay */
    float coda_rms;       /* RMS for coda fit */
    float dur_mag;        /* Event magnitude */
    int	  mag_weight;     /* Magnitude weight */
    int   nsample;        /* Number of code sample windows */
    int   tau;            /* Coda duration */
    int   time1;          /* Time for pair 1 */
    int   amp1;           /* Amplitude for pair 1 */
    int   time2;          /* Time for pair 2 */
    int   amp2;           /* Amplitude for pair 2 */
    int   time3;          /* Time for pair 3 */
    int   amp3;           /* Amplitude for pair 3 */
    int   time4;          /* Time for pair 4 */
    int   amp4;           /* Amplitude for pair 4 */
    int   time5;          /* Time for pair 5 */
    int   amp5;           /* Amplitude for pair 5 */
    int   time6;          /* Time for pair 6 */
    int   amp6;           /* Amplitude for pair 6 */
    char  phase[2];	  /* Phase from which coda duration is measured */
};
#endif

// Structure containing the information for an Associator location
// message
//
struct assocLoc {
    int32_t locid;
    TimeStamp origtime;
    float latitude;
    float longitude;
    float depth;
    float dur_magnitude;
    float dur_mag_mad;
    float dist_to_nearest_station;
    float travel_time_residual_rms;
    float horizontal_error_km;
    float vertical_error_km;
    float azimuth_largest_error;
    float dip_largest_error;
    float largest_error;
    float azimuth_inter_error;
    float dip_inter_error;
    float inter_error;
    float smallest_error;
    int number_of_phases_input;
    int number_of_phases_used;
    int number_of_s_phases;
    int number_of_p_first_motions;
    int number_of_picks;
    int number_of_codas;
    int number_of_weighted_mags;
    int have_dur_magnitude;
    int max_azimuthal_gap;
    int hyp_version;
    int depth_constrained;
    int quality;
    char domain_code[3];
    char processing_version_code[3];
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
    char loc_algo[DB_MAX_ALGORITHM_LEN+1];
    char mag_algo[DB_MAX_ALGORITHM_LEN+1];
    assocPickChannel picks[MTYPE_MAX_ASSOC_LOC_PHASE_PICKS];
#ifdef CODAS_FROM_PICKEW
    assocCodaChannel codas[MTYPE_MAX_ASSOC_LOC_PHASE_PICKS];
#endif
};


int packAssocLoc(Message &msg, struct assocLoc &a);
int unpackAssocLoc(struct assocLoc &a, Message &msg);


#endif

