/** @file
 * @ingroup group_libs_tnpkg
 * @brief header file defines the interface to the package routines for TN_TMT_ASSOC_LOC_FILE messages.
 */
/***********************************************************

File Name :
        PackageAssocLocFile.h

Original Author:
        Pete Lombard

Description:

        This header file defines the interface to the package routines
for TN_TMT_ASSOC_LOC_FILE messages. Two functions are provided to pack and
unpack an Associator Location File message.


Creation Date:
        28 August 2007

Modification History:

Usage Notes:

Warnings:

        There are two caveats to be aware of when using this
module:

1) Any messages that are packed using this interface must be
   immediately transmitted. Calling the packAssocLocFile() function
   for a new message before sending the old message will result in 
   the fields of the old message being invalidated.


2) The assocLocFile data structure supplied to the packAssocLoc() function
   MUST remain valid for the lifetime of the message. If the assocLocFile
   data structure is freed before the contructed message is sent, its
   fields become invalid.

       These restrictions are necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_assoclocfile_H
#define package_assoclocfile_H


// Various include files
#include <climits>
#include "seismic.h"
#include "Connection.h"
#include "DatabaseLimits.h"
#include "earthworm.h"

// Structure containing the information for an Associator location
// file (raw hypoinverse archive) message
//
struct assocLocFile {
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
    char loc_algo[DB_MAX_ALGORITHM_LEN+1];
    char mag_algo[DB_MAX_ALGORITHM_LEN+1];
    char hyp_arc[PATH_MAX];
};


int packAssocLocFile(Message &msg, struct assocLocFile &a);
int unpackAssocLocFile(struct assocLocFile &a, Message &msg);


#endif

