/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_EVENT_AMP messages
 */
/***********************************************************

File Name :
        PackageEventAmp.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_EVENT_AMP messages. Two functions are provided to pack and
unpack an Event Amplitude message.


Creation Date:
        18 September 1998

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:

Warnings:

        There are two caveats to be aware of when using this
module:

1) Any messages that are packed using this interface must be
   immediately transmitted. Calling the packEventAmp() function
   for a new message before sending the old message will result in 
   the fields of the old message being invalidated.


2) The eventAmp data structure supplied to the packEventAmp() 
   function MUST remain valid for the lifetime of the message. If the 
   eventAmp data structure is freed before the contructed message is 
   sent, its fields become invalid.

       These restrictions are necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_eventamp_H
#define package_eventamp_H


// Various include files
#include "TimeStamp.h"
#include "seismic.h"
#include "Connection.h"
#include "DatabaseLimits.h"
#include "nscl.h"


// Structure containing the amplitude readings
//
class ampReading : public nscl {
 public:
    char amp_type[MAX_CHARS_IN_AMPTYPE_STRING];
    char amp_units[MAX_CHARS_IN_AMP_UNITS_STRING];
    float value;
    TimeStamp time;
    float channel_mag_value;
};


// Structure containing the information for an event amplitude
// message
//
struct eventAmp {
    int32_t evid;
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
    int number_of_amplitudes;
    ampReading amplitudes[MTYPE_MAX_EVENT_AMP_READINGS];
};


int packEventAmp(Message &msg, struct eventAmp &e);
int unpackEventAmp(struct eventAmp &e, Message &msg);


#endif

