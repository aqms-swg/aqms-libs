/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_EVENT_CANCEL messages
 */
/***********************************************************

File Name :
        PackageEventCancel.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_EVENT_CANCEL messages. Two functions are provided to pack and
unpack an Event Cancellation message.


Creation Date:
        03 December 1999

Modification History:


Usage Notes:

**********************************************************/

#ifndef package_event_cancel_H
#define package_event_cancel_H


// Various include files
#include "seismic.h"
#include "Connection.h"
#include "DatabaseLimits.h"


// Structure containing the information for an Associator location
// message
//
struct eventCancel {
    int32_t evid;
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
};


int packEventCancel(Message &msg, struct eventCancel &ec);
int unpackEventCancel(struct eventCancel &ec, Message &msg);


#endif

