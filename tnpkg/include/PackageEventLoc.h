/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_Event_LOC messages
 */
/***********************************************************

File Name :
        PackageEventLoc.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_Event_LOC messages. Two functions are provided to pack and
unpack an Event Location message.


Creation Date:
        18 September 1998

Modification History:


Usage Notes:

Warnings:

        There are two caveats to be aware of when using this
module:

1) Any messages that are packed using this interface must be
   immediately transmitted. Calling the packEventLoc() function
   for a new message before sending the old message will result in 
   the fields of the old message being invalidated.


2) The eventLoc data structure supplied to the packEventLoc() function
   MUST remain valid for the lifetime of the message. If the eventLoc
   data structure is freed before the contructed message is sent, its
   fields become invalid.

       These restrictions are necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_eventloc_H
#define package_eventloc_H


// Various include files
#include "seismic.h"
#include "Connection.h"
#include "PackageAssocLoc.h"
#include "DatabaseLimits.h"


// Structure containing the information for an Associator location
// message
//
struct eventLoc {
    // Event related information
    int32_t evid;
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
    int nmod;
    int locfinal;

    // Location information
    struct assocLoc location;
};


int packEventLoc(Message &msg, struct eventLoc &a);
int unpackEventLoc(struct eventLoc &a, Message &msg);


#endif

