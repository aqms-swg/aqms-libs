/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for Event Magnitude/Location/Archive message
 */
/***********************************************************

File Name :
        PackageEventLocMag.h

Original Author:
        Patrick Small

Description:


Creation Date:
        03 September 1998

Modification History:


Usage Notes:


**********************************************************/


#ifndef package_eventlocmag_H
#define package_eventlocmag_H


// Various include files
#include "seismic.h"
#include "Connection.h"
#include "PackageEventLoc.h"


// Structure containing the information for an Event Magnitude/Location/Archive
// message
struct eventLocMag {
    int magavail;
    char magtype[MAX_CHARS_IN_MAGTYPE_STRING];
    float magnitude;
    int magfinal;
    int num_mag_channels;
    float mag_rms;
    float dist_to_closest_mag_station;
    struct eventLoc event;
};


int packEventLocMag(Message &msg, struct eventLocMag &e);
int unpackEventLocMag(struct eventLocMag &e, Message &msg);


#endif

