/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_EVENT_TRIG messages
 */
/***********************************************************

File Name :
        PackageEventTrigger.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package 
routines for TN_TMT_EVENT_TRIG messages. Two functions are 
provided to pack and unpack an Event Trigger message.


Creation Date:
        17 November 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:


**********************************************************/

#ifndef package_event_trigger_H
#define package_event_trigger_H

// Various include files
#include "TimeStamp.h"
#include "seismic.h"
#include "DatabaseLimits.h"
#include "Connection.h"
#include "nscl.h"


// Structure containing the information for an Event Trigger
// message
//
class eventTrigger : public nscl{
 public:
    int32_t locid;
    char source[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char program[DB_MAX_ALGORITHM_LEN+1];
    TimeStamp origtime;
    float latitude;
    float longitude;
    float peak_amp;
    int num_confirming;
    float amp_thresh;
};


int packEventTrigger(Message &msg,eventTrigger &et);
int unpackEventTrigger(eventTrigger &et, Message &msg);


#endif

