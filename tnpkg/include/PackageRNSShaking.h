/** @file
 * @ingroup group_libs_tnpkg
 * @brief header file defines the interface to the package routines for TN_TMT_RNS_SHAKING messages 
 */
/***********************************************************

File Name :
        PackageRNSShaking.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_RNS_SHAKING messages. Two functions are provided to pack and
unpack a Rapid Notification Service Shaking message.


Creation Date:
        9 November 1998

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:

Warnings:

        There are two caveats to be aware of when using this
module:

1) Any messages that are packed using this interface must be
   immediately transmitted. Calling the packRequestCard() function
   for a new message before sending the old message will result in 
   the fields of the old message being invalidated.


2) The requestCard data structure supplied to the packRequestCard() 
   function MUST remain valid for the lifetime of the message. If the 
   requestCard data structure is freed before the contructed message is 
   sent, its fields become invalid.

       These restrictions are necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_rnsshaking_H
#define package_rnsshaking_H


// Various include files
#include "TimeStamp.h"
#include "seismic.h"
#include "Connection.h"
#include "nscl.h"


// Structure containing the time windows for selected channels.
//
class stationTrigger : public nscl{
 public:
    float latitude;
    float longitude;
    TimeStamp samptime;
    float reading;
};


// Structure containing the information for a Request Card
// message
//
struct shakeReport {
    int msgnum;
    TimeStamp msgtime;
    int32_t eventid;
    int conflevel;
    stationTrigger firsttrig;
    int accelavail;
    stationTrigger peakaccel;
    int velavail;
    stationTrigger peakvel;
};


int packRNSShaking(Message &msg, struct shakeReport &a);
int unpackRNSShaking(struct shakeReport &a, Message &msg);


#endif

