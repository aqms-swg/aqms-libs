/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_REQUEST_CARD messages
 */
/***********************************************************

File Name :
        PackageRequestCard.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package routines
for TN_TMT_REQUEST_CARD messages. Two functions are provided to pack and
unpack a Request Card message.


Creation Date:
        18 September 1998

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:

Warnings:

        There are two caveats to be aware of when using this
module:

1) Any messages that are packed using this interface must be
   immediately transmitted. Calling the packRequestCard() function
   for a new message before sending the old message will result in 
   the fields of the old message being invalidated.


2) The requestCard data structure supplied to the packRequestCard() 
   function MUST remain valid for the lifetime of the message. If the 
   requestCard data structure is freed before the contructed message is 
   sent, its fields become invalid.

       These restrictions are necessary since the Communications API
is optimized to copy arrays by pointer, not by value. Thus, pointers
to user data structures are incorporated directly into the new
messages. The original data structures must remain valid long enough
to transmit the messages. Once a message is sent, it is safe to
free the associated data structures.


**********************************************************/

#ifndef package_requestcard_H
#define package_requestcard_H


// Various include files
#include "TimeStamp.h"
#include "seismic.h"
#include "Connection.h"
#include "DatabaseLimits.h"
#include "nscl.h"

// Structure containing the time windows for selected channels.
//
class selectedChannel : public nscl {
 public:
    TimeStamp start_time;
    TimeStamp end_time;
};


// Structure containing the information for a Request Card
// message
//
struct requestCard {
    int32_t evid;
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
    int num_selected_channels;
    selectedChannel selected[MTYPE_MAX_REQUEST_CARD_CHANNELS];
};


int packRequestCard(Message &msg, struct requestCard &a);
int unpackRequestCard(struct requestCard &a, Message &msg);


#endif

