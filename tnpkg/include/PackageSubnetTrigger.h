/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines the interface to the package routines for TN_TMT_SUBNET_TRIG messages
 */
/***********************************************************

File Name :
        PackageSubnetTrigger.h

Original Author:
        Patrick Small

Description:

        This header file defines the interface to the package 
routines for TN_TMT_SUBNET_TRIG messages. Two functions are 
provided to pack and unpack a Subnet Trigger message.


Creation Date:
        19 November 1999

Modification History:
	10 Dec 2007 Pete Lombard - Modified to use leap-second-aware 
	tntime classes.


Usage Notes:



**********************************************************/

#ifndef package_subnet_trigger_H
#define package_subnet_trigger_H

// Various include files
#include "TimeStamp.h"
#include "seismic.h"
#include "DatabaseLimits.h"
#include "Connection.h"
#include "nscl.h"


struct trigGlobal {
    int all_channels;
    TimeStamp starttime;
    double duration;
};


class trigChannel : public nscl{
 public:
    char trigflag[DB_MAX_TRIGFLAG_LEN+1];
    TimeStamp trigtime;
    TimeStamp starttime;
    double duration;
};


// Structure containing the information for an Subnet Trigger
// message
//
struct subnetTrigger {
    int32_t locid;
    char source[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char program[DB_MAX_ALGORITHM_LEN+1];
    char subnet_code[DB_MAX_SUBNET_CODE_LEN+1];
    TimeStamp origtime;
    struct trigGlobal global;
    int num_triggers;
    trigChannel trigs[MTYPE_MAX_SUBNET_CHANNEL_TRIGGERS];
};


int packSubnetTrigger(Message &msg, struct subnetTrigger &st);
int unpackSubnetTrigger(struct subnetTrigger &st, Message &msg);


#endif

