/** @file
 * @ingroup group_libs_tnpkg
 * @brief Header file defines a static class that provides static methods to fill Message objects with the contents of conventional data structures
 */
/***********************************************************

File Name :
        PackageTraceBuf.h

Original Author:
        Patrick Small

Description:

        This header file defines a static class that provides static
methods to fill Message objects with the contents of conventional
data structures. These methods are intended to be used as convenience
functions and are not required to pack data in a Message object.
        Currently, only TracePacket messages are supported.


Creation Date:
        06 July 1998

Modification History:


Usage Notes:


**********************************************************/


#ifndef package_tracebuf_H
#define package_tracebuf_H


// Various include files
#include <trace_buf.h>
#include "Message.h"

int packTraceBuf(Message&, TracePacket&);
int unpackTraceBuf(TracePacket&, Message&);

#endif

