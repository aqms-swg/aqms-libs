/** @file
 * @ingroup group_libs_tnstd
 * @brief General limit declarations defined here for use by the AQMS software
 */
/***********************************************************

File Name :
	GenLimits.h

Programmer:
	Patrick Small

Description:
	These are general limit declarations defined here for use 
	by the TriNet software.

Creation Date:
	02 July 1998

Modification History:


Usage Notes:


**********************************************************/
#ifndef GenLimits_H
#define GenLimits_H

// Default maximum length of a character string
//
const int MAXSTR = 256;


// Number of days in each calendar month
//
const int daysInMonth[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

#endif
