/** @file
 * @ingroup group_libs_tnstd
 * @brief Header file for system_timeout.c
 */

int system_timeout( char *, int);
int system_timeout2( char *, int, char *, char *);
int system_timeout3( char *, int);
