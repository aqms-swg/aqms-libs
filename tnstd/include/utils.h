/** @file
 * @ingroup group_libs_tnstd
 * @brief Header file for utils.C
 */
#ifndef __utils_h
#define __utils_h

namespace utils{
  int round(double a);
};

#endif
