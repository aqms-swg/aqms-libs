/** @file
 * @ingroup group_libs_tntcp
 * @brief Header file for TCPConn.C 
 */
/***********************************************************

File Name :
        TCPConn.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 February 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_conn_H
#define tcp_conn_H

// Various include files
#include <list>
#include "Client.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "TCPMessage.h"

/// Version of the TCP Connection protocol
const int TCP_CONN_VERSION = 1;


/// Connection reference structure
//
/// This makes multiple connection objects possible, while using
/// only a single TCP socket connection.
//
struct tcpconncb {
    int refcount;
    int sd;
    Client destaddr;
};


/// Maximum size of the data portion of a TCP packet
//
// The packet size must exceed DATAFIELD_MAX_SIZE
// so that each packet may contain at least one field.
//
const int TCP_CONN_MAX_DATA_SIZE = 16366;



/// Structure defining the packet header of a TCP message
struct packetHeader {
  int ver;
  int mtype;
  int packnum;
  int packtot;
  int datalen;
};

typedef struct packetHeader packetHeader;



/// Structure defining a packet
struct packet {
  packetHeader hdr;
  char data[TCP_CONN_MAX_DATA_SIZE];
};

typedef struct packet packet;


/// Type definition for a Packet list
typedef std::list<packet, std::allocator<packet> > PacketList;



class TCPConn
{
 private:

 protected:
    int valid;
    struct tcpconncb *mysession;

    int _Open(struct sockaddr *addr);
    int _Close();
    int _SetNonBlocking();
    int _ConstructIP(unsigned short port, struct sockaddr *ipaddr);
    int _ConstructIP(const char *host, unsigned short port, 
		     struct sockaddr *ipaddr);
    int _ReceiveBuffer(char *buf, int buflen, Duration dur);
    int _ReceivePacket(packet &p, Duration dur);
    int _ExtractFields(PacketList pl, DataFieldList &dl); 
    int _CanReceiveData(int dur);
    int _CanSendData(int dur);

 public:
    TCPConn();
    TCPConn(const TCPConn &c);
    TCPConn(int sd, Client dest);
    ~TCPConn();
    int Send(TCPMessage msg);
    int Receive(TCPMessage &msg, Duration dur);
    int GetDestAddress(Client &dest);
    int CanReceiveData(Duration dur);
    int CanSendData(Duration dur);
    int CanReceiveData();
    int CanSendData();
    friend int operator!(const TCPConn &c);
    TCPConn& operator=(const TCPConn &c);
};

#endif

