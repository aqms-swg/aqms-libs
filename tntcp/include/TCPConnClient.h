/** @file
 * @ingroup group_libs_tntcp
 * @brief Header file for TCPConnClient.C 
 */
/***********************************************************

File Name :
        TCPConnClient.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 February 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_conn_client_H
#define tcp_conn_client_H

// Various include files
#include "TCPConn.h"
#include "Client.h"


class TCPConnClient : public TCPConn
{
 private:

    int _Establish(const Client &addr);
    int _Establish(const Client &addr, Duration dur);

 public:
    TCPConnClient();
    TCPConnClient(const char *svhost, unsigned int svport);
    TCPConnClient(const Client &addr);
    TCPConnClient(const Client &addr, Duration dur);
    ~TCPConnClient();
};

#endif

