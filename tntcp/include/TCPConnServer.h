/** @file
 * @ingroup group_libs_tntcp
 * @brief Header file for TCPConnServer.C 
 */
/***********************************************************

File Name :
        TCPConnServer.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 February 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_conn_server_H
#define tcp_conn_server_H

// Various include files
#include "TCPConn.h"


class TCPConnServer : public TCPConn
{
 private:

 public:
    TCPConnServer();
    TCPConnServer(unsigned int port, unsigned int maxqueue);
    ~TCPConnServer();

    TCPConn* Accept();
    TCPConn* AcceptNoRDNS();
};

#endif

