/** @file
 * @ingroup group_libs_tntcp
 * @brief Header file for TCPMessage.C
 */
/***********************************************************

File Name :
        TCPMessage.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 February 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef tcp_message_H
#define tcp_message_H

// Various include files
#include <vector>
#include "DataField.h"
#include "MessageTypesTCP.h"


// Type definition for a DataField list
typedef std::vector<DataField, std::allocator<DataField> > DataFieldList;


class TCPConn;

class TCPMessage
{
 private:
    int valid;
    int mtype;
    DataFieldList dlist;
    int readpos;

 public:
    TCPMessage();
    TCPMessage(int msgtype);
    ~TCPMessage();

    int Append(int i);
    int Append(double d);
    int Append(char *str);
    int Append(char *buf, int buflen);
    int Next(int &i);
    int Next(double &d);
    int Next(char *str);
    int Next(char *buf, int &buflen);
    int GetType();
    int GetNumFields();
    int SetFieldNum(int num);
    int ClearFields();

    TCPMessage& operator=(const TCPMessage &msg);

    friend class TCPConn;
};

#endif

