/** @file                                                                                
 * @ingroup group_libs_tntime 
 * @brief This header file defines the fundamental time representation, with the expectation that timestamps and durations are derived from this.
 */
/***************************************************************

  Filename:
	TimeRep.H


  Purpose:
	This defines the fundamental time representation, with the
        expectation that timestamps and durations are derived from
        this.

  Programmer:
    Phil M.
	
  Creation Date:
    7/17/1995	

  Modification History:

  Usage Notes:

    All constructors assume that the input time is in utc time.

    Don't call construct dayofyear with leading 0 (eg 017) as
      it will be interpreted as octal.

***************************************************************/

#ifndef TIMEREP_H
#define TIMEREP_H

#include <time.h>
#include <iostream>

using std::istream;
using std::ostream;


const int PST_OFFSET = 8;

class TimeRep 
{

  friend ostream& operator<<(ostream &ot, const TimeRep &in);
  friend istream& operator>>(istream &ot, const TimeRep &in);

  int operator!=(const TimeRep &in) const;
public:

// Time representation constructors

  TimeRep();
  TimeRep(const double &tval );
  TimeRep(const timeval &tval );
  TimeRep(const TimeRep &intime);
  TimeRep(const int &sec, const int &usec);
  TimeRep(int year, int month, int day, int hour, int min, int sec, int tmsec);
  TimeRep(int year, int dayofyear,      int hour, int min, int sec, int tmsec);

// Relational operators

  int operator==(const TimeRep &in) const;
  int operator< (const TimeRep &in) const;
  int operator<=(const TimeRep &in) const;
  int operator> (const TimeRep &in) const;
  int operator>=(const TimeRep &in) const;

//
//  Methods of access to the time representation.
//  These return the private members
//

  int32_t seconds() const;
  int32_t milliseconds() const;
  int32_t tenth_milliseconds() const;
  int32_t micro_seconds() const;

  const timeval get_p_time() const {
    return p_time;
  }

private:

  timeval  p_time;

};

#endif
