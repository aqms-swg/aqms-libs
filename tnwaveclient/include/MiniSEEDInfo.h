/** @file
 * @ingroup group_libs_tnwaveclient
 * @brief Header file for MiniSEEDInfo.C
 */
/***********************************************************

File Name :
        MiniSEEDInfo.h

Original Author:
        Patrick Small

Description:


Creation Date:
        08 July 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef mseed_info_H
#define mseed_info_H

// Various include files
#include "WaveClientStructs.h"
#include "MiniSEED.h"


class MiniSEEDInfo
{

 private:
    int valid;
    int hdr_worder;
    int data_worder;
    int recsize;
    int format;
    char rec_type;


 public:

    MiniSEEDInfo(const DataSegmentList &dl);
    ~MiniSEEDInfo();

    int GetHeaderWordOrder(int &word_order);
    int GetDataWordOrder(int &word_order);
    int GetFormat(int &data_format);
    int GetRecordSize(int &record_size);
    int GetRecordType(char &rectype);

    friend int operator!(const MiniSEEDInfo &mi);
};


#endif
