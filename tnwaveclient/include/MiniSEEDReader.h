/** @file
 * @ingroup group_libs_tnwaveclient
 * @brief Header file for MiniSEEDReader.C
 */
/***********************************************************

File Name :
        MiniSEEDReader.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 March 1999

Modification History:


Usage Notes:

**********************************************************/

#ifndef mseed_reader_H
#define mseed_reader_H


// Various include files
#include "qlib2.h"
#include "WaveClientStructs.h"
#include "Waveform.h"
#include "MiniSEED.h"
#include "nscl.h"

class MiniSEEDReader : public nscl
{

 private:
    int valid;
    FILE *fd;
    int idknown;

 public:

    // OBSOLETE METHODS
    MiniSEEDReader(const char *path, const char *userstr, 
		   const char *net, const char *sta, const char *chan,const char *loc);
    int read(DataList &dl);


    // STANDARD METHODS
    MiniSEEDReader(const char *minifile);
    ~MiniSEEDReader();
    int Read(Waveform &w);
    friend int operator!(const MiniSEEDReader &mf);
};


#endif
