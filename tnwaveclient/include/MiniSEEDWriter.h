/** @file
 * @ingroup group_libs_tnwaveclient
 * @brief Header file for MiniSEEDWriter.C
 */
/***********************************************************

File Name :
        MiniSEEDWriter.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 March 1999

Modification History:
	2009/06/20 PNL - Replaced stdio file access with C++ IO.


Usage Notes:


**********************************************************/

#ifndef mseed_writer_H
#define mseed_writer_H


// Various include files
#include <iostream>
#include <fstream>
#include "qlib2.h"
#include "WaveClientStructs.h"
#include "Waveform.h"
#include "Channel.h"
#include "MiniSEED.h"
#include "nscl.h"


class MiniSEEDWriter : public nscl
{

 private:
    int valid;
    ofstream ofile;
    int data_format;
    int recordsize;
    int mode;

 public:

    // STANDARD METHODS
    MiniSEEDWriter(const char *minifile, int recsize = MINISEED_REC_ORIGINAL,
		   int format = MINISEED_FORMAT_ORIGINAL);
    ~MiniSEEDWriter();
    int Write(Waveform &w);
    int Flush();

    friend int operator!(const MiniSEEDWriter &mf);
};


#endif
