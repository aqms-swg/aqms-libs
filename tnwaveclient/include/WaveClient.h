/** @file
 * @ingroup group_libs_tnwaveclient
 * @brief Header file for WaveClient.C
 */
/***********************************************************

File Name :
        WaveClient.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 March 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef waveclient_H
#define waveclient_H


// Various include files
#include "Waveform.h"
#include "WaveClientStructs.h"
#include "TCPMessage.h"
#include "Counter.h"


// Modes for retrieving data from the server
//
const int WAVE_CLIENT_IGNORE_GAPS = 0;  // ignore time gaps - get all data
const int WAVE_CLIENT_FIND_GAPS = 1;    // watch for time gaps


class WaveClient
{

 private:

    int valid;
    ServerList servers;
    int mode;
    Counter seqnum;

    int _Reconnect(ServerList::iterator &sp);
    int _RequestReply(ServerList::iterator &sp, TCPMessage &reqmsg, 
		      TCPMessage &respmsg);
    int _DoSendRequest(ServerList::iterator &sp,Channel &chan,TimeWindow &reqwin);
    int _DoGetResponse(ServerList::iterator &sp,TCPMessage &respmsg);

    int _DoGetData(ServerList::iterator &sp, Channel &chan,
		   TimeWindow &reqwin, DataSegmentList &dl);
    int _DoGetSampleRate(ServerList::iterator &sp, Channel &chan);
    int _DoGetChannels(ServerList::iterator &sp, ChannelList &cl);
    int _DoGetTimes(ServerList::iterator &sp, Channel &chan,
		    TimeWindowList &tl);
    int _VerifyTimeWindows(TimeWindowList &tl);
    int _MergeTimeWindows(TimeWindowList &tl, double samprate);
    int _HaveRequestedData(TimeWindow &tw, DataSegmentList &dl, 
			   double samprate);
    

 public:

    // STANDARD METHODS
    WaveClient();
    ~WaveClient();
    int SetRetrieveMode(int m);
    int AddServer(const Client &addr);
    int AddServer(const Client &addr, Duration dur);
    int RemoveServer(const Client &addr);
    int GetData(const Channel &c, const TimeWindow &win, Waveform &w);
    int GetDataInBulk(ArgumentList &al);
    int GetChannels(ChannelList &cl);
    int GetTimes(const Channel &c, TimeWindowList &tl);
    int GetSampleRate(Channel &c);
    friend int operator!(const WaveClient &wc);

};


#endif
