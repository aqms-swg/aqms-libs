/** @file
 * @ingroup group_libs_tnwaveclient
 * @brief Header file for WaveClientStructs.C
 */
/***********************************************************

File Name :
        WaveClientStructs.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 March 1999

Modification History:


Usage Notes:

**********************************************************/

#ifndef waveclientstructs_H
#define waveclientstructs_H


// Various include files
#include <cstdlib>
#include <list>
#include <map>
#include "TimeStamp.h"
#include "Duration.h"
#include "Client.h"
#include "Channel.h"
#include "TimeWindow.h"
#include "DataSegment.h"
#include "TCPConnClient.h"
#include "Waveform.h"

using namespace std;

struct Argument{
    Channel chn;
    TimeWindow win;
    Waveform wave;
    int state;    //-1 = UNSATISFIED,-2 = NODATA, 0 = UNPROCESSED, 1 = SATISFIED.
    int errcode;
};

//Definition of the ArgumentList
typedef list<Argument> ArgumentList;

// Definition of the ServerList
//
typedef std::map<Client, TCPConn, std::less<Client>, std::allocator<std::pair<const Client, TCPConn> > > ServerList;



// Definition of the ChannelList
//
typedef std::list<Channel, std::allocator<Channel> > ChannelList;



// Time entry class used by the WaveClient class to maintain
// a list of currently available waveform segment times
//
// OBSOLETE DEFINITION
//
class time_entry {

 public:

    TimeStamp starttime;
    TimeStamp endtime;
    
    // Constructor
    //
    time_entry();
    
    // Destructor
    //
    ~time_entry();


    // Overloaded less-than operator
    //
    friend int operator<(const time_entry &t1, 
			 const time_entry &t2);


    // Overloaded less-than-or-equal-to operator
    //
    friend int operator<=(const time_entry &t1, 
			 const time_entry &t2);


    // Overloaded equality operator
    //
    friend int operator==(const time_entry &t1, 
			  const time_entry &t2);


    friend ostream& operator<<(ostream &o, const time_entry &t);
};


// Definition of the TimeList
//
//
// OBSOLETE DEFINITION
//
typedef std::list<time_entry, std::allocator<time_entry> > TimeList;


// Definition of the TimeList
//
typedef std::list<TimeWindow, std::allocator<TimeWindow> > TimeWindowList;



// Data entry class used by the WaveClient class to maintain
// a list of retrieved waveform data segments
//
class data_entry {

 public:

    TimeStamp starttime;
    TimeStamp endtime;
    unsigned int numsamp;
    int *data;

    // Constructor
    //
    data_entry();

    // Copy Constructor
    //
    data_entry(const data_entry&);

    // Constructor
    //
    data_entry(int numdata);

    // Destructor
    //
    ~data_entry();


    // Overloaded less-than operator
    //
    friend int operator<(const data_entry &d1, 
			 const data_entry &d2);


    // Overloaded equality operator
    //
    friend int operator==(const data_entry &d1, 
			  const data_entry &d2);

};


// Definition of the DataList
//
typedef std::list<data_entry, std::allocator<data_entry> > DataList;



// Definition of a DataSegmentList
//
typedef std::list<DataSegment, std::allocator<DataSegment> > DataSegmentList;


#endif
