/** @file
 * @ingroup group_libs_tnwavepool
 * @brief Header file for RapidWavePool.C
 */
/***********************************************************

File Name :
        RapidWavePool.h

Original Author:
        Patrick Small

Description:


Creation Date:
        March 2 1999


Modification History:


Usage Notes:


**********************************************************/

#ifndef rapid_wave_pool_H
#define rapid_wave_pool_H

// Various include files
#include <list>
#include <unistd.h>
#include "TimeStamp.h"
#include "Duration.h"
#include "TimeWindow.h"
#include "Channel.h"
#include "DataSegment.h"
#include "Counter.h"
#include "nscl.h"


// Defines the maximum packet size
const int RWP_MAX_PACKET_SIZE = 512;


// Type definition for a Data Segment list
typedef std::list<DataSegment, std::allocator<DataSegment> > DataSegmentList;


// Type definition for a Time Window list
typedef std::list<TimeWindow, std::allocator<TimeWindow> > TimeWindowList;



// General information regarding this rapid wave pool
/* class generalHeader : public nscl{ */
/*  public: */
/*     double samprate; */
/*     int maxsamples; */
/*     int maxpackets; */
/*     int maxpacketsize; */
/*     //  int semid; */
/*     int semkey; */
/* }; */

struct generalHeader{
    char network[MAX_CHARS_IN_NETWORK_STRING];
    char station[MAX_CHARS_IN_STATION_STRING];
    char channel[MAX_CHARS_IN_CHANNEL_STRING];
    char location[MAX_CHARS_IN_LOCATION_STRING];
    double samprate;
    int maxsamples;
    int maxpackets;
    int maxpacketsize;
    //  int semid;
    int semkey;
};



// State variables for the sample array
struct sampleHeader {
  int newest;
  int oldest;
  double newtime;
  double oldtime;
};


// Format of a single sample entry
struct sampleEntry {
    short int valid : 1;
    short int index : 15;
};


// State variables for the packet array
struct dataPacketHeader {
  int newest;
};


// Format of a single packet entry
struct dataPacketEntry {
    // Fields for book-keeping
    int startsamp;
    int refcount;
    int origrefcount;
    // Fields that track packet data
    double start;
    int numsamp;
    int datalen;
    char data[RWP_MAX_PACKET_SIZE];
};


// Format of the RWP Shared Memory Area
struct rwpArea {
  struct generalHeader ghdr;
  struct sampleHeader shdr;
  struct dataPacketHeader phdr;
  int sarrayoffset;
  int parrayoffset;
  int writer_pid; //There must be only one writer process per RWP area//
};


// Control block containing the shared memory segment key. This structure
// allows multiple RapidWavePool objects to reference the same shared 
// memory area.
struct rwpCB {
    int refcount;
    int shmid;
    int semid;
    void *shma;
    Channel chan;
    struct rwpArea *ma;
    struct sampleEntry *sarray;
    struct dataPacketEntry *parray;
};


class RapidWavePool
{
 private:
    int valid;
    struct rwpCB *cb;

    int _Cleanup();
    int _CreateSharedResources(void **shma, int &shmid, 
			       int shmkey, int shmsize, 
			       int semkey, int &semid);
    int _UseSharedResources(void **shma, int shmid, int semkey, int &semid);
    int _CleanupSharedResources(void *shma, int shmid, int semid);
    int _AcquireLock(int semid);
    int _ReleaseLock(int semid);    
    int _VerifySegment(const Channel &c, int numsecs, int maxpackets,
		       int semkey, struct rwpArea *ma);
    int _InitializeSamples(struct rwpArea *ma,
			   struct sampleEntry *sarray);
    int _InitializePackets(struct rwpArea *ma,
			   struct dataPacketEntry *parray);
    int _ClearPacketSamples(int packindex);
    int _GetNextPacketIndex(Counter &packindex);
    int _InsertPacket(DataSegment &ds, Counter startsamp, Counter numsamp,
		      Counter packindex);
    int _ClearData(struct rwpArea *ma, struct sampleEntry *sarray,
		   struct dataPacketEntry *parray);
    TimeStamp _TimeOfSample(Counter samp);
    int _NextSampleOfTime(TimeStamp t, Counter &samp);
    int _NearestSampleOfTime(TimeStamp t, Counter &samp);

    // Diagnostic methods
    int DumpState();
    int DumpSegment(DataSegment &ds);

 protected:

 public:    
    RapidWavePool();
    RapidWavePool(const RapidWavePool &rwp);
    RapidWavePool(const Channel &c, int numsecs, int maxpackets, 
		  int semkey, int shmkey);
    ~RapidWavePool();
    int lockWriter();
    int unlockWriter();
    int Insert(DataSegment &ds);
    int Extract(TimeStamp reqstart, int reqnumsamp, DataSegmentList &dl);
    double GetSampleRate();
    int GetTimes(TimeWindowList &tl);

    friend int operator!(const RapidWavePool &rwp);
    RapidWavePool& operator=(const RapidWavePool &rwp);
};


#endif

